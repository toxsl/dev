/*******************************************************************************
 * Copyright (c) 1999, 2014 IBM Corp.
 * <p/>
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * <p/>
 * The Eclipse Public License is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 */
package com.onlinetaxi.mqtt;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;


import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.helper.AppController;
import com.onlinetaxi.helper.SessionManager;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;


/**
 * Handles call backs from the MQTT Client
 */
public class MqttCallbackHandler implements MqttCallback {

    /**
     * {@link Context} for the application used to format and import external strings
     **/
    private Context context;
    /**
     * Client handle to reference the connection that this handler is attached to
     **/
    private String clientHandle;

    /**
     * Creates an <code>MqttCallbackHandler</code> object
     *
     * @param context      The application's context
     * @param clientHandle The handle to a {@link Connection} object
     */
    public MqttCallbackHandler(Context context, String clientHandle) {
        this.context = context;
        this.clientHandle = clientHandle;
    }

    /**
     * @see MqttCallback#connectionLost(Throwable)
     */
    @Override
    public void connectionLost(Throwable cause) {
//	  cause.printStackTrace();
        if (cause != null) {
            Connection c = AppController.getInstance().getConnection();
            c.addAction("Connection Lost");
            c.changeConnectionStatus(Connection.ConnectionStatus.DISCONNECTED);

            //format string to use a notification text
            Object[] args = new Object[2];
            args[0] = "You";
            args[1] = c.getHostName();

            String message = context.getString(R.string.connection_lost, args);

            //build intent
            Intent intent = new Intent();
            intent.setClassName(context, "org.eclipse.paho.android.service.sample.ConnectionDetails");
            intent.putExtra("handle", clientHandle);

            //notify the user
            Notify.notifcation(context, message, intent, R.string.notifyTitle_connectionLost);
        }
    }

    /**
     * @see MqttCallback#messageArrived(String, MqttMessage)
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {

        //Get connection object associated with this object
        Connection c = AppController.getInstance().getConnection();

        //create arguments to format message arrived notifcation string
        String[] args = new String[2];
        args[0] = new String(message.getPayload());
        args[1] = topic + ";qos:" + message.getQos() + ";retained:" + message.isRetained();

        //get the string from strings.xml and format
        String messageString = context.getString(R.string.messageRecieved, (Object[]) args);

        //create intent to start activity
        Intent intent = new Intent();
        intent.setClassName(context, "org.eclipse.paho.android.service.sample.ConnectionDetails");
        intent.putExtra("handle", clientHandle);


        if (message.getQos() == 1) {

//            if (TextUtils.equals(topic, SessionManager.getReturnTopic(context))) {
//                String msg = new String(message.getPayload());
////            Log.e("Return Topic Handler", msg);
//                c.returnTopicData(message);
//            }


            //format string args
            Object[] notifyArgs = new String[3];
            notifyArgs[0] = "You";
            notifyArgs[1] = new String(message.getPayload());
            notifyArgs[2] = topic;

            //notify the user
            Notify.notifcation(context, context.getString(R.string.notification, notifyArgs), intent, R.string.notifyTitle);

            //update client history
            c.addAction(messageString);

            c.updateChat(args[0]);


        } else if (message.getQos() == 0) {

            if (TextUtils.equals(topic, SessionManager.getReturnTopic(context))) {
                //String msg = new String(message.getPayload());
                //Notify.notifcation(context, msg, intent, R.string.notifyTitle);
                c.returnTopicData(message);
            }
        }
    }

    /**
     * @see MqttCallback#deliveryComplete(IMqttDeliveryToken)
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        // Do nothing
    }

}
