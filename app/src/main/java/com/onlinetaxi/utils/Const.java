package com.onlinetaxi.utils;

public class Const {


    public static final int GALLERY_REQ = 1;
    public static final int CAMERA_REQ = 2;
    public static final int GALLERY_CAMERA_BOTH = 3;
    public static final int GALLERY_MULTI_IMAGE = 4;
    public static final int CAMERA_MULTI_IMAGE = 5;


    public static final int ALL_NOTI = -1;


    public static final String API_JOURNEY_DETAIL_ID = "api/journey/detail/id/";

    public static final String JOURNEY_ID = "journey_id";

    public static final long MIN_TIME_BW_UPDATES = 15000;
    public static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 100;
    public static String SERVER_REMOTE_URL = "http://web.toxsl.in/taxi_now/";
//    public static final String SENDER_ID = "861598299514";
    public static final String FOREGROUND = "foreground";
    public static final String DRIVER_ID = "driver_id";
    public static final String FROM_LAT = "from_lat";
    public static final String FROM_LONG = "from_long";
    public static final String TO_LAT = "to_lat";
    public static final String TO_LONG = "to_long";
    public static final String JOURNEY_STATE = "journey_state";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String History_List = "historylist";


/*---------------------------Constants used in shared prefrences-----------------------------------*/


    public static  String USER_TYPE = "user_type" ;
    public static  String USER_PASSENGER = "P" ;
    public static  String USER_DRIVER = "D" ;
    public static  String USER_STATUS="user_status";




}