package com.onlinetaxi.utils;

/**
 * Created by TOXSL/utkarsh.shukla on 2/12/16.
 */

public interface MyMqttConnectedCallback {
    public void onMqttConnected();

}
