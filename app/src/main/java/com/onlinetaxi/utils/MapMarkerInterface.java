package com.onlinetaxi.utils;

import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by TOXSL/utkarsh.shukla on 1/12/16.
 */

public interface MapMarkerInterface {
    void OnLoadedMarkerData(MqttMessage mqttMessage);
}
