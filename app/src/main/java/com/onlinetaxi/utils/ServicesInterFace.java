package com.onlinetaxi.utils;

/**
 * Created by TOXSL/utkarsh.shukla on 29/11/16.
 */

public interface ServicesInterFace {
    void OnStart();
    void OnStop();

}
