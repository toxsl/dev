package com.onlinetaxi.utils;

import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class MapUtils {
    private LatLng origin, destination;
    Context context;
    private static final MapUtils mapUtils = new MapUtils();
    private boolean isAddLine;
    public GoogleMap routeMap;

    private Polyline polyline;
    private Double totalDistance;

    public MapUtils getInstance(Context context) {
        mapUtils.setAct(context);
        return mapUtils;
    }

    private void setAct(Context mAct) {
        this.context = mAct;
    }

    public String getAddress(double lat, double lang) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context);
            if (lat != 0 || lang != 0) {
                addresses = geocoder.getFromLocation(lat, lang, 1);
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                String country = addresses.get(0).getAddressLine(2);
                String state = addresses.get(0).getSubLocality();
                return address + ", " + city + ", " + (country != null ? country : "");
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getDirectionsUrl(LatLng origin, LatLng dest) {
        isAddLine = true;
        String str_origin = "origin=" + origin.latitude + ","
                + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        public DownloadTask(LatLng source, LatLng dest, GoogleMap map) {

            origin = source;
            destination = dest;
            routeMap = map;
        }

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);

        }
    }

    public String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
        } finally {
            if (iStream != null)
                iStream.close();
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return data;
    }

    public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser(((BaseActivity) context).store);
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = new PolylineOptions();
            if (result == null) {
                return;
            }
            if (result.size() < 1) {
                return;
            }

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if (j == 0) {
                        String line = point.get("distance");
                        if (line != null) {
                            String[] parts = line.split(" ");
                            double distance = Double.parseDouble(parts[0].replace(",", "."));

                            int dis = (int) Math.ceil(distance);
                            totalDistance = distance;
                        }
                        continue;

                    } else if (j == 1) {

                        String duration = point.get("duration");
                        if (duration.contains("hours")
                                && (duration.contains("mins") || duration
                                .contains("min"))) {

                            String[] arr = duration.split(" ");
                            int timeDur = 0;
                            for (int k = 0; k < arr.length; k++) {
                                if (k == 0)
                                    timeDur = Integer.parseInt(arr[k]) * 60;
                                if (k == 2)
                                    timeDur = timeDur
                                            + Integer.parseInt(arr[k]);

                            }

//                            totalDuration = String.valueOf(timeDur);

                        } else if (duration.contains("mins")
                                || duration.contains("min")) {
                            String[] words = duration.split(" ");
//                            totalDuration = words[0];
                        }
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.BLUE);
            }
            if (polyline != null)
                polyline.remove();

            polyline = routeMap.addPolyline(lineOptions);
        }
    }

}
