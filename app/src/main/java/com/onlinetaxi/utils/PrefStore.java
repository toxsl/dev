package com.onlinetaxi.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.onlinetaxi.data.AvailableDriverData;
import com.onlinetaxi.data.HistoryData;

import java.util.ArrayList;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class PrefStore {

    private Context mAct;
    boolean checkstate = false;
    private String PREFS_NAME = "YourApp";

    public PrefStore(Context aAct) {
        this.mAct = aAct;
    }

    public SharedPreferences getPref() {
        return PreferenceManager.getDefaultSharedPreferences(mAct);
    }

    public void cleanPref() {
        SharedPreferences settings = getPref();
        settings.edit().clear().apply();

    }


    public boolean getBoolean(String key) {
        return getBoolean(key, true);
    }

    public boolean getNotificationValue(String key) {
        return getBoolean(key, true);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        SharedPreferences settings = getPref();

        return settings.getBoolean(key, defaultValue);
    }

    public void setBoolean(String key, boolean value) {
        SharedPreferences settings = getPref();

        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean(key, value);

        editor.apply();
    }

    public void saveString(String key, String value) {
        SharedPreferences settings = getPref();

        SharedPreferences.Editor editor = settings.edit();

        editor.putString(key, value);

        editor.apply();
    }

    public String getString(String key) {
        SharedPreferences settings = getPref();

        return (settings.getString(key, null));
    }

    public boolean containValue(String key) {
        SharedPreferences settings = getPref();

        return settings.contains(key);
    }

    public void saveLong(String key, long value) {
        SharedPreferences settings = getPref();

        SharedPreferences.Editor editor = settings.edit();

        editor.putLong(key, value);

        editor.apply();
    }

    public long getLong(String key) {
        SharedPreferences settings = getPref();
        return settings.getLong(key, 0);
    }

    public void setAuthCode(String key, String value) {
        SharedPreferences settings = getPref();

        SharedPreferences.Editor editor = settings.edit();

        editor.putString(key, value);

        editor.apply();
    }

    public String getAuthCode(String key) {
        SharedPreferences settings = getPref();

        return settings.getString(key, "");
    }

    public void setPassword(String key, String value) {
        SharedPreferences settings = getPref();

        SharedPreferences.Editor editor = settings.edit();

        editor.putString(key, value);

        editor.apply();
    }

    public String getPassword(String key) {
        SharedPreferences settings = getPref();

        return settings.getString(key, "");
    }

    public void setUserName(String key, String value) {
        SharedPreferences settings = getPref();

        SharedPreferences.Editor editor = settings.edit();

        editor.putString(key, value);

        editor.apply();
    }

    public String getUserName(String key) {
        SharedPreferences settings = getPref();

        return settings.getString(key, "");
    }

    public boolean getNotificationEnabled() {
        return getBoolean("NotificationValue", false);

    }

    public String getLoginStatus() {
        // Restore preferences
        SharedPreferences settings = mAct.getSharedPreferences(PREFS_NAME, 0);

        return settings.getString("auth_code", null);
    }

    public void setLoginStatus(String loginValid) {
        SharedPreferences settings = mAct.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("auth_code", loginValid);
        // Commit the edits!
        editor.apply();
        //log("PrefStore" + "loginValid " + loginValid);
    }

    public void setInt(String subscription_id, int sbu_id) {
        SharedPreferences settings = getPref();

        SharedPreferences.Editor editor = settings.edit();

        editor.putInt(subscription_id, sbu_id);

        editor.apply();
    }


    public int getInt(String key) {
        SharedPreferences settings = getPref();
        return settings.getInt(key, 0);
    }


    public <T extends Object> void setData(String value, ArrayList<T> datas) {
        getPref().edit().putString(value, ObjectSerializer.serialize(datas)).commit();
    }

    public <T extends Object> ArrayList<T> getData(String name) {

        return (ArrayList<T>) ObjectSerializer.deserialize(getPref().
                getString(name, ObjectSerializer.serialize(new ArrayList<T>())));
    }

/*    public <T extends Object> ArrayList<T> getAvailableDriverDatas(String key, Class<T> tClass) {

        return (ArrayList<T>) ObjectSerializer.deserialize(getPref().
                getString(key, ObjectSerializer.serialize(new ArrayList<T>())));
    }*/
}