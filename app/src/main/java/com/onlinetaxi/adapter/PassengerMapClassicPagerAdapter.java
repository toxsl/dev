package com.onlinetaxi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.onlinetaxi.fragments.PassengerClassicFragment;
import com.onlinetaxi.fragments.PassengerMapFragment;



/**
 * Created by sukhwinder.kaur on 1/8/16.
 */
public class PassengerMapClassicPagerAdapter extends FragmentStatePagerAdapter {
    CharSequence titles[];
    int numOfTabs;

    public PassengerMapClassicPagerAdapter(FragmentManager fm, CharSequence titles[], int numOfTabs) {
        super(fm);
        this.titles = titles;
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new PassengerMapFragment();
        } else {
            return new PassengerClassicFragment();
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
