package com.onlinetaxi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.onlinetaxi.fragments.DestinationFragment;
import com.onlinetaxi.fragments.LoginFragment;
import com.onlinetaxi.fragments.PickupFragment;
import com.onlinetaxi.fragments.RegisterFragment;


/**
 * Created by sukhwinder.kaur on 1/8/16.
 */
public class SetLocationFragmentsAdapter extends FragmentStatePagerAdapter {
    CharSequence titles[];
    int numOfTabs;

    public SetLocationFragmentsAdapter(FragmentManager fm, CharSequence titles[], int numOfTabs) {
        super(fm);
        this.titles = titles;
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new PickupFragment();
        } else {
            return new DestinationFragment();
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
