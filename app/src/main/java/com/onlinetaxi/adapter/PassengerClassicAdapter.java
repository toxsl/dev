package com.onlinetaxi.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.data.AvailableDriverData;
import com.onlinetaxi.utils.FontManager;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 8/11/16.
 */
public class PassengerClassicAdapter extends BaseAdapter {
    Activity activity;
    Context context;
    ArrayList<AvailableDriverData> list;
    Typeface iconFont;



    public PassengerClassicAdapter(Activity activity, Context context, ArrayList<AvailableDriverData> list) {
        this.activity = activity;
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.passenger_classic_row, parent, false);
            viewHolder.DriverNameTV = (TextView) convertView.findViewById(R.id.DriverNameTV);
            viewHolder.PickupAddressTV = (TextView) convertView.findViewById(R.id.PickupAddressTV);
            viewHolder.fontStar1 = (TextView) convertView.findViewById(R.id.fontStar1);
            viewHolder.fontStar2 = (TextView) convertView.findViewById(R.id.fontStar2);
            viewHolder.fontStar3 = (TextView) convertView.findViewById(R.id.fontStar3);
            viewHolder.fontStar4 = (TextView) convertView.findViewById(R.id.fontStar4);
            viewHolder.fontStar5 = (TextView) convertView.findViewById(R.id.fontStar5);
            viewHolder.textviewDriverRate = (TextView) convertView.findViewById(R.id.textviewDriverRate);
            viewHolder.textViewTime = (TextView) convertView.findViewById(R.id.textViewTime);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.availableDriverData = list.get(position);
        if (viewHolder.availableDriverData.driverStatus.equalsIgnoreCase("AVAILABLE")) {
            viewHolder.DriverNameTV.setText(viewHolder.availableDriverData.driverName);
            viewHolder.textviewDriverRate.setText(viewHolder.availableDriverData.driverKM);
            int rating = 0;
            if (viewHolder.availableDriverData.driverrating != null && !viewHolder.availableDriverData.driverrating.isEmpty())
                rating = Integer.parseInt(viewHolder.availableDriverData.driverrating);
            Rating(rating, viewHolder);
        }
        iconFont = FontManager.getTypeface(context, FontManager.FONTAWESOMEWebFont);
        FontManager.markAsIconContainer(viewHolder.fontStar1, iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar2, iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar3, iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar4, iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar5, iconFont);
        viewHolder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
        viewHolder.fontStar2.setTextColor(Color.parseColor("#e77c12"));
        return convertView;
    }

    private class ViewHolder {
        public TextView DriverNameTV, PickupAddressTV, textviewDriverRate, textViewTime;
        public TextView fontStar1, fontStar2, fontStar3, fontStar4, fontStar5;
        public LinearLayout parentViewLL;
        private ImageView ImageVievIV;
        private AvailableDriverData availableDriverData;

    }

    @Override
    public int getCount() {
        if (list.size() == 0)
            return 0;
        else
            return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    private void Rating(int rating, ViewHolder viewHolder) {
        if (rating == 0) {
            viewHolder.fontStar1.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar2.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar3.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar4.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar5.setTextColor(Color.parseColor("#6E6E82"));
        } else if (rating == 1) {
            viewHolder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar2.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar3.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar4.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar5.setTextColor(Color.parseColor("#6E6E82"));
        } else if (rating == 2) {
            viewHolder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar2.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar3.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar4.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar5.setTextColor(Color.parseColor("#6E6E82"));
        } else if (rating == 3) {
            viewHolder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar2.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar3.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar4.setTextColor(Color.parseColor("#6E6E82"));
            viewHolder.fontStar5.setTextColor(Color.parseColor("#6E6E82"));
        } else if (rating == 4) {
            viewHolder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar2.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar3.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar4.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar5.setTextColor(Color.parseColor("#6E6E82"));
        } else if (rating == 5) {
            viewHolder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar2.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar3.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar4.setTextColor(Color.parseColor("#e77c12"));
            viewHolder.fontStar5.setTextColor(Color.parseColor("#e77c12"));
        }

    }


}
