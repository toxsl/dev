package com.onlinetaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.DriverRatingData;
import com.onlinetaxi.data.FeedBackListData;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 16/11/16.
 */
public class FeedBackListAdapter extends ArrayAdapter<FeedBackListData> {
    BaseActivity baseActivity;
    LayoutInflater inflater;

    public FeedBackListAdapter(BaseActivity context, int resource, ArrayList<FeedBackListData>feedBackListDatas ) {
        super(context, resource,feedBackListDatas);
        this.baseActivity=context;
    }
    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView==null){
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            convertView = inflater.inflate(R.layout.feedback_list_adapter,parent,false);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        return convertView;
    }

    private class ViewHolder {

    }
}
