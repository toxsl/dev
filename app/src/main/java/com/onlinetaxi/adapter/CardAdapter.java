package com.onlinetaxi.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.mobile.connect.payment.credit.PWCreditCardType;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.CardsModel;
import com.onlinetaxi.fragments.AccountsFragment;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.CryptoUtil;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.material.ButtonFlat;
import com.onlinetaxi.material.RippleView;
import com.onlinetaxi.ormdata.AppDataBaseOrm;
import com.onlinetaxi.utils.FontManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CardAdapter extends ArrayAdapter<AppDataBaseOrm> {
    private Activity activity;
    private int row;
    Typeface iconFont1;
    private List<AppDataBaseOrm> items;
DeleteCard task;
    private ImageView starIV;
    Dialog alertDialogProgressBar;
    long cardIdTodelete;
    int cardPosition;

    public CardAdapter(Activity act, int row, List<AppDataBaseOrm> _items) {
        super(act, row, _items);
        this.activity = act;
        this.row = row;
        this.items = _items;

    }

    @Override
    public int getCount() {
        return items.size();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.custon_card_row, null);

        }

        final AppDataBaseOrm appDataBaseOrm = items.get(position);
        CryptoUtil cryptoUtil = new CryptoUtil();

        String cardnumber = cryptoUtil.decryptIt(String.valueOf(appDataBaseOrm.getCardnumber()));
        String cardtype = cryptoUtil.decryptIt(String.valueOf(appDataBaseOrm.getCardType()));
        cardIdTodelete=appDataBaseOrm.getId();


        iconFont1 = FontManager.getTypeface(activity, FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(v.findViewById(R.id.fontDelete), iconFont1);

        ((TextView) v.findViewById(R.id.TextViewlastFour)).setText(cardnumber);
        ImageView imageView = (ImageView) v.findViewById(R.id.ImageViewStoredCArdType);
        try {

            switch (PWCreditCardType.valueOf(cardtype)) {
                case VISA:


                    imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.visalogo));
                    imageView.invalidate();

                    break;

                case MASTERCARD:


                    imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.mastercard));
                    imageView.invalidate();
                    break;
                case AMEX:


                    imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.americanexpress));
                    imageView.invalidate();
                    break;

                case DINERS:


                    imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.dinerclub));
                    imageView.invalidate();
                    break;

                case JCB:


                    imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.jcb));
                    imageView.invalidate();
                    break;

                default:
                    break;
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        starIV = (ImageView) v.findViewById(R.id.starIV);

        if (position == 0)
            starIV.setVisibility(View.VISIBLE);
        else
            starIV.setVisibility(View.INVISIBLE);


        ((RippleView) v.findViewById(R.id.rippleViewDElete)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                final Dialog alertDialogdelete = new Dialog(activity);

                alertDialogdelete
                        .requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialogdelete
                        .setContentView(R.layout.chose_delete_card);
                alertDialogdelete.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));


                ButtonFlat delete = (ButtonFlat) alertDialogdelete
                        .findViewById(R.id.buttonflatChanege);

                ButtonFlat buttonFlatCancel = (ButtonFlat) alertDialogdelete
                        .findViewById(R.id.buttonflatCancel);


                alertDialogdelete.show();
                delete.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub


                        if (ApplicationGlob.isConnectingToInternet(activity)) {

                            task = new DeleteCard();

                            task.execute(SessionManager.getUserCountryCode(activity),
                                    SessionManager.getUserID(activity),
                                    appDataBaseOrm.getCardnumber()
                                    );

                        } else {

                            ApplicationGlob.ToastShowInterNetConnection(activity.getApplicationContext(), activity);
                        }
                        cardPosition=position;

                        alertDialogdelete.dismiss();




                    }
                });

                buttonFlatCancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        alertDialogdelete.dismiss();
                    }
                });

            }
        });


        return v;


    }


    private class DeleteCard extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            activity.runOnUiThread(new Runnable() {
                public void run() {
                    progressbar(true, "");
                }
            });

        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_remove_payment_card.php";
                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("pcardno", paramsa[2]);
                params.put("submit", "remove_payment_card");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("OT Login Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {

           activity.runOnUiThread(new Runnable() {
                public void run() {
                    progressbar(false,"");
                }
            });



            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");

                if (returncode.equals("0")) {
                    String massage=jsoObject1.getString("message");

                    if (SessionManager.getTokenNumber(activity.getApplicationContext()).equals(CryptoUtil.decryptIt(items.get(cardPosition).getTokenid()))) {

                        SessionManager.setTokenNumber(activity.getApplicationContext(), "TokenNumber");
                        SessionManager.setStoredCardNumber(activity.getApplicationContext(), "9696");
                    }


                    AppDataBaseOrm.DeleteImformations(cardIdTodelete);

                    items.remove(cardPosition);
                    notifyDataSetChanged();
                    notifyDataSetInvalidated();



                }else{

                    String massage=jsoObject1.getString("message");
                    ToastOnUiThread(massage);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void ToastOnUiThread(final String message) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, message,
                            Toast.LENGTH_LONG).show();
                }
            });

        }

    }


    public void progressbar(boolean b, String string) {

        alertDialogProgressBar = new Dialog(activity, R.style.YourCustomStyle);
        alertDialogProgressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.layout_progressbar);
        alertDialogProgressBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textView = (TextView) alertDialogProgressBar.findViewById(R.id.textView1);

        textView.setVisibility(View.INVISIBLE);
        textView.setText(string);

        if (b == true) {
            alertDialogProgressBar.show();
        } else {

            alertDialogProgressBar.dismiss();
        }


    }

}
