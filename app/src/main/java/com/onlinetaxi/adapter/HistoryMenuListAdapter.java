package com.onlinetaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.HistoryMenuListData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOXSL/utkarsh.shukla on 25/11/16.
 */

public class HistoryMenuListAdapter extends ArrayAdapter<HistoryMenuListData> {
    BaseActivity baseActivity;
    LayoutInflater inflater;
    private List<HistoryMenuListData> historyMenuListDatas;

    public HistoryMenuListAdapter(BaseActivity context, int resource, ArrayList<HistoryMenuListData> historyMenuListDatas) {
        super(context, 0, historyMenuListDatas);
        this.baseActivity = context;
        this.historyMenuListDatas = historyMenuListDatas;
        this.inflater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adapter_history_menu, parent, false);

            holder.MenuTitleTV = (TextView) convertView.findViewById(R.id.MenuTitleTV);
            convertView.setTag(holder);

            holder.MenuTitleTV.setText(historyMenuListDatas.get(position).menu_name);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        return convertView;
    }

    private class ViewHolder {
        TextView MenuTitleTV;
    }
}
