package com.onlinetaxi.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.data.ManageDriverDataModel;

import java.util.ArrayList;


/**
 * Created by w7 on 3/22/2016.
 */
public class ManageDriverMenuAdapter extends BaseAdapter {


    public static Context context;
    public static Activity _activity;
    Dialog alertDialogProgressBar;
    ArrayList<ManageDriverDataModel> listArray;
    private Dialog alertDialogPassengerHistory;

    public static Boolean fromHistory = false;
    public static Boolean fromManageDrivermenu = false;

    public ManageDriverMenuAdapter(ArrayList<ManageDriverDataModel> arrayProducts,
                                   Context context, Activity activity) {
        super();
        this.listArray = arrayProducts;
        this.context = context;
        this._activity = activity;


    }

    public int getCount() {
        return listArray.size(); // total number of elements in the list
    }

    public Object getItem(int i) {
        return listArray.get(i); // single item in the list
    }

    public long getItemId(int i) {
        return i; // index number
    }

    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.mange_history_row,
                    parent, false);

        }

        final ManageDriverDataModel adsproductItem = listArray.get(index);

        TextView textViewtitle = (TextView) view
                .findViewById(R.id.textMenu);
        textViewtitle.setText(adsproductItem.getMenu());


        return view;
    }

/*

    public void showPassengerDurationDialog() {
        alertDialogPassengerHistory = new Dialog(_activity);
        alertDialogPassengerHistory.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogPassengerHistory.setContentView(R.layout.remove_driver_dialog);
        alertDialogPassengerHistory.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialogPassengerHistory.show();


        ButtonFlat buttonFlatChange = (ButtonFlat) alertDialogPassengerHistory.findViewById(R.id.buttonflatChanege);
        ButtonFlat buttonFlatCancel = (ButtonFlat) alertDialogPassengerHistory.findViewById(R.id.buttonflatCancel);


        buttonFlatChange.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (ApplicationGlob.isConnectingToInternet(_activity)) {

                    RemoveDriverTask Task = new RemoveDriverTask();
                    Task.execute();

                } else {

                    ApplicationGlob.ToastShowInterNetConnection(_context, _activity);

                }
                //  textViewPaymentMethod.setText(PaymentTypeString);
                alertDialogPassengerHistory.dismiss();
            }
        });

        buttonFlatCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                alertDialogPassengerHistory.dismiss();

            }
        });


    }

*/

    /* private class RemoveDriverTask extends
             AsyncTask<Transaction, Void, Transaction> {

         @Override
         protected void onPreExecute() {

             super.onPreExecute();

             alertDialogProgressBar = new Dialog(_activity,
                     R.style.YourCustomStyle);

             alertDialogProgressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
             alertDialogProgressBar.setContentView(R.layout.layout_progressbar);
             alertDialogProgressBar.getWindow().setBackgroundDrawable(
                     new ColorDrawable(android.graphics.Color.TRANSPARENT));


             try {
                 progressbar(true, "");
             } catch (Exception e) {
                 // TODO: handle exception
             }

         }

         @Override
         protected Transaction doInBackground(Transaction... params) {
             Transaction result = new Transaction();

             try {
                 DriverModel driverModel = new DriverModel();


                 driverModel.setDriverId(DriverModel.getDriverModel().getDriverId());
                 driverModel.setCompanyCode(DriverModel.getDriverModel().getCompanyCode());


                 result = DriverModel.RemoveDriver(driverModel,
                         _context);


             } catch (Exception e) {
                 // TODO: handle exception
                 e.printStackTrace();

             }

             return result;

         }

         @Override
         protected void onPostExecute(Transaction result) {
             progressbar(false, "dismiss");
             if (result.getTransactionStatus() == 0) {

                 Toast.makeText(_context,
                         result.getTransactionMessage(), Toast.LENGTH_SHORT)
                         .show();
                 Intent intent = new Intent(
                         _context, ManageDriverActivity.class);
                 _activity.startActivity(intent);

             } else {

                 Toast.makeText(_context,
                         result.getTransactionMessage(), Toast.LENGTH_SHORT)
                         .show();

             }

         }

     }

 */
    public void progressbar(boolean b, String string) {

        if (b == true) {

            TextView textView = (TextView) alertDialogProgressBar
                    .findViewById(R.id.textView1);

            textView.setVisibility(View.VISIBLE);

            textView.setText(string);

            textView.invalidate();

            alertDialogProgressBar.show();
            LinearLayout dfdsf = (LinearLayout) alertDialogProgressBar
                    .findViewById(R.id.tyh);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 50);
            dfdsf.setLayoutParams(params);

        } else {

            if (string.equals("dismiss")) {

                alertDialogProgressBar.dismiss();

            } else {

                TextView textView = (TextView) alertDialogProgressBar
                        .findViewById(R.id.textView1);
                textView.setText(string);
            }

        }
    }

}