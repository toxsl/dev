package com.onlinetaxi.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.data.PaymentTypeModel;
import com.onlinetaxi.fragments.AddcardFragment;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 1/11/16.
 */
public class SpinnerAdapter extends ArrayAdapter<PaymentTypeModel> {

    private Context context;
    ArrayList<PaymentTypeModel> data = null;

    public SpinnerAdapter(Activity context, int resource,  ArrayList<PaymentTypeModel> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.spinner_value_layout, null);

        }

        TextView textView = (TextView) convertView.findViewById(R.id.spinnerTextView);
        textView.setText(data.get(position).getCardType());

        ImageView imageView = (ImageView) convertView.findViewById(R.id.spinnerImages);
        if (position == 0) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setImageDrawable(context.getResources().getDrawable(data.get(position).getImageDrawable()));
        }
        return convertView;

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.spinner_value_layout, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.spinnerTextView);
        textView.setText(data.get(position).getCardType());

        ImageView imageView = (ImageView) row.findViewById(R.id.spinnerImages);
        imageView.setImageDrawable(context.getResources().getDrawable(data.get(position).getImageDrawable()));

        return row;
    }
}
