package com.onlinetaxi.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.data.SharePaymentModel;

import java.util.List;


public class SpSharedAmountAdapter extends ArrayAdapter<SharePaymentModel> {
    private Activity activity;
    private int row;
    Typeface iconFont1;
    List<SharePaymentModel> items;

    public SpSharedAmountAdapter(Activity act, int row, List<SharePaymentModel> _items) {
        super(act, row, _items);
        this.activity = act;
        this.row = row;
        this.items = _items;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        SharePaymentModel paymentModel=getItem(position);

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.share_amout_frnd_row, null);

        }


        ImageView imageView = (ImageView) v.findViewById(R.id.userimage);
        imageView.setImageDrawable(activity.getResources().getDrawable(R.mipmap.ic_default_profile));

        TextView username_friend=(TextView)v.findViewById(R.id.username);
        TextView lastseen=(TextView)v.findViewById(R.id.lastseen);
        TextView textViewshareAmount=(TextView)v.findViewById(R.id.textViewshareAmount);

        username_friend.setText(paymentModel.getFriendName());
        lastseen.setText(paymentModel.getLastseen());
        textViewshareAmount.setText(paymentModel.getSharedAmount());

        return v;


    }

}
