package com.onlinetaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.AssociationData;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 22/11/16.
 */
public class ManageAssociationListAdapter extends ArrayAdapter<AssociationData> {
    BaseActivity baseActivity;
    LayoutInflater inflater;

    public ManageAssociationListAdapter(BaseActivity context, int resource, ArrayList<AssociationData> associationDatas) {
        super(context, resource,associationDatas);
        this.baseActivity=context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView==null){
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.manage_association_list_adapter,parent,false);

            holder.updateTFA= (TextView) convertView.findViewById(R.id.updateTFA);
            holder.updateTFA.setTypeface(baseActivity.iconFont1);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        return convertView;
    }

    private class ViewHolder {
        TextView updateTFA;

    }
}
