package com.onlinetaxi.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.ManageDriverDataModel;
import com.onlinetaxi.utils.FontManager;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 8/11/16.
 */
public class DriverClassicAdapter extends BaseAdapter {
    Activity activity;
    Context context;
    ArrayList<String> list;
    Typeface iconFont;
    public DriverClassicAdapter(Activity activity, Context context, ArrayList<String> list) {

        this.activity=activity;
        this.context=context;
        this.list=list;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if(convertView==null){
            viewHolder = new ViewHolder();

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.driver_classic_row,parent,false);
            viewHolder.PassengerNameTV = (TextView)convertView.findViewById(R.id.PassengerNameTV);
            viewHolder.PickupAddressTV = (TextView)convertView.findViewById(R.id.PickupAddressTV);

            viewHolder.fontStar1=(TextView)convertView.findViewById(R.id.fontStar1);
            viewHolder.fontStar2=(TextView)convertView.findViewById(R.id.fontStar2);
            viewHolder.fontStar3=(TextView)convertView.findViewById(R.id.fontStar3);
            viewHolder.fontStar4=(TextView)convertView.findViewById(R.id.fontStar4);
            viewHolder.fontStar5=(TextView)convertView.findViewById(R.id.fontStar5);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }


        iconFont = FontManager.getTypeface(context, FontManager.FONTAWESOMEWebFont);

        FontManager.markAsIconContainer(viewHolder.fontStar1,iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar2,iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar3,iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar4,iconFont);
        FontManager.markAsIconContainer(viewHolder.fontStar5,iconFont);

        viewHolder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
        viewHolder.fontStar2.setTextColor(Color.parseColor("#e77c12"));


        return convertView;
    }
    private class ViewHolder{
        public TextView PassengerNameTV,PickupAddressTV;
        public TextView fontStar1,fontStar2,fontStar3,fontStar4,fontStar5;
        public LinearLayout parentViewLL;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
}
