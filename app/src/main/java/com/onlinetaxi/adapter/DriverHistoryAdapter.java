package com.onlinetaxi.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.HistoryData;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 17/11/16.
 */
public class DriverHistoryAdapter extends ArrayAdapter<HistoryData> {
    BaseActivity baseActivity;
    LayoutInflater inflater;
    public DriverHistoryAdapter(BaseActivity context, int resource, ArrayList<HistoryData> historyDatas) {
        super(context, 0,historyDatas);
        this.baseActivity = context;
        this.inflater = context.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView==null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adapter_history_driver,parent,false);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        return convertView;
    }

    private class ViewHolder {

    }
}