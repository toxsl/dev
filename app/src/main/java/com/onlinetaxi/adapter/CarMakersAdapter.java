package com.onlinetaxi.adapter;

import android.app.Activity;


import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;


import com.onlinetaxi.R;
import com.onlinetaxi.data.CarMakerModel;
import com.onlinetaxi.fragments.DetailsFragment;
import com.onlinetaxi.fragments.driver.CarMakersFragment;



import java.util.ArrayList;
import java.util.List;


public class CarMakersAdapter extends BaseAdapter implements Filterable {

    private static final String TAG = CarMakersAdapter.class.getSimpleName();
    ArrayList<CarMakerModel> listArray;
    public static Context _context;
    public static Activity _activity;
    public static String AllocatedDriver, RequestCode;
    ValueFilter valueFilter;
    ArrayList<CarMakerModel> mStringFilterList;
    private List<CarMakerModel> mItems;
    public static Button createVehicleBtn;
    Typeface iconFont;

    public CarMakersAdapter(ArrayList<CarMakerModel> arrayProducts,
                            Context context, Activity activity) {
        super();
        this.listArray = arrayProducts;
        this._context = context;
        this._activity = activity;
        mStringFilterList = arrayProducts;
    }

    public int getCount() {
        return listArray.size(); // total number of elements in the list
    }

    public Object getItem(int i) {
        return listArray.get(i); // single item in the list
    }

    public long getItemId(int i) {
        return i; // index number
    }

    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.mange_history_row,
                    parent, false);

        }

        final CarMakerModel adsproductItem = listArray.get(index);

        TextView textViewtitle = (TextView) view
                .findViewById(R.id.textMenu);
        textViewtitle.setText(adsproductItem.getCarMaker());


        return view;
    }


    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }


    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {


                ArrayList<CarMakerModel> filterList = new ArrayList<CarMakerModel>();
                filterList.clear();

                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ((mStringFilterList.get(i).getCarMaker().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {


                        CarMakerModel country = new CarMakerModel();
                        country.setCarMaker(mStringFilterList.get(i).getCarMaker());

                        filterList.add(country);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {


            if (results.count == 0) {

                CarMakersFragment.MainLayout.setVisibility(View.VISIBLE);
                listArray = (ArrayList<CarMakerModel>) results.values;
                notifyDataSetChanged();

                LayoutInflater layoutInflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.new_vehicle_layout, CarMakersFragment.MainLayout);


            } else {
                CarMakersFragment.MainLayout.setVisibility(View.GONE);
                listArray = (ArrayList<CarMakerModel>) results.values;
                notifyDataSetChanged();

            }
        }

    }


}