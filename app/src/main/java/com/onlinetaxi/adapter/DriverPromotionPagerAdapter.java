package com.onlinetaxi.adapter;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.onlinetaxi.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class DriverPromotionPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    public static String imageUrl;
    public static String loc;

    public static ImageView Image_item;
    ArrayList<String> arrayListVehicles;

    public DriverPromotionPagerAdapter(ArrayList<String> arrayList, Context context) {

        mContext = context;
        arrayListVehicles = arrayList;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayListVehicles.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.viewpager_row, container,
                false);

        Image_item = (ImageView) itemView
                .findViewById(R.id.imageView1);


        Picasso.with(mContext)
                .load(arrayListVehicles.get(position))
                .into(Image_item);

        Image_item.invalidate();
        container.addView(itemView);


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


}