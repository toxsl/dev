package com.onlinetaxi.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.onlinetaxi.R;
import com.onlinetaxi.utils.FontManager;

import java.util.List;


public class AllocateVehicleAdapter extends RecyclerView.Adapter<AllocateVehicleAdapter.MyViewHolder> {

    private Context mContext;
    private List<String> featureList;
    Typeface iconFont;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView vehicleNameTV,vehicleModelTV;
        public TextView fontStar1,fontStar2,fontStar3,fontStar4,fontStar5;



        public MyViewHolder(View view) {
            super(view);
            vehicleNameTV = (TextView) view.findViewById(R.id.vehicleNameTV);
            vehicleModelTV = (TextView) view.findViewById(R.id.vehicleModelTV);

            fontStar1=(TextView)view.findViewById(R.id.fontStar1);
            fontStar2=(TextView)view.findViewById(R.id.fontStar2);
            fontStar3=(TextView)view.findViewById(R.id.fontStar3);
            fontStar4=(TextView)view.findViewById(R.id.fontStar4);
            fontStar5=(TextView)view.findViewById(R.id.fontStar5);

            iconFont = FontManager.getTypeface(mContext, FontManager.FONTAWESOMEWebFont);

            FontManager.markAsIconContainer(fontStar1,iconFont);
            FontManager.markAsIconContainer(fontStar2,iconFont);
            FontManager.markAsIconContainer(fontStar3,iconFont);
            FontManager.markAsIconContainer(fontStar4,iconFont);
            FontManager.markAsIconContainer(fontStar5,iconFont);

        }
    }


    public AllocateVehicleAdapter(Context mContext, List<String> List) {
        this.mContext = mContext;
        this.featureList = List;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.allocate_vehicle_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        holder.fontStar1.setTextColor(Color.parseColor("#e77c12"));
        holder.fontStar2.setTextColor(Color.parseColor("#e77c12"));

    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
