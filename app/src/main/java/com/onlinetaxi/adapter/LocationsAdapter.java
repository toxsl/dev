package com.onlinetaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.NeartestPlacesData;

import java.util.ArrayList;

/**
 * Created by TOXSL\jyotpreet.kaur on 29/9/16.
 */
public class LocationsAdapter extends ArrayAdapter<NeartestPlacesData> {
    ArrayList<NeartestPlacesData> list;
    BaseActivity context;

    public LocationsAdapter(BaseActivity context, ArrayList<NeartestPlacesData> list) {
        super(context, 0, list);
        this.list = list;
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_location, parent, false);
        TextView location_nameTV = (TextView) view.findViewById(R.id.location_nameTV);
        TextView vicnityTV = (TextView) view.findViewById(R.id.vicnityTV);
        NeartestPlacesData data = list.get(position);
        location_nameTV.setText(data.locationName);
        vicnityTV.setText(data.vicinity);
        return view;


    }
}
