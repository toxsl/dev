package com.onlinetaxi.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.FavListData;

import java.util.List;

/**
 * Created by TOXSL/utkarsh.shukla on 1/11/16.
 */
public class FavListAdapter extends ArrayAdapter<FavListData> {
    private Activity activity;
    LayoutInflater inflater;


    public FavListAdapter(BaseActivity context, int resource, List<FavListData> favListDatas) {
        super(context, resource, favListDatas);
        this.activity = context;
        this.inflater = activity.getLayoutInflater();

    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder ViewHolder = null;
        if (convertView == null) {
            ViewHolder = new ViewHolder();

            convertView = inflater.inflate(R.layout.adapter_fav_list, null);


            ViewHolder.profileIV= (ImageView) convertView.findViewById(R.id.profileIV);
            ViewHolder.nameTV= (TextView) convertView.findViewById(R.id.nameTV);
            ViewHolder.carNameTV= (TextView) convertView.findViewById(R.id.carNameTV);
            ViewHolder.ratingRb= (RatingBar) convertView.findViewById(R.id.ratingRb);
            ViewHolder.distanceTV= (TextView) convertView.findViewById(R.id.distanceTV);


        } else {
            ViewHolder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }

    private class ViewHolder {

        ImageView profileIV;
        TextView nameTV, carNameTV;
        RatingBar ratingRb;
        TextView distanceTV;

    }
}
