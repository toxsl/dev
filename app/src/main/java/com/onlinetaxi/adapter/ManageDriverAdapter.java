package com.onlinetaxi.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.ManageDriverDataModel;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 8/11/16.
 */
public class ManageDriverAdapter extends ArrayAdapter<ManageDriverDataModel> {
    BaseActivity  baseActivity;
    LayoutInflater inflater;
    public ManageDriverAdapter(BaseActivity context, int resource, ArrayList<ManageDriverDataModel> manageDriverDatas) {
        super(context, 0,manageDriverDatas);
        this.baseActivity = context;
        this.inflater = baseActivity.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;
        if(convertView==null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adapter_manage_driver,parent,false);
            viewHolder.nameTVF = (TextView)convertView.findViewById(R.id.nameTVF);
            viewHolder.emailTVF = (TextView)convertView.findViewById(R.id.emailTVF);
            viewHolder.carTytpTVF = (TextView)convertView.findViewById(R.id.carTytpTVF);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.nameTVF.setTypeface(baseActivity.iconFont);
        viewHolder.emailTVF.setTypeface(baseActivity.iconFont);
        viewHolder.carTytpTVF.setTypeface(baseActivity.iconFont1);
        return convertView;
    }
    private class ViewHolder{
        TextView nameTV,emailTV,carTypeTV,nameTVF,emailTVF,carTytpTVF;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
