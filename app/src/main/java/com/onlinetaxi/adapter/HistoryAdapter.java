package com.onlinetaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.HistoryData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOXSL\himanshu.gulati on 29/9/16.
 */
public class HistoryAdapter extends ArrayAdapter<HistoryData> {

    BaseActivity baseActivity;
    LayoutInflater inflater;
    private List<HistoryData> historyDatas;

    public HistoryAdapter(BaseActivity context, int resource, ArrayList<HistoryData> historyDatas) {
        super(context, 0, historyDatas);
        this.baseActivity = context;
        this.historyDatas = historyDatas;
        this.inflater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adapter_history, parent, false);
            holder.dateTv = (TextView) convertView.findViewById(R.id.dateTv);
            holder.paymentMethodTV = (TextView) convertView.findViewById(R.id.paymentMethodTV);
            holder.pickupTV = (TextView) convertView.findViewById(R.id.pickupTV);
            holder.destinationTV = (TextView) convertView.findViewById(R.id.destinationTV);
            holder.amountTV = (TextView) convertView.findViewById(R.id.amountTV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.dateTv.setText(historyDatas.get(position).date);
        holder.paymentMethodTV.setText(historyDatas.get(position).paymentmethod);
//        if (historyDatas.get(position).currencySymbol == null)
            holder.amountTV.setText(historyDatas.get(position).currencySymbol + " " + historyDatas.get(position).amount);
//        else
//            holder.amountTV.setText("R" + " " + historyDatas.get(position).amount);

        holder.pickupTV.setText(historyDatas.get(position).pickup);
        holder.destinationTV.setText(historyDatas.get(position).DropOff);


        return convertView;
    }

    private class ViewHolder {
        TextView dateTv, paymentMethodTV, pickupTV, destinationTV, amountTV;
    }
}
