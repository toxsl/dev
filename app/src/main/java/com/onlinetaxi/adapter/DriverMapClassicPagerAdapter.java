package com.onlinetaxi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.onlinetaxi.fragments.driver.DriverClassicFragment;
import com.onlinetaxi.fragments.driver.DriverMapFragment;


/**
 * Created by sukhwinder.kaur on 1/8/16.
 */
public class DriverMapClassicPagerAdapter extends FragmentStatePagerAdapter {
    CharSequence titles[];
    int numOfTabs;

    public DriverMapClassicPagerAdapter(FragmentManager fm, CharSequence titles[], int numOfTabs) {
        super(fm);
        this.titles = titles;
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new DriverMapFragment();
        } else {
            return new DriverClassicFragment();
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
