package com.onlinetaxi.helper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.nispok.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Android on 10/20/2016.
 */

public class ApplicationGlob {


    public static Comparator<Integer> PassergerLowToHigh = new Comparator<Integer>() {
        public int compare(Integer one, Integer other) {
            return one.compareTo(other);
        }
    };

    public static void loadStrictModePolicies() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }


    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isConnectingToInternet(Activity CurrentActivity) {
        Boolean Connected = false;
        ConnectivityManager connectivity = (ConnectivityManager) CurrentActivity
                .getApplicationContext().getSystemService(
                        Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        Log.e("Network is: ", "Connected" + " " + CurrentActivity.getLocalClassName());
                        Connected = true;
                    } else {
                    }

        } else {
            Log.e("Network is: ", "Not Connected");

            Toast.makeText(CurrentActivity.getApplicationContext(),
                    "Please Check Your  internet connection", Toast.LENGTH_LONG)
                    .show();
            Connected = false;

        }
        return Connected;

    }
    public static void ToastShowInterNetConnection(Context context, Activity activity) {


        Snackbar.with(context) // context
                .text("Please Check Your  internet connection") // text to display
                .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                .textColor(Color.parseColor("#ffffff"))
                .show(activity);


    }


    public static ArrayList<Integer> getNextfivePassenger(Integer passenger) {

        ArrayList<Integer> arrayList = new ArrayList<Integer>(0);
        arrayList.add(passenger);
        for (int i = passenger; i > 1; i--) {
            passenger = passenger - 1;
            arrayList.add(passenger);

        }
        return arrayList;

    }


    public static String getCompleteAddressString(double LATITUDE, double LONGITUDE, Context context) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE,
                    LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);

                String countryCode = addresses.get(0).getCountryCode();

                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String zip = addresses.get(0).getPostalCode();
                String country = addresses.get(0).getCountryName();
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress
                            .append(returnedAddress.getAddressLine(i)).append(
                            "\n");
                }
                strAdd = strReturnedAddress.toString().trim();
                Log.e("My Current loction",
                        "" + strReturnedAddress.toString());
            } else {
                Log.e("My Current loction", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My Current loction", "Canont get Address!");
        }
        return strAdd;
    }

    public static String Get_Device_Manufacturer() {
        try {
            String str = Build.MANUFACTURER;
            return str;
        } catch (Exception localException) {
        }
        return "";
    }

    public static String Get_Device_Brand() {
        try {
            String str = Build.BRAND;

            return str;
        } catch (Exception localException) {
        }
        return "";
    }

    public static String Get_DeviceModel() {
        try {
            String str = Build.MODEL;
            return str;
        } catch (Exception localException) {
        }
        return "";
    }



}
