package com.onlinetaxi.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

public class MyLocationListener implements LocationListener {
    // The minimum distance to change updates in metters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    // The minimum time beetwen updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 0;
    LocationListener listener;
    // flag for GPS status
    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    boolean isPassiveLocation = false;
    String providerName;
    private Location location = null;
    private LocationManager locationManager = null;
    private Context context;
    private Location locationStore;

    public MyLocationListener(Context context) {
        this.context = context;
        location();
    }

    public Boolean getGpsStatus() {

        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);


        return isGPSEnabled;


    }

    public Location location() {
        try {
            locationManager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            isPassiveLocation = locationManager
                    .isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!isGPSEnabled && !isNetworkEnabled && !isPassiveLocation) {
                Log.e("Things aren't enabled", isGPSEnabled + " " + isNetworkEnabled + " " + isPassiveLocation);
            } else {
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.e("GPS Enabled", "GPS Enabled");

                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        //updateGPSCoordinates();
                    }
                }
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.e("Network", "Network");

                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            //updateGPSCoordinates();
                        }
                    }

                } else {
                    location = getLastKnownLocation();
                }
            }
        } catch (Exception e) {
            Log.e("Error in location call", e.getMessage());
        }
        this.locationStore = location;
        return location;
    }

    private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return null;
                }
                mLocationManager.requestLocationUpdates(provider, 1000L, 1000F, this);
                l = mLocationManager.getLastKnownLocation(provider);
            }
            if (l != null && (bestLocation == null || l.getAccuracy() > bestLocation.getAccuracy())) {
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public String getCurrentLatitude() {
        if (!isGPSEnabled && !isNetworkEnabled && !isPassiveLocation) {
        } else {
            try {
                if (location.getLatitude() != 0.0) {
                    Log.e("LatNotZero", String.valueOf(location.getLatitude()));
                    return Double.toString(location.getLatitude());
                } else {
                    Log.e("LatZero", String.valueOf(location.getLatitude()));
                    return "0";
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
        return "0";
    }

    public String getCurrentLongitude() {
        if (!isGPSEnabled && !isNetworkEnabled && !isPassiveLocation) {
        } else {
            try {

                if (location.getLongitude() != 0.0) {
                    return Double.toString(location.getLongitude());
                } else {
                    return "0";
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
        return "0";
    }


    public String getLocationLatitude() {

        MyLocationListener listener = new MyLocationListener(context);


        String location = listener.location().toString();

        String latlanfsplit[] = location.split(" ");


        String latlang = latlanfsplit[1];

        String valueoflatlang[] = latlang.split(",");


        String lattitude = valueoflatlang[0];

        String longitude = valueoflatlang[1];

        return lattitude;


    }

    public String getLocationLongitude() {

        MyLocationListener listener = new MyLocationListener(context);
        String location = null;
        if (listener.location() == null) {
            if (locationStore != null)
                location = locationStore.toString();
            else {
                return "0";
            }
        } else
            location = listener.location().toString();

        String latlanfsplit[] = location.split(" ");


        String latlang = latlanfsplit[1];

        String valueoflatlang[] = latlang.split(",");


        String longitude = valueoflatlang[1];

        return longitude;


    }


    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
