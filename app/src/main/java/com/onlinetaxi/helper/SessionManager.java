package com.onlinetaxi.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.onlinetaxi.data.AvailableDriverData;
import com.onlinetaxi.data.DriverProfileData;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.data.HistoryMenuListData;
import com.onlinetaxi.utils.Const;
import com.onlinetaxi.utils.ObjectSerializer;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Android on 10/27/2016.
 */

public class SessionManager {

    private static String UserCountryCodeFromIPApi = "UserCountryCodeFromIPApi";
    private static String UserCountryCodeFromGPS = "UserCountryCodeFromGPS";
    private static String UserCountryCode = "UserCountryCode";
    private static String UserSplashLatitude = "UserSplashLatitude";
    private static String UserSplashLongitude = "UserSplashLongitude";
    private static String UserDeviceToken = "UserDeviceToken";
    private static String UserFirstName = "UserFirstName";
    private static String UserID = "UserID";
    private static String UserLastName = "UserLastName";
    private static String UserEmail = "UserEmail";
    private static String UserContactNumber = "UserContactNumber";
    private static String UserAddress = "UserAddress";
    private static String CarMakerDataSP = "CarMakerDataSP";
    private static String CarModelDataSP = "CarModelDataSP";
    private static String CarYearDataSP = "CarYearDataSP";
    private static String UserStatus = "UserStatus";
    private static String UserType = "UserType";
    private static String UserAvailability = "UserAvailability";
    private static String UserStateTopic = "UserStateTopic";
    private static String Havesine_KM = "Havesine_KM";
    private static String UserReturnTopic = "UserReturnTopic";
    private static String MapRefreshTime = "MapRefreshTime";
    private static String DriverRatePerKM = "DriverRatePerKM";
    private static String UserCountry = "UserCountry";
    private static String Millage_amount = "millageamount";
    private static String paydate = "paydate";
    private static String UsersCurrency = "userscurrency";
    private static String HistoryAmount = "historyamount";
    private static String Historydiscount = "historydiscount";
    private static String Historytaxamount = "historytaxamount";
    private static String HistoryList = "historylist";
    private static String MonthList = "monthlist";
    private static String CurrencyList = "currencylist";
    private static String HistoryMenuListData = "historymenu";
    private static String SelectedHistoryPosition = "selectedhistoryposition";
    private static String CurrencySymbol = "currencysymbol";
    private static String LatitudeFromLocationListner = "LatitudeFromLocationListner";
    private static String LongitudeFromLocationListner = "LongitudeFromLocationListner";
    private static String TokenNumber = "TokenNumber";
    private static String StoredCardNumber = "StoredCardNumber";
    private static String UserZipcode = "UserZipcode";
    private static String Usercity = "Usercity";
    private static String UserState = "UserState";
    private static String MqttClientID = "clientid";
    private static String MqttURI = "mqtturi";
    private static String State_Topic = "statetopic";
    private static String MqttReturnTopic = "mqttreturntopic";
    private static String DriverRadius = "driverradius";
    private static String DriverProfileData = "driverprofiledata";
    private static String AvailableDriverData = "availabledriverdata";

    public static void setDriverProfileData(DriverProfileData driverProfile, Context context) {
        Gson gson = new Gson();
        String json = gson.toJson(driverProfile);
        getPrefs(context).edit().putString(DriverProfileData, json).commit();
    }

    public static DriverProfileData getDriverProfileData(Context context) {
        Gson gson = new Gson();
        DriverProfileData data = gson.fromJson(getPrefs(context).getString(DriverProfileData, null), DriverProfileData.class);
        return data;

    }


    public static String getDriverRadius(Context context) {
        return getPrefs(context).getString(DriverRadius, "");
    }

    public static void setDriverRadius(Context context, String value) {
        getPrefs(context).edit().putString(DriverRadius, value).commit();
    }


    public static String getMqttclientID(Context context) {
        return getPrefs(context).getString(MqttClientID, "");
    }

    public static void setMqttclientID(Context context, String value) {
        getPrefs(context).edit().putString(MqttClientID, value).commit();
    }


    public static String getMqttURI(Context context) {
        return getPrefs(context).getString(MqttURI, "");
    }

    public static void setMqttURI(Context context, String value) {
        getPrefs(context).edit().putString(MqttURI, value).commit();
    }

    public static String getState_Topic(Context context) {
        return getPrefs(context).getString(State_Topic, "");
    }

    public static void setState_Topic(Context context, String value) {
        getPrefs(context).edit().putString(State_Topic, value).commit();
    }

    public static String getUserState(Context context) {
        return getPrefs(context).getString(UserState, UserState);
    }

    public static void setUserState(Context context,
                                    String storedCardNumber) {
        getPrefs(context).edit().putString(UserState, storedCardNumber)
                .commit();
    }

    public static String getUsercity(Context context) {
        return getPrefs(context).getString(Usercity, Usercity);
    }

    public static void setUsercity(Context context,
                                   String storedCardNumber) {
        getPrefs(context).edit().putString(Usercity, storedCardNumber)
                .commit();
    }

    public static String getUserZipcode(Context context) {
        return getPrefs(context).getString(UserZipcode, UserZipcode);
    }

    public static void setUserZipcode(Context context,
                                      String storedCardNumber) {
        getPrefs(context).edit().putString(UserZipcode, storedCardNumber)
                .commit();
    }

    public static String getStoredCardNumber(Context context) {
        return getPrefs(context).getString(StoredCardNumber, StoredCardNumber);
    }

    public static void setStoredCardNumber(Context context,
                                           String storedCardNumber) {
        getPrefs(context).edit().putString(StoredCardNumber, storedCardNumber)
                .commit();
    }


    public static String getTokenNumber(Context context) {
        return getPrefs(context).getString(TokenNumber, TokenNumber);
    }

    public static void setTokenNumber(Context context, String tokenNumber) {
        getPrefs(context).edit().putString(TokenNumber, tokenNumber).commit();
    }


    public static String getLongitudeFromLocationListner(Context context) {
        return getPrefs(context).getString(LongitudeFromLocationListner, "LongitudeFromLocationListner");
    }

    public static void setLongitudeFromLocationListner(Context context, String value) {
        getPrefs(context).edit().putString(LongitudeFromLocationListner, value).commit();
    }

    public static String getLatitudeFromLocationListner(Context context) {
        return getPrefs(context).getString(LatitudeFromLocationListner, "LatitudeFromLocationListner");
    }

    public static void setLatitudeFromLocationListner(Context context, String value) {
        getPrefs(context).edit().putString(LatitudeFromLocationListner, value).commit();
    }

    public static String getCurrencySymbol(Context context) {
        return getPrefs(context).getString(CurrencySymbol, "");
    }

    public static void setCurrencySymbol(Context context, String value) {
        getPrefs(context).edit().putString(CurrencySymbol, value).commit();
    }

    public static String getSelectedHistoryPosition(Context context) {

        return getPrefs(context).getString(SelectedHistoryPosition, "");
    }

    public static void setSelectedHistoryPosition(Context context, String value) {

        getPrefs(context).edit().putString(SelectedHistoryPosition, value).commit();
    }

    public static void setHistoryMenuListData(Context context, ArrayList<HistoryMenuListData> list) {
        getPrefs(context).edit().putString(HistoryMenuListData, ObjectSerializer.serialize(list)).commit();
    }

    public static ArrayList<HistoryMenuListData> getHistoryMenuListData(Context context) {
        return (ArrayList<HistoryMenuListData>) ObjectSerializer.deserialize(getPrefs(context).
                getString(HistoryMenuListData, ObjectSerializer.serialize(new ArrayList<HistoryMenuListData>())));
    }


    public static void setCurrencyList(Context context, ArrayList<String> list) {
        getPrefs(context).edit().putString(CurrencyList, ObjectSerializer.serialize(list)).commit();
    }

    public static ArrayList<String> getCurrencyList(Context context) {
        return (ArrayList<String>) ObjectSerializer.deserialize(getPrefs(context).
                getString(CurrencyList, ObjectSerializer.serialize(new ArrayList<String>())));
    }

    public static void setMonthList(Context context, ArrayList<String> list) {
        getPrefs(context).edit().putString(MonthList, ObjectSerializer.serialize(list)).commit();
    }

    public static ArrayList<String> getMonthList(Context context) {
        return (ArrayList<String>) ObjectSerializer.deserialize(getPrefs(context).
                getString(MonthList, ObjectSerializer.serialize(new ArrayList<String>())));
    }


    public static String getHistoryAmount(Context context) {

        return getPrefs(context).getString(HistoryAmount, "historyamount");
    }

    public static void setHistoryAmount(Context context, String value) {

        getPrefs(context).edit().putString(HistoryAmount, value).commit();
    }

    public static String getHistorydiscount(Context context) {

        return getPrefs(context).getString(Historydiscount, "historydiscount");
    }

    public static void setHistorydiscount(Context context, String value) {

        getPrefs(context).edit().putString(Historydiscount, value).commit();
    }

    public static String getHistorytaxamount(Context context) {

        return getPrefs(context).getString(Historytaxamount, "historytaxamount");
    }

    public static void setHistorytaxamount(Context context, String value) {

        getPrefs(context).edit().putString(Historytaxamount, value).commit();
    }

    public static String getUsersCurrency(Context context) {

        return getPrefs(context).getString(UsersCurrency, "userscurrency");
    }

    public static void setUsersCurrency(Context context, String value) {

        getPrefs(context).edit().putString(UsersCurrency, value).commit();
    }

    public static String getpaydate(Context context) {

        return getPrefs(context).getString(paydate, "paydate");
    }

    public static void setpaydate(Context context, String value) {

        getPrefs(context).edit().putString(paydate, value).commit();
    }


    public static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("UserNameAcrossApplication",
                context.MODE_PRIVATE);//change this to Mode_Private as MODE_WORLD_WRITEABLE crashes in android N as it is deprecated

    }


    public static String getUserCountry(Context context) {

        return getPrefs(context).getString(UserCountry, "UserCountry");
    }

    public static void setUserCountry(Context context, String value) {

        getPrefs(context).edit().putString(UserCountry, value).commit();
    }

    public static String getDriverRatePerKM(Context context) {

        return getPrefs(context).getString(DriverRatePerKM, "DriverRatePerKM");
    }

    public static void setDriverRatePerKM(Context context, String value) {

        getPrefs(context).edit().putString(DriverRatePerKM, value).commit();
    }

    public static String getMapRefreshTime(Context context) {

        return getPrefs(context).getString(MapRefreshTime, "MapRefreshTime");
    }

    public static void setMapRefreshTime(Context context, String value) {

        getPrefs(context).edit().putString(MapRefreshTime, value).commit();
    }

    public static String getUserAvailability(Context context) {

        return getPrefs(context).getString(UserAvailability, "UserAvailability");
    }

    public static void setUserAvailability(Context context, String value) {

        getPrefs(context).edit().putString(UserAvailability, value).commit();
    }


    public static String getHavesine_KM(Context context) {

        return getPrefs(context).getString(Havesine_KM, "Havesine_KM");
    }

    public static void setHavesine_KM(Context context, String value) {

        getPrefs(context).edit().putString(Havesine_KM, value).commit();
    }

    public static String getUserReturnTopic(Context context) {

        return getPrefs(context).getString(UserReturnTopic, "UserReturnTopic");
    }

    public static void setUserReturnTopic(Context context, String value) {

        getPrefs(context).edit().putString(UserReturnTopic, value).commit();
    }


    public static String getUserType(Context context) {
        return getPrefs(context).getString(UserType, "UserType");
    }


    public static String getUserStatus(Context context) {
        return getPrefs(context).getString(UserStatus, "UserStatus");
    }

    public static void setUserType(Context context, String userType) {
        getPrefs(context).edit().putString(UserType, userType).commit();
    }

    public static void setUserStatus(Context context, String userStatus) {
        getPrefs(context).edit().putString(UserStatus, userStatus).commit();
    }

    public static String getCarMakerDataSP(Context context) {

        return getPrefs(context).getString(CarMakerDataSP, "CarMakerDataSP");
    }

    public static void setCarMakerDataSP(Context context, String value) {

        getPrefs(context).edit().putString(CarMakerDataSP, value).commit();
    }

    public static String getCarModelDataSP(Context context) {

        return getPrefs(context).getString(CarModelDataSP, "CarModelDataSP");
    }

    public static void setCarModelDataSP(Context context, String value) {

        getPrefs(context).edit().putString(CarModelDataSP, value).commit();
    }

    public static String getCarYearDataSP(Context context) {

        return getPrefs(context).getString(CarYearDataSP, "CarYearDataSP");
    }

    public static void setCarYearDataSP(Context context, String value) {

        getPrefs(context).edit().putString(CarYearDataSP, value).commit();
    }

    public static String getUserID(Context context) {

        return getPrefs(context).getString(UserID, "UserID");
    }

    public static void setUserID(Context context, String value) {

        getPrefs(context).edit().putString(UserID, value).commit();
    }

    public static String getUserFirstName(Context context) {

        return getPrefs(context).getString(UserFirstName, "UserFirstName");
    }

    public static void setUserFirstName(Context context, String value) {

        getPrefs(context).edit().putString(UserFirstName, value).commit();
    }

    public static String getUserLastName(Context context) {

        return getPrefs(context).getString(UserLastName, "UserLastName");
    }

    public static void setUserLastName(Context context, String value) {

        getPrefs(context).edit().putString(UserLastName, value).commit();
    }

    public static String getUserEmail(Context context) {

        return getPrefs(context).getString(UserEmail, "UserEmail");
    }

    public static void setUserEmail(Context context, String value) {

        getPrefs(context).edit().putString(UserEmail, value).commit();
    }

    public static String getUserAddress(Context context) {

        return getPrefs(context).getString(UserAddress, "UserAddress");
    }

    public static void setUserAddress(Context context, String value) {

        getPrefs(context).edit().putString(UserAddress, value).commit();
    }

    public static String getUserContactNumber(Context context) {

        return getPrefs(context).getString(UserContactNumber, "UserContactNumber");
    }

    public static void setUserContactNumber(Context context, String email) {

        getPrefs(context).edit().putString(UserContactNumber, email).commit();
    }

    public static String getUserDeviceToken(Context context) {

        return getPrefs(context).getString(UserDeviceToken, "UserDeviceToken");
    }

    public static void setUserDeviceToken(Context context, String email) {

        getPrefs(context).edit().putString(UserDeviceToken, email).commit();
    }

    public static String getUserCountryCodeFromIPApi(Context context) {

        return getPrefs(context).getString(UserCountryCodeFromIPApi, "UserCountryCodeFromIPApi");
    }

    public static void setUserCountryCodeFromIPApi(Context context, String email) {

        getPrefs(context).edit().putString(UserCountryCodeFromIPApi, email).commit();
    }

    public static String getUserCountryCodeFromGPS(Context context) {

        return getPrefs(context).getString(UserCountryCodeFromGPS, "UserCountryCodeFromGPS");
    }

    public static void setUserCountryCodeFromGPS(Context context, String value) {

        getPrefs(context).edit().putString(UserCountryCodeFromGPS, value).commit();
    }

    public static String getUserCountryCode(Context context) {

        return getPrefs(context).getString(UserCountryCode, "UserCountryCode");
    }

    public static void setUserCountryCode(Context context, String value) {

        getPrefs(context).edit().putString(UserCountryCode, value).commit();
    }

    public static String getUserSplashLatitude(Context context) {

        return getPrefs(context).getString(UserSplashLatitude, "UserSplashLatitude");
    }

    public static void setUserSplashLatitude(Context context, String value) {

        getPrefs(context).edit().putString(UserSplashLatitude, value).commit();
    }

    public static String getUserSplashLongitude(Context context) {

        return getPrefs(context).getString(UserSplashLongitude, "UserSplashLongitude");
    }

    public static void setUserSplashLongitude(Context context, String value) {

        getPrefs(context).edit().putString(UserSplashLongitude, value).commit();
    }

    public static String getMillageAmount(Context context) {

        return getPrefs(context).getString(Millage_amount, "millageamount");
    }

    public static void setMillageAmount(Context context, String value) {

        getPrefs(context).edit().putString(Millage_amount, value).commit();
    }

    public static void setHistoryData(Context context, ArrayList<HistoryData> list) {
        getPrefs(context).edit().putString(HistoryList, ObjectSerializer.serialize(list)).commit();
    }

    public static ArrayList<HistoryData> getHistoryData(Context context) {
        return (ArrayList<HistoryData>) ObjectSerializer.deserialize(getPrefs(context).
                getString(HistoryList, ObjectSerializer.serialize(new ArrayList<HistoryData>())));
    }

    public static String getReturnTopic(Context context) {
        return getPrefs(context).getString(MqttReturnTopic, "");
    }

    public static void setReturnTopic(Context context, String value) {

        getPrefs(context).edit().putString(MqttReturnTopic, value).commit();
    }

    public static void setAvailableDriverData(Context context, ArrayList<AvailableDriverData> list) {
        getPrefs(context).edit().putString(AvailableDriverData, ObjectSerializer.serialize(list)).commit();
    }

    public static ArrayList<AvailableDriverData> getAvailableDriverData(Context context) {
        return (ArrayList<AvailableDriverData>) ObjectSerializer.deserialize(getPrefs(context).
                getString(AvailableDriverData, ObjectSerializer.serialize(new ArrayList<AvailableDriverData>())));
    }


}
