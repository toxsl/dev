package com.onlinetaxi.activity;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.onlinetaxi.R;
import com.onlinetaxi.adapter.LoginFragmentsAdapter;
import com.onlinetaxi.utils.SlidingTabLayout;

public class LoginRegisterActivity extends BaseActivity {

    private ViewPager pager;
    private CharSequence titles[] = new CharSequence[2];
    private LoginFragmentsAdapter adapter;
    private int noOfTabs = 2;
    private SlidingTabLayout tabs;
    private FloatingActionButton submitFAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);
        init();
    }

    private void init() {

        pager = (ViewPager) findViewById(R.id.pager);
        titles[0] = getString(R.string.signin);
        titles[1] = getString(R.string.register);
        adapter = new LoginFragmentsAdapter(getSupportFragmentManager(), titles,
                noOfTabs);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorPrimary);
            }

            @Override
            public int getDividerColor(int position) {
                return getResources().getColor(R.color.transparent);
            }
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
    //    submitFAB = (FloatingActionButton) findViewById(R.id.submitFAB);
      //  submitFAB.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:
                if (validate()) {
                    gotoMain();
                }
                break;
        }
    }

    private boolean validate() {
        return true;
    }
}
