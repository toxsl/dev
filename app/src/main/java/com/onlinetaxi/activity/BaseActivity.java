package com.onlinetaxi.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.onlinetaxi.BuildConfig;
import com.onlinetaxi.R;
import com.onlinetaxi.data.NeartestPlacesData;
import com.onlinetaxi.fragments.PassengerMapAndClassicFragment;
import com.onlinetaxi.helper.AppController;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.MyLocationListener;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.mqtt.ActionListener;
import com.onlinetaxi.mqtt.ActivityConstants;
import com.onlinetaxi.mqtt.Connection;
import com.onlinetaxi.mqtt.MqttAndroidClient;
import com.onlinetaxi.mqtt.MqttCallbackHandler;
import com.onlinetaxi.mqtt.MqttTraceCallback;
import com.onlinetaxi.utils.Const;
import com.onlinetaxi.utils.FontManager;
import com.onlinetaxi.utils.ImageUtils;
import com.onlinetaxi.utils.MapMarkerInterface;
import com.onlinetaxi.utils.MyMqttConnectedCallback;
import com.onlinetaxi.utils.ObjectSerializer;
import com.onlinetaxi.utils.PrefStore;
import com.onlinetaxi.utils.ServicesInterFace;
import com.onlinetaxi.video.SinchService;
import com.soundcloud.android.crop.Crop;
import com.toxsl.http.RequestParams;
import com.toxsl.http.SyncEventListner;
import com.toxsl.http.SyncManager;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.net.SocketFactory;

import cz.msebera.android.httpclient.Header;


/**
 * Created by tx-ubuntu-207 on 2/7/15.
 */
public class BaseActivity extends AppCompatActivity implements SyncEventListner, View.OnClickListener, ServiceConnection {
    public BaseActivity baseActivity;
    public LayoutInflater inflater;
    public SyncManager syncManager;
    public PrefStore store;
    public PermCallback permCallback;
    public Dialog fullscreenProgressDialog;
    public TextView progressDialogTV;
    private Toast toast;
    private int reqCode;
    private Uri inputUri, outputUri;
    private ImageUtils imageUtils;
    private TextView toastMsgTV;
    private AlertDialog retryDialog;
    public Double longitude, latitude;
    public ArrayList<NeartestPlacesData> nearestLocations = new ArrayList<>();
    public String pickupLocation = "";
    public String destinationLocation = "";
    public Typeface iconFont, iconFont1;
    public ServicesInterFace servicesInterFace;
    public MapMarkerInterface mapMarkerInterface;

    public MqttAndroidClient mqttAndroidClient;
    private Connection connection;
    private ChangeListener changeListener = new ChangeListener();
    private boolean islogedin;
    private MyMqttConnectedCallback myMqttConnectedCallback;
    public MainActivity mainActivity;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    public ServiceConnection serviceConnection;
    private boolean isFirstLogin;


    public static void hideSoftKeyboard(BaseActivity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void RegisterMapInterface(MapMarkerInterface markerInterface) {
        if (markerInterface != null) {
            mapMarkerInterface = markerInterface;
        }
    }


    public void cancelNotification(int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) this.getSystemService(ns);
        if (notifyId == Const.ALL_NOTI)
            nMgr.cancelAll();
        else
            nMgr.cancel(notifyId);
    }

    public void showPermissionsDenialDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.not_sufficient_permissions))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(i);
                    }
                })
                .setNegativeButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.are_you_sure_you_want_to_exit))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        finishAffinity();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void userLoginCheck(String token) {
        if (syncManager.getLoginStatus() == null) {
            errorLogin();
        } else
            check(token);
    }

    protected void check(String token) {
        RequestParams params = new RequestParams();
        if (token != null)
            params.put("AuthSession[device_token]", token);
//        syncManager.sendToServer(Const.API_USER_CHECK, params, this);

    }

    private void errorLogin() {
        syncManager.setLoginStatus(null);
        syncManager.handleCheck(false);
        //  gotoLogin();
    }

    void ServicesConnection() {
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                if (SinchService.class.getName().equals(componentName.getClassName())) {
                    mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
                    onService_Connected();
                    if (servicesInterFace != null) {
                        servicesInterFace.OnStart();

                    }
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                if (SinchService.class.getName().equals(componentName.getClassName())) {
                    mSinchServiceInterface = null;
                    onService_Disconnected();
                    if (servicesInterFace != null) {
                        servicesInterFace.OnStop();

                    }
                }
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        syncManager = SyncManager.getInstance(this);
        ServicesConnection();
        getApplicationContext().bindService(new Intent(this, SinchService.class), serviceConnection,
                BIND_AUTO_CREATE);

      /*  if (islogedin)
            connectMqttClient();*/
        /*connectMqttClient();*/
        iconFont = FontManager.getTypeface(this, FontManager.FONTAWESOME);
        iconFont1 = FontManager.getTypeface(this, FontManager.FONTAWESOMEWebFont);


        strictModeThread();

        createToast();
        checkDate();
        // transitionSlideInHorizontal();
        store = new PrefStore(this);
        progressDialog();
        syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name));

        imageUtils = ImageUtils.getInstance(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void createToast() {
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        toastMsgTV = (TextView) View.inflate(this, R.layout.toast_layout, null);
        toast.setView(toastMsgTV);
    }

    public void gotoMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void gotoLogin() {
        Intent i = new Intent(BaseActivity.this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void resetAppData() {
        store.saveString(Const.FROM_LAT, null);
        store.saveString(Const.FROM_LONG, null);
        store.saveString(Const.TO_LAT, null);
        store.saveString(Const.TO_LONG, null);
        store.saveString(Const.JOURNEY_STATE, null);
        store.setInt(Const.JOURNEY_ID, 0);
    }

    public void initFCM() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        store.saveString(Const.DEVICE_TOKEN, refreshedToken);
        if (refreshedToken != null) {

            if (SessionManager.getUserDeviceToken(getApplicationContext()).equals("UserDeviceToken")) {

                SessionManager.setUserDeviceToken(getApplicationContext(), refreshedToken);
            }


            userLoginCheck(refreshedToken);

        } else {
            errorLogin();
        }

    }

    public void gotoLoginRegister() {
        startActivity(new Intent(BaseActivity.this, LoginRegisterActivity.class));
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Base Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onService_Connected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = null;
            onService_Disconnected();
        }
    }

    public MqttAndroidClient getMqttclient() {
        return mqttAndroidClient;
    }


    public class GetNearLocations extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" +
                    latitude + "," + longitude + "&radius=5000&types=5&sensor=true&key=AIzaSyCad3-AnMIpVCz58allhfsrv3Hh-3pnXZM";
            String resp = getJSON(url, 50000);
            parseJson(resp);
            return null;
        }
    }

    public String getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                }
            }
        }
        return null;
    }

    private void parseJson(String total) {
        try {
            nearestLocations.clear();

            if (total != null) {
                if (total.length() > 10) {
                    JSONObject obj = new JSONObject(total);
                    JSONArray array = obj.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        NeartestPlacesData neartestPlacesData = new NeartestPlacesData();
                        neartestPlacesData.vicinity = object.getString("vicinity");
                        neartestPlacesData.id = object.getString("id");
                        neartestPlacesData.locationName = object.getString("name");
                        JSONObject object1 = object.getJSONObject("geometry");
                        JSONObject object2 = object1.getJSONObject("location");
                        neartestPlacesData.lati = object2.getDouble("lat");
                        neartestPlacesData.longi = object2.getDouble("lng");
                        nearestLocations.add(neartestPlacesData);
                    }

                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String getAddress(double lat, double lang) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this);
            if (lat != 0 || lang != 0) {
                addresses = geocoder.getFromLocation(lat, lang, 1);
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                String country = addresses.get(0).getAddressLine(2);
                String state = addresses.get(0).getSubLocality();
                return address + ", " + city + ", " + (country != null ? country : "");
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String changeDateFormat(String dateString, String sourceDateFormat, String targetDateFormat) {
        SimpleDateFormat inputDateFromat = new SimpleDateFormat(sourceDateFormat, Locale.getDefault());
        Date date = new Date();
        try {
            date = inputDateFromat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(targetDateFormat, Locale.getDefault());
        return outputDateFormat.format(date);
    }

    protected void checkDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(2016, Calendar.NOVEMBER, 25);
        Calendar currentcal = Calendar.getInstance();
       /* if (currentcal.after(cal)) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
            builder.setMessage("Please contact : shiv@toxsl.com");
            builder.setTitle("Demo Expired");
            builder.setCancelable(false);
            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    exitFromApp();
                }
            });
            android.support.v7.app.AlertDialog alert = builder.create();
            alert.show();
        }*/
    }


    public void checkNetwork() {
        if (isNetworkAvailable())
            return;
        AlertDialog.Builder myDialog = new AlertDialog.Builder(this);
        myDialog.setTitle(R.string.network_connection);
        myDialog.setMessage(R.string.you_are_not_connected_to_any_network_press_ok_to_change_settings);
        myDialog.setPositiveButton(R.string.ok_button,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        Intent settings = new Intent(
                                Settings.ACTION_WIFI_SETTINGS);
                        settings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(settings);
                        dialog.dismiss();

                    }
                });
        myDialog.setNegativeButton(R.string.exit,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        exitFromApp();
                        dialog.dismiss();
                    }
                });
        myDialog.setCancelable(false);
        AlertDialog alertd = myDialog.create();
        alertd.show();
    }

    public boolean checkPermissions(String[] perms, int requestCode) {
        this.reqCode = requestCode;
        ArrayList<String> permsArray = new ArrayList<>();
        boolean hasPerms = true;
        for (String perm : perms) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                permsArray.add(perm);
                hasPerms = false;
            }
        }
        if (!hasPerms) {
            String[] permsString = new String[permsArray.size()];
            for (int i = 0; i < permsArray.size(); i++) {
                permsString[i] = permsArray.get(i);
            }
            ActivityCompat.requestPermissions(BaseActivity.this, permsString, 99);
            return false;
        } else
            return true;
    }

    public void loge(String string) {
        if (BuildConfig.DEBUG)
            Log.e("BaseActivity", string);
    }

    public void handleFailureCodes(int code) {
        if (code == ERROR_HTTP_SERVER_ERROR) {
            showToast(getString(R.string.problem_connecting_to_the_server));
        } else if (code == ERROR_CODE_INTERNET_CONNECTION) {
            showToast("ERROR: " + code + getString(R.string.connecting_to_internet));
        } else if (code == ERROR_CODE_CONNECTION_FAILED) {
            showToast("ERROR: " + code + getString(R.string.connection_failed));
        } else if (code == ERROR_HTTP_BAD_REQUEST) {
            showToast("ERROR: " + code + getString(R.string.bad_request));
        } else if (code == ERROR_HTTP_NOT_FOUND) {
            showToast("ERROR: " + code + getString(R.string.not_found_please));
        } else if (code == ERROR_CODE_RESPONSE_NULL) {
            showRetryDialog();
            showToast("ERROR: " + code + getString(R.string.request_timeout_slow_connection));
        }
        if (code == ERROR_HTTP_FORBIDDEN || code == ERROR_HTTP_URL_REDIRECTION) {
            showToast("ERROR: " + code + getString(R.string.session_timeout_redirecting));
            syncManager.setLoginStatus(null);
            syncManager.handleCheck(false);
            gotoLoginScreen();
        }
    }

    private void showRetryDialog() {
        if (retryDialog == null || !retryDialog.isShowing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.request_timed_out_retry);
            builder.setCancelable(false)
                    .setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            exitFromApp();
                        }
                    })
                    .setNegativeButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            restartApp();
                        }
                    });
            retryDialog = builder.create();
            retryDialog.show();
        }
    }

    private void restartApp() {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permGrantedBool = false;
        switch (requestCode) {
            case 99:
                for (int grantResult : grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        showToast(getString(R.string.not_sufficient_permissions)
                                + getString(R.string.app_name)
                                + getString(R.string.permissionss));
                        permGrantedBool = false;
                        break;
                    } else {
                        permGrantedBool = true;
                    }
                }
                if (permCallback != null) {
                    if (permGrantedBool) {
                        permCallback.permGranted(reqCode);
                    } else {
                        permCallback.permDenied(reqCode);
                    }
                }
                break;
        }
    }

    public void setPermCallback(PermCallback permCallback) {
        this.permCallback = permCallback;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Const.GALLERY_REQ && resultCode == RESULT_OK) {
            inputUri = data.getData();
            if (imageUtils.isCrop()) {
                File file = new File("" + getCacheDir() + System.currentTimeMillis() + ".jpg");
                outputUri = Uri.fromFile(file);
                imageUtils.crop();
            } else {
                imageUtils.sendBackImagePath(inputUri);
            }
        } else if (requestCode == Const.CAMERA_REQ && resultCode == RESULT_OK) {
            if (imageUtils.isCrop()) {
                File file = new File("" + getCacheDir() + System.currentTimeMillis() + ".jpg");
                outputUri = Uri.fromFile(file);
                imageUtils.crop();
            } else {
                imageUtils.sendBackImagePath();
            }

        } else if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            imageUtils.sendBackImagePath(outputUri);
        }
    }


    public void exitFromApp() {
        finish();
        finishAffinity();
    }

    public String getUniqueDeviceId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null
                && activeNetworkInfo.isConnectedOrConnecting();

    }

    public boolean isValidMail(String email) {
        return email.matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
    }

    public void keyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                log("KeyHash:>>>>>>>>>>>>>>>" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void log(String string) {
        if (BuildConfig.DEBUG)
            Log.e("BaseActivity", string);
    }

    private void progressDialog() {
        fullscreenProgressDialog = new Dialog(this);
        View view = View.inflate(this, R.layout.progress_dialog, null);
        fullscreenProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fullscreenProgressDialog.setContentView(view);
        fullscreenProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialogTV = (TextView) view.findViewById(R.id.txtMsgTV);
        fullscreenProgressDialog.setCancelable(false);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSyncStart(boolean isNetworkAvail) {
        if (!isNetworkAvail)
            showToast(getString(R.string.no_network_avail));
        else if (!fullscreenProgressDialog.isShowing() && !isFinishing()) {
            fullscreenProgressDialog.show();
        }
    }

    @Override
    public void onSyncFinish() {
        dismissProgressDialog();
    }

    @Override
    public void onSyncFailure(int code) {
        log("Error code: " + code);
        handleFailureCodes(code);
    }

    @Override
    public void onSyncForbidden(int code, String string) {
        dismissProgressDialog();

        showToast(getString(R.string.session_expired));
        gotoLogin();
    }

    private void dismissProgressDialog() {
        if (fullscreenProgressDialog.isShowing() && !isFinishing()) {
            fullscreenProgressDialog.dismiss();
        }
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {

        if (action.equalsIgnoreCase("check") && controller.equalsIgnoreCase("user")) {
            if (status) {
                syncManager.handleCheck(true);
                if (jsonObject.has("journey")) {

                } else {
                    gotoMain();
                }
            } else {
                gotoLoginScreen();
            }
        }
    }

    public void gotoLoginScreen() {
        Intent intent = new Intent(BaseActivity.this, LoginRegisterActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onSyncProgress(long progress, long length) {

    }

    @Override
    public void onSyncFailure(int statusCode, Header[] headers, String responseString, Throwable arg0) {
        log("Code: " + statusCode + " Response: " + Html.fromHtml(responseString));
        showToast(getString(R.string.server_unavailable));
        log(statusCode + "");
        if (statusCode == 403) {
            gotoLogin();
        } else {
            showToast(getString(R.string.problem_connecting_to_server_please_try_again));
        }
        if (fullscreenProgressDialog.isShowing()) {
            fullscreenProgressDialog.dismiss();
        }
    }

    @Override
    public void onSyncProgress(int i, int i1) {

    }

    public void showToast(String msg) {
        showToast(msg, Toast.LENGTH_SHORT);
    }

    public void showToast(String msg, int length) {
        toast.setDuration(length);
        toastMsgTV.setText(msg);
        toast.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.orange));
        toast.show();
    }

    private void strictModeThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void transitionSlideInHorizontal() {
        this.overridePendingTransition(R.anim.slide_in_right,
                R.anim.slide_out_left);
    }

    @Override
    public void onClick(View v) {

    }

    public void settitle(String title) {
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setTitle(title);

    }

    public void gotoForgotPassword() {
        startActivity(new Intent(BaseActivity.this, ForgotPasswordActivity.class));
    }

    public interface PermCallback {
        void permGranted(int resultCode);

        void permDenied(int resultCode);
    }


    private SinchService.SinchServiceInterface mSinchServiceInterface;


    public void onService_Connected() {
        // for subclasses
    }

    public void onService_Disconnected() {
        // for subclasses
    }

    public SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }


    //utk: MQTT connection

    public void connectMqttClient() {


        MqttConnectOptions conOpt = new MqttConnectOptions();

        String server = "onlinetaxi.co.za";
//        String clientId = UUID.randomUUID().toString();
        String clientId = MqttClient.generateClientId();

        // int port = 1883;
        int port = 9004;
        boolean cleanSession = true;

        boolean ssl = true;
        String ssl_key = "onlinetaxi.bks";
        String uri = null;
        if (ssl) {
            Log.e("SSLConnection", "Doing an SSL Connect");
            uri = "ssl://";
        } else {
            uri = "tcp://";
        }
        uri = uri + server + ":" + port;
        SessionManager.setMqttURI(this, uri);
        SessionManager.setMqttclientID(this, clientId);
        log("uri>>>>>>> " + uri);
        mqttAndroidClient = createClient(this, uri, clientId);
        if (ssl) {
            try {
                if (!ssl_key.equalsIgnoreCase("")) {

                    InputStream input = this.getResources().openRawResource(R.raw.onlinetaxi);
                    conOpt.setSocketFactory(mqttAndroidClient.getSSLSocketFactory(input,
                            "adnaan"));
                }

            } catch (MqttSecurityException e) {
                Log.e(this.getClass().getCanonicalName(),
                        "MqttException Occured: ", e);
            }
        }

        // create a client handle
        String clientHandle = uri + clientId;

        // last will message
        String message = "";
        String topic = "";
        Integer qos = ActivityConstants.defaultQos;
        Boolean retained = false;

        // connection options
        String username = "onlinetaxi_mqtt";
        String password = "online_taxi_mqtt_password";


        int timeout = 100;
        int keepalive = 600;

        Log.e("Connection", clientHandle + "");
//          connection = new Connection(clientHandle, clientId, server, port,this, mqttAndroidClient, true);
        connection = new Connection(clientHandle, clientId, server, port, this, mqttAndroidClient, ssl);
        connection.registerChangeListener(changeListener);

        // connectMqttClient client

        String[] actionArgs = new String[1];
        actionArgs[0] = clientId;
        connection.changeConnectionStatus(Connection.ConnectionStatus.CONNECTING);

        conOpt.setCleanSession(cleanSession);
        conOpt.setConnectionTimeout(timeout);
        conOpt.setKeepAliveInterval(keepalive);
        if (!username.equals(ActivityConstants.empty)) {
            conOpt.setUserName(username);
        }
        if (!password.equals(ActivityConstants.empty)) {
            conOpt.setPassword(password.toCharArray());
        }

        final ActionListener callback = new ActionListener(this,
                ActionListener.Action.CONNECT, clientHandle, actionArgs);

        boolean doConnect = true;

        if ((!message.equals(ActivityConstants.empty))
                || (!topic.equals(ActivityConstants.empty))) {
            // need to make a message since last will is set
            try {
                conOpt.setWill(topic, message.getBytes(), qos.intValue(),
                        retained.booleanValue());
            } catch (Exception e) {
                Log.e(this.getClass().getCanonicalName(), "Exception Occured", e);
                doConnect = false;
                callback.onFailure(null, e);
            }
        }
        mqttAndroidClient.setCallback(new MqttCallbackHandler(this, clientHandle));


        //set traceCallback
        mqttAndroidClient.setTraceCallback(new MqttTraceCallback());

        connection.addConnectionOptions(conOpt);


        AppController.getInstance().setConnection(connection);
        if (doConnect) {
            try {
                IMqttToken iMqttToken = mqttAndroidClient.connect(conOpt, this, callback);
                log("mqtt token: " + iMqttToken);
            } catch (MqttException e) {
                Log.e(this.getClass().getCanonicalName(),
                        "MqttException Occured", e);
            }
        }

    }

    public void disconnectMQTT() {
        try {
            IMqttToken disconToken = mqttAndroidClient.disconnect();
            disconToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // we are now successfully disconnected
                    log("MQTT disconnected");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    // something went wrong, but probably we are disconnected anyway
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public MqttAndroidClient createClient(Context context, String serverURI, String clientId) {
        return new MqttAndroidClient(context, serverURI, clientId);
    }

    private class ChangeListener implements PropertyChangeListener {

        /**
         * @see PropertyChangeListener#propertyChange(PropertyChangeEvent)
         */
        @Override
        public void propertyChange(PropertyChangeEvent event) {

            if (event.getPropertyName().equals(ActivityConstants.ConnectionStatusProperty)) {
                log("Connection status>>>>>>>>>> " + connection.toString());
                Toast.makeText(getApplicationContext(), "Connection status>>>>>>>>>>>>" + connection.toString(), Toast.LENGTH_LONG).show();

                if (connection.isConnected()) {
                    if (myMqttConnectedCallback != null)
                        myMqttConnectedCallback.onMqttConnected();
                }
            } else if (event.getPropertyName().equals(ActivityConstants.returnedDataProperty)) {
                MqttMessage mqttMessage = (MqttMessage) event.getNewValue();
                log("msging event: " + mqttMessage.toString());
                mapMarkerInterface.OnLoadedMarkerData(mqttMessage);
            }
        }

    }


    /*================login Api==================*/
    private PhoneLoginTask phoneLoginTask;
    String GPS_Status = "OFF";

    public void loginAPI(String passingLatitude, String passingLongitude, String email, String password, boolean isFrstLogin, String uid) {
        this.isFirstLogin = isFrstLogin;
        MyLocationListener locationListener = new MyLocationListener(this);
        if (locationListener.getGpsStatus() == true) {
            GPS_Status = "ON";
        }

        phoneLoginTask = new PhoneLoginTask();
        phoneLoginTask.execute(uid,
                SessionManager.getUserCountryCode(this),
                ApplicationGlob.Get_Device_Brand(),
                ApplicationGlob.Get_Device_Manufacturer(),
                ApplicationGlob.Get_DeviceModel(),
                SessionManager.getUserDeviceToken(this),
                passingLatitude,
                passingLongitude,
                GPS_Status,
                email,
                password
        );
    }


    private class PhoneLoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isFirstLogin)
                progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_user_login.php";

                params.put("uid", paramsa[0]);
                params.put("pisocode", paramsa[1]);
                params.put("pbrand", paramsa[2]);
                params.put("pmanufacturer", paramsa[3]);
                params.put("pmodel", paramsa[4]);
                params.put("pdeviceid", paramsa[5]);
                params.put("platitude", paramsa[6]);
                params.put("plongitude", paramsa[7]);
                params.put("pgps_status", paramsa[8]);
                params.put("pos_version", String.valueOf(android.os.Build.VERSION.SDK_INT));
                params.put("pip_address", "");
                params.put("pwebsite", "");
                params.put("pgender", "");
                params.put("plastname", "");
                params.put("pfirstname", "");
                params.put("psocial", "");
                params.put("email", paramsa[9]);
                params.put("Password", paramsa[10]);
                params.put("action", "action_user_login");
                try {
                    HttpUtility.sendPostRequest(requestURL, params);
                    response = HttpUtility.readInputStreamToString();
                    log(requestURL + "  " + params);
                    Log.e("OT Login Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                    Log.e("Exception", "" + ex);
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            if (isFirstLogin)
                progressbar(false);


            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");

                if (returncode.equals("0")) {

                    JSONObject user_data_Object = jsoObject1.getJSONObject("user_data");
                    JSONObject transaction = user_data_Object.getJSONObject("transaction");


                    //   ToastOnUiThread("" + user_data_Object);
                    //     ToastOnUiThread("" + user_data_Object);

                    String Userid = user_data_Object.getString("uid");
                    String UserFirstName = user_data_Object.getString("fname");
                    String UserLastName = user_data_Object.getString("lnme");
                    String UserEmail = user_data_Object.getString("email");
                    String UserContact = user_data_Object.getString("cellno");
                    String country = user_data_Object.getString("country");

                    JSONObject settingObject = user_data_Object.getJSONObject("settings");

                    String haversine_km = settingObject.getString("haversine_km");
                    String availability = settingObject.getString("availability ");
                    String refresh = settingObject.getString("refresh");
                    String driverratekm = settingObject.getString("driverratekm");

                    String UserStatus = user_data_Object.optString("status");
                    String UserType = user_data_Object.optString("usertype");

                    String HomeAddress = user_data_Object.optString("home_address");
                    String UserZipCode = user_data_Object.optString("zipcode");
                    String UserCity = user_data_Object.optString("city");
                    String UserState = user_data_Object.optString("State");
                    String Usercountry = user_data_Object.optString("country");

                    String state_topic = settingObject.optString("state_topic");
                    SessionManager.setState_Topic(BaseActivity.this, state_topic);

                    String refresh_value = settingObject.optString("refresh");
                    SessionManager.setMapRefreshTime(BaseActivity.this, refresh_value);

                    String radius = settingObject.optString("radius");
                    SessionManager.setDriverRadius(BaseActivity.this, radius);

                    SessionManager.setUserAddress(BaseActivity.this, HomeAddress + " " + UserCity + " " + UserState + " " + Usercountry);

                    String MillageAmount = transaction.getString("millageamount");
                    SessionManager.setMillageAmount(BaseActivity.this, MillageAmount);

                    String userscurrency = transaction.getString("userscurrency");
                    SessionManager.setUsersCurrency(BaseActivity.this, userscurrency);

                    SessionManager.setUserStatus(BaseActivity.this, UserStatus);
                    SessionManager.setUserType(BaseActivity.this, UserType);

                    SessionManager.setUserID(BaseActivity.this, Userid);
                    SessionManager.setUserFirstName(BaseActivity.this, UserFirstName);
                    SessionManager.setUserLastName(BaseActivity.this, UserLastName);
                    SessionManager.setUserEmail(BaseActivity.this, UserEmail);
                    SessionManager.setUserContactNumber(BaseActivity.this, UserContact);
                    SessionManager.setUserAvailability(BaseActivity.this, availability);
                    SessionManager.setHavesine_KM(BaseActivity.this, haversine_km);
                    SessionManager.setMapRefreshTime(BaseActivity.this, refresh);
                    SessionManager.setDriverRatePerKM(BaseActivity.this, driverratekm);
                    SessionManager.setUserState(BaseActivity.this, UserState);
                    SessionManager.setUsercity(BaseActivity.this, UserCity);
                    SessionManager.setUserZipcode(BaseActivity.this, UserZipCode);


                    if (!getSinchServiceInterface().isStarted()) {

                        if (!Userid.isEmpty() && Userid != null) {
                            getSinchServiceInterface().startClient(Userid);
                            showToast("sinch service started ");
                          /*  islogedin = true;
                            connectMqttClient();*/
                        }
                    }
                    if (isFirstLogin) {
                        Intent redirect = new Intent(BaseActivity.this, MainActivity.class);
                        startActivity(redirect);
                    } else {
                        if (mainActivity != null) {
                            Fragment fragment = mainActivity.getSupportFragmentManager().findFragmentById(R.id.frame_container);
                            if (fragment instanceof PassengerMapAndClassicFragment) {
                                ((PassengerMapAndClassicFragment) fragment).refreshMap();
                            }
                        }
                    }


                } else {
                    String excptn = jsoObject1.getString("message");
                    ToastOnUiThread(excptn);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void ToastOnUiThread(final String message) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    Dialog alertDialogProgressBar;

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(this,
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }

    public void setMyMqttConnectedCallback(MyMqttConnectedCallback myMqttConnectedCallback) {
        this.myMqttConnectedCallback = myMqttConnectedCallback;
    }

    public double HaversineFormula(double lat1, double long1, double lat2, double long2) {
        double R = 6371; // earth’s radius (mean radius = 6,371km)
        double a;
        double dLat = Math.toRadians(lat2 - lat1);

        double dLon = Math.toRadians(long2 - long1);
        a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double result = R * c;

        log("Haversine Distance>>>>>>>>>>> " + String.valueOf(result));

        return result;
    }

}
