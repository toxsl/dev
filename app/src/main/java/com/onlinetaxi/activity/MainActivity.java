package com.onlinetaxi.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.onlinetaxi.R;
import com.onlinetaxi.data.DriverProfileData;
import com.onlinetaxi.fragments.AccountsFragment;
import com.onlinetaxi.fragments.FragmentFavorite;
import com.onlinetaxi.fragments.HistoryFragment;
import com.onlinetaxi.fragments.PassengerMapAndClassicFragment;
import com.onlinetaxi.fragments.ProfileFragment;
import com.onlinetaxi.fragments.PromotionsFragment;
import com.onlinetaxi.fragments.driver.CompanyHistory;

import com.onlinetaxi.fragments.driver.DriverMapAndClassicFragment;
import com.onlinetaxi.fragments.driver.DriverPromotionFragment;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.fragments.driver.DriverCompenyFragment;
import com.onlinetaxi.fragments.driver.HistoryNotAdminDriverFragment;
import com.onlinetaxi.fragments.driver.ManageDriverFragment;
import com.onlinetaxi.fragments.driver.DriverProfileFragment;

import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.mqtt.MqttAndroidClient;
import com.onlinetaxi.utils.Const;
import com.onlinetaxi.video.SinchService;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView nvDrawer;
    public static DrawerLayout mDrawer;
    private View header;
    private ActionBarDrawerToggle toggle;
    Dialog alertDialogProgressBar;
    private CheckTask checkTask;
    private TextView userNameTV;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;
        initUI();
        connectMqttClient();
        gotoHomefragment();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private void gotoHomefragment() {

//        if (!store.getString(Const.USER_TYPE).isEmpty()) {

        if (!SessionManager.getUserStatus(getApplicationContext()).isEmpty()) {
//            if (store.getString(Const.USER_TYPE).equals(Const.USER_PASSENGER)) {
            if (SessionManager.getUserType(getApplicationContext()).equals(Const.USER_PASSENGER)) {
                nvDrawer.getMenu().clear();
                nvDrawer.inflateMenu(R.menu.menus_for_passenger);
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new PassengerMapAndClassicFragment()).commit();
            } /*else if (store.getString(Const.USER_TYPE).equals(Const.USER_DRIVER) && store.getString(Const.USER_STATUS).equals("NC")) {*/
            else if (SessionManager.getUserType(getApplicationContext()).equals(Const.USER_DRIVER) &&
                    SessionManager.getUserStatus(getApplicationContext()).equals("NC")) {
                nvDrawer.getMenu().clear();
                nvDrawer.inflateMenu(R.menu.menus_for_driver);
                checkTask = new CheckTask();
                checkTask.execute(SessionManager.getUserCountryCode(getApplicationContext()), SessionManager.getUserID(getApplicationContext()));

                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new DriverCompenyFragment()).commit();
            } /*else if (store.getString(Const.USER_TYPE).equals(Const.USER_DRIVER) && store.getString(Const.USER_STATUS).equals("A"))*/ else if (SessionManager.getUserType(getApplicationContext()).equals(Const.USER_DRIVER) &&
                    SessionManager.getUserStatus(getApplicationContext()).equals("A")) {
                nvDrawer.getMenu().clear();
                nvDrawer.inflateMenu(R.menu.menus_for_driver);
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new DriverMapAndClassicFragment()).commit();
                checkTask = new CheckTask();
                checkTask.execute(SessionManager.getUserCountryCode(getApplicationContext()), SessionManager.getUserID(getApplicationContext()));

            } else {
                Intent intent = new Intent(MainActivity.this, LoginRegisterActivity.class);
                startActivity(intent);

            }
        } else {
            Intent intent = new Intent(MainActivity.this, LoginRegisterActivity.class);
            startActivity(intent);

        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    private class CheckTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_drivers_profile.php";
                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("submit", "ot_drivers_profile_proc");

                log(requestURL + "  " + params);
                try {
                    HttpUtility.sendPostRequest(requestURL, params);
                    log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();


                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);

            Log.i("result=", " " + result);
            try {
                if (result != null && !result.isEmpty() && result != " ") {
                    log(result);
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray Response = jsonObject.getJSONArray("Response");

                    JSONObject object = Response.getJSONObject(0);
                    JSONObject company_data = object.getJSONObject("company_data");
                    JSONObject vehicle_data = company_data.getJSONObject("vehicle_data");
                    JSONObject priviledges = company_data.getJSONObject("priviledges");

                    JSONObject driver_Data = company_data.getJSONObject("driver_data");

                    store.saveString("taxi_admin", priviledges.optString("taxi_admin"));
                    store.saveString("taxi_opera", priviledges.optString("taxi_opera"));

                    DriverProfileData driverProfileData = new DriverProfileData(Parcel.obtain());
                    driverProfileData.DriverCompanyAddress = company_data.optString("company_address");
                    driverProfileData.DriverCompanyEmail = company_data.optString("company_email");
                    driverProfileData.DriverCompanyTelno = company_data.optString("company_telno");
                    driverProfileData.DrivrCompnyCode = company_data.optString("companycode");
                    driverProfileData.DrivrCompnylogo = company_data.optString("logo");
                    driverProfileData.DriverCompanyName=company_data.optString("company_name");

                    driverProfileData.DriverCode = driver_Data.optString("userid");
                    driverProfileData.DriverType = "";
                    driverProfileData.DriverFirstName = driver_Data.optString("firstname");
                    driverProfileData.Driverlastname = driver_Data.optString("lastname");
                    driverProfileData.Rating = driver_Data.optString("rating");
                    driverProfileData.Status = driver_Data.optString("status");


                    driverProfileData.DriverPeakRate = vehicle_data.optString("rate_km_peak");
                    driverProfileData.DriverOffPeakRAte = vehicle_data.optString("rate_km_off_peak");
                    driverProfileData.DriverVehicleSeat = vehicle_data.optString("vehicle_seat");
                    driverProfileData.Veahicle = vehicle_data.optString("vehicle");

                    driverProfileData.coverage=vehicle_data.optString("coverage");
                    driverProfileData.locality=vehicle_data.optString("locality");

                    SessionManager.setDriverProfileData(driverProfileData, MainActivity.this);


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void ToastOnUiThread(final String message) {
            runOnUiThread(new Runnable() {
                public void run() {
                    /*Toast.makeText(getActivity().getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();*/
                }
            });

        }

    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(this,
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }

    private void initUI() {
        nvDrawer = (NavigationView) findViewById(R.id.nvDrawer);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        header = nvDrawer.inflateHeaderView(R.layout.profile_header_layout);


        userNameTV = (TextView) header.findViewById(R.id.userNameTV);
        userNameTV.setText(SessionManager.getUserFirstName(getApplicationContext()) + " " + SessionManager.getUserLastName(getApplicationContext()));

        nvDrawer.setNavigationItemSelectedListener(this);
        toggle = new ActionBarDrawerToggle(this, mDrawer, null,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                hideSoftKeyboard(MainActivity.this);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                invalidateOptionsMenu();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                hideSoftKeyboard(MainActivity.this);
            }
        };

        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        checkTask = new CheckTask();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment mFragment = null;
        mDrawer.closeDrawers();
        switch (item.getItemId()) {
            case R.id.home:
                gotoHomefragment();
                break;
            case R.id.profile:
                mFragment = new ProfileFragment();
                break;
            case R.id.history:
                mFragment = new HistoryFragment();
                break;
            case R.id.favorite:
                mFragment = new FragmentFavorite();
                // mFragment = new FavorieDriverFragment();
                break;
            case R.id.promotion:
                mFragment = new PromotionsFragment();
                break;
            case R.id.account:
                mFragment = new AccountsFragment();
                break;
//
            case R.id.logout:
                SessionManager.setUserID(getApplicationContext(), "UserID");

                if (getSinchServiceInterface().isStarted()) {
                    getSinchServiceInterface().stopClient();
                    getApplicationContext().unbindService(serviceConnection);
                }
                gotoLogin();

                disconnectMQTT();

                break;

            case R.id.profile_driver:
                mFragment = new DriverProfileFragment();

                break;
            case R.id.history_driver:
                if (store.getString("taxi_admin").equals("1"))
                    mFragment = new CompanyHistory();
                else if (store.getString("taxi_admin").equals("0") && store.getString("taxi_opera").equals("1"))
                    mFragment = new HistoryNotAdminDriverFragment();
                break;
            case R.id.manage_driver:
                mFragment = new ManageDriverFragment();
                break;
            case R.id.promotion_driver:
                mFragment = new DriverPromotionFragment();
                break;
            case R.id.account_driver:
                break;

        }
        if (mFragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, mFragment).commit();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        hideSoftKeyboard(this);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragment instanceof DriverMapAndClassicFragment || fragment instanceof PassengerMapAndClassicFragment) {
            showExit();
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else
            gotoHomefragment();
    }


}
