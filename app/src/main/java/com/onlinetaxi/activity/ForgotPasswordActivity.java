package com.onlinetaxi.activity;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;

import com.onlinetaxi.R;

public class ForgotPasswordActivity extends BaseActivity {

    private FloatingActionButton submitFAB;
    private TextView emailTVF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
    }

    private void init() {

        submitFAB = (FloatingActionButton) findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
        emailTVF= (TextView) findViewById(R.id.emailTVF);
        emailTVF.setTypeface(iconFont);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:
                gotoMain();

                break;
        }
    }
}
