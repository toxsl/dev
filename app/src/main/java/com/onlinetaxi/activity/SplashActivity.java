package com.onlinetaxi.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.onlinetaxi.R;
import com.onlinetaxi.fragments.LoginFragment;
import com.onlinetaxi.helper.AppController;
import com.onlinetaxi.helper.AppLocationService;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.LocationAddress;
import com.onlinetaxi.helper.MyLocationListener;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.mqtt.ActionListener;
import com.onlinetaxi.mqtt.ActivityConstants;

import com.onlinetaxi.mqtt.Connection;
import com.onlinetaxi.mqtt.MqttAndroidClient;
import com.onlinetaxi.mqtt.MqttCallbackHandler;
import com.onlinetaxi.mqtt.MqttTraceCallback;
import com.onlinetaxi.utils.Const;

import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SplashActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BaseActivity.PermCallback {

    ProgressDialog pDialog;
    String CountryCodeFromGeo;
    AppLocationService appLocationService;
    CircularProgressView mprogressBar;
    protected GoogleApiClient mGoogleApiClient;
    Dialog alertDialogProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        store.saveString(Const.DEVICE_TOKEN, null);
        setContentView(R.layout.activity_splash);
        setPermCallback(this);


        appLocationService = new AppLocationService(SplashActivity.this);

        if (checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99)) {
            buildGoogleApiClient();

          //  initFCM();
            getCountryCodeFromGeocode();
        }

    }


    protected synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();


    }


    @Override
    public void onBackPressed() {
        exitFromApp();
    }


    public void getIsoCodeFromIpAPI() {


        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        String url = "http://ip-api.com/json";


        progressbar(true);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Respose :", response.toString());


                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            String Country = jsonObject.getString("country");
                            String CountryCode = jsonObject.getString("countryCode");
                            String Latitude = jsonObject.getString("lat");
                            String Longitude = jsonObject.getString("lon");
                            SessionManager.setUserCountryCode(getApplicationContext(), CountryCode);
                            if (CountryCodeFromGeo.toUpperCase().equals(CountryCode.toUpperCase())) {

                                Toast.makeText(getApplicationContext(), "ISO Code from IP_API: " + CountryCode, Toast.LENGTH_LONG).show();

                                SessionManager.setUserCountryCodeFromIPApi(getApplicationContext(), Country);
                                SessionManager.setUserCountryCode(getApplicationContext(), CountryCode);
                                SessionManager.setUserSplashLatitude(getApplicationContext(), Latitude);
                                SessionManager.setUserSplashLongitude(getApplicationContext(), Longitude);


                            } else if (CountryCodeFromGeo.equals("UserCountryCodeFromIPApi") || CountryCodeFromGeo.equals("null") || CountryCodeFromGeo.equals("Unable to get address for this lat-long")) {

                                Toast.makeText(getApplicationContext(), "ISO Code from IP_API: " + CountryCode, Toast.LENGTH_LONG).show();
                                SessionManager.setUserCountryCode(getApplicationContext(), CountryCode);
                                SessionManager.setUserSplashLatitude(getApplicationContext(), Latitude);
                                SessionManager.setUserSplashLongitude(getApplicationContext(), Longitude);

                            } else {
                                Location location = appLocationService.getLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    double latitude = location.getLatitude();
                                    double longitude = location.getLongitude();
                                    LocationAddress locationAddress = new LocationAddress();
                                    locationAddress.getAddressFromLocation(latitude, longitude,
                                            getApplicationContext(), new GeocoderHandler());

                                } else {

                                    getIsoCodeFromFreeGeoAPI();
                                }


                                //  Toast.makeText(getApplicationContext(), "Country not equal", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //   dismissProgressBar();

                        goToNext();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Response", "Error: " + error.getMessage());
                // hide the progress dialog

                progressbar(false);
                //  pDialog.hide();
            }
        });

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }


    public void getIsoCodeFromFreeGeoAPI() {


        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        String url = "http://freegeoip.net/json/";


        progressbar(true);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Respose :", response.toString());


                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            String Country = jsonObject.getString("country_name");
                            String CountryCode = jsonObject.getString("country_code");
                            String Latitude = jsonObject.getString("latitude");
                            String Longitude = jsonObject.getString("longitude");

                            if (CountryCodeFromGeo.toUpperCase().equals(CountryCode.toUpperCase())) {

                                Toast.makeText(getApplicationContext(), "ISO Code from freeGeo :" + CountryCode, Toast.LENGTH_LONG).show();
                                SessionManager.setUserCountryCode(getApplicationContext(), CountryCode);
                                SessionManager.setUserSplashLatitude(getApplicationContext(), Latitude);
                                SessionManager.setUserSplashLongitude(getApplicationContext(), Longitude);

                            } else {

                                turnOnGPS_Dialog();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Response", "Error: " + error.getMessage());
                // hide the progress dialog

                progressbar(false);

            }
        });

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


    }

    public void getCountryCodeFromGeocode() {
        Location location = appLocationService.getLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude,
                    getApplicationContext(), new GeocoderHandler());
        } else {

            if (SessionManager.getUserCountryCodeFromGPS(getApplicationContext()).equals("UserCountryCodeFromGPS")) {

                CountryCodeFromGeo = SessionManager.getUserCountryCodeFromIPApi(getApplicationContext());

            } else {

                CountryCodeFromGeo = SessionManager.getUserCountryCodeFromGPS(getApplicationContext());
            }
            getIsoCodeFromIpAPI();

        }


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void permGranted(int resultCode) {
        buildGoogleApiClient();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
        initFCM();
        getCountryCodeFromGeocode();

    }

    @Override
    public void permDenied(int resultCode) {
        exitFromApp();
    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }

            CountryCodeFromGeo = locationAddress;

            if (CountryCodeFromGeo.equals("null") || CountryCodeFromGeo == null || CountryCodeFromGeo.equals("Unable to get address for this lat-long")) {

                if (ApplicationGlob.isConnectingToInternet(SplashActivity.this)) {

                    getIsoCodeFromIpAPI();

                } else {
                    ApplicationGlob.ToastShowInterNetConnection(getApplicationContext(), SplashActivity.this);
                }

            } else {

                Toast.makeText(getApplicationContext(), "ISO Code From GPS : " + CountryCodeFromGeo, Toast.LENGTH_SHORT).show();

                SessionManager.setUserCountryCodeFromGPS(getApplicationContext(), CountryCodeFromGeo);
                SessionManager.setUserCountryCode(getApplicationContext(), CountryCodeFromGeo);


                if (SessionManager.getUserCountryCode(getApplicationContext()).equals("UserCountryCode")) {

                    getIsoCodeFromIpAPI();
                } else {

                    goToNext();
                }


            }


        }
    }

    public void goToNext() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                if (SessionManager.getUserID(getApplicationContext()).equals("UserID") || SessionManager.getUserID(getApplicationContext()).equals("")) {

                    gotoLogin();
                } else {

                    if (ApplicationGlob.isConnectingToInternet(SplashActivity.this)) {

                        String passingLatitude, passingLongitude;
                        String GPS_Status = "OFF";
                        MyLocationListener locationListener = new MyLocationListener(SplashActivity.this);

                        if (SessionManager.getUserSplashLatitude(SplashActivity.this).equals("UserSplashLatitude") || SessionManager.getUserSplashLongitude(SplashActivity.this).equals("UserSplashLongitude")) {
                            passingLatitude = locationListener.getCurrentLatitude();
                            passingLongitude = locationListener.getLocationLongitude();

                            SessionManager.setLatitudeFromLocationListner(getApplicationContext(), passingLatitude);
                            SessionManager.setLongitudeFromLocationListner(getApplicationContext(), passingLongitude);
                        } else {
                            passingLatitude = SessionManager.getUserSplashLatitude(SplashActivity.this);
                            passingLongitude = SessionManager.getUserSplashLongitude(SplashActivity.this);

                        }

                        if (locationListener.getGpsStatus() == true) {
                            GPS_Status = "ON";
                        }

                        loginAPI(passingLatitude, passingLongitude,"","",true,SessionManager.getUserID(getApplicationContext()));

                        /*phoneLoginTask = new PhoneLoginTask();
                        phoneLoginTask.execute(SessionManager.getUserID(getApplicationContext()),
                                SessionManager.getUserCountryCode(SplashActivity.this),
                                ApplicationGlob.Get_Device_Brand(),
                                ApplicationGlob.Get_Device_Manufacturer(),
                                ApplicationGlob.Get_DeviceModel(),
                                SessionManager.getUserDeviceToken(SplashActivity.this),
                                passingLatitude,
                                passingLongitude,
                                GPS_Status
                        );
*/

                    }

                }

            }
        }, 5000);


    }


    public void turnOnGPS_Dialog() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient = new GoogleApiClient.Builder(SplashActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(SplashActivity.this)
                    .addOnConnectionFailedListener(SplashActivity.this).build();
            mGoogleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result1 =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result1.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result1) {
                    final com.google.android.gms.common.api.Status status = result1.getStatus();
                    final LocationSettingsStates state = result1.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        SplashActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }


    }


    /*public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(SplashActivity.this,
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }*/


}
