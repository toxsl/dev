package com.onlinetaxi.activity;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.onlinetaxi.R;

public class LoginActivity extends BaseActivity {

    private Button loginBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void init() {
        loginBT = (Button) findViewById(R.id.loginBT);
        loginBT.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exitFromApp();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginBT:
                gotoLoginRegister();
                break;
        }
    }


}
