package com.onlinetaxi.video;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.helper.SessionManager;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class AudioCallScreenActivity extends BaseActivity {


    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;

    private String mCallId;
    private long mCallStart = 0;

    private TextView switchVideoTVF;
    FloatingActionButton endCallButton;
    ArrayList<HistoryData> historyDatas;

    private TextView mCallDuration;
    private TextView mCallState;
    private TextView mCallerName;
    private ImageView backButtonIV;
    private Boolean isCall;


    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            AudioCallScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_calling);

        historyDatas = SessionManager.getHistoryData(getApplicationContext());
        mAudioPlayer = new AudioPlayer(this);
        mCallDuration = (TextView) findViewById(R.id.callDuration);
        mCallerName = (TextView) findViewById(R.id.remoteUser);
        mCallState = (TextView) findViewById(R.id.callState);
        endCallButton = (FloatingActionButton) findViewById(R.id.submitFAB);
        backButtonIV = (ImageView) findViewById(R.id.backButtonIV);

        backButtonIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCall)
                    alertDialog();
                else
                    finish();
            }
        });

        endCallButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
            }
        });

        switchVideoTVF = (TextView) findViewById(R.id.switchVideoTVF);
        switchVideoTVF.setTypeface(iconFont);
        switchVideoTVF.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
                videoCallButtonClicked();
            }
        });
        mCallStart = System.currentTimeMillis();
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        super.onServiceConnected(name, service);
    }

    @Override
    public void onService_Connected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.addCallListener(new SinchCallListener());
            mCallerName.setText(call.getRemoteUserId());
            mCallState.setText(call.getState().toString());
        } else {
            showToast("Started with invalid callId, aborting.");
//            finish();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        mDurationTask.cancel();
        mTimer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
        if (isCall)
            alertDialog();
        else
            finish();
    }

    private void alertDialog() {
        new AlertDialog.Builder(getApplicationContext())
                .setTitle("End Call")
                .setMessage("Do you want to disconnect call?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        endCall();
                        finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }


    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
            isCall = false;
        }
        finish();
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (mCallStart > 0) {
            mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
        }
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            showToast("Call ended. Reason: " + cause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();
            Toast.makeText(AudioCallScreenActivity.this, endMsg, Toast.LENGTH_LONG).show();
            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            showToast("Call established");
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            mCallStart = System.currentTimeMillis();
            isCall = true;
        }

        @Override
        public void onCallProgressing(Call call) {
            showToast("Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

    }

    private void videoCallButtonClicked() {
         String userName = historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(this))).drivercode;
        //String userName = "193";
        showToast(userName);

        if (userName.isEmpty()) {
            showToast("Please enter a user to call");
            return;
        }

        Call call = getSinchServiceInterface().callUserVideo(userName);
        String callId = call.getCallId();

        Intent callScreen = new Intent(this, VideoCallScreenActivity.class);
        callScreen.putExtra(SinchService.CALL_ID, callId);
        startActivity(callScreen);
    }
}
