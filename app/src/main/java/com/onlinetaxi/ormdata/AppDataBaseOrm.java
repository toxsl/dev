package com.onlinetaxi.ormdata;

import android.content.Context;
import com.onlinetaxi.helper.SessionManager;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

public class AppDataBaseOrm extends SugarRecord<AppDataBaseOrm> implements Comparable<AppDataBaseOrm> {

    private String cardnumber;
    private String tokenid;
    private String cardType;
    private String userid;
    private String originalcardnumber;

    public AppDataBaseOrm() {

    }


    public AppDataBaseOrm(String Cardnumber, String Tokenid, String CardType, String UserID, String OriginalCArdNumber) {
        super();
        this.cardnumber = Cardnumber;
        this.tokenid = Tokenid;
        this.userid = UserID;
        this.cardType = CardType;
        this.originalcardnumber = OriginalCArdNumber;


    }

    public static void insertImformations(Context context, AppDataBaseOrm imformation) {

        imformation.save(); //stores the new user into the database
    }

    public static void DeleteImformations(Long id) {

        //stores the new user into the database
        AppDataBaseOrm information = AppDataBaseOrm.findById(AppDataBaseOrm.class, id);
        information.delete();

    }

    public static List<AppDataBaseOrm> getAllList(Context context) {

        List<AppDataBaseOrm> favouritesSugarOrmModel = new ArrayList<AppDataBaseOrm>();

        favouritesSugarOrmModel = AppDataBaseOrm.find(AppDataBaseOrm.class, "userid = ?", SessionManager.getUserID(context));

        return favouritesSugarOrmModel;
    }

    public static List<AppDataBaseOrm> getSpecificCard(String string) {

        List<AppDataBaseOrm> favouritesSugarOrmModel = new ArrayList<AppDataBaseOrm>();

        favouritesSugarOrmModel = AppDataBaseOrm.find(AppDataBaseOrm.class, "originalcardnumber = ?", string);

        return favouritesSugarOrmModel;
    }

    public String getOriginalCardnumber() {
        return originalcardnumber;
    }

    public void setOriginalCardnumber(String originalCardnumber) {
        this.originalcardnumber = originalCardnumber;
    }

    public String getUserID() {
        return userid;
    }

    public void setUserID(String userID) {
        this.userid = userID;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getTokenid() {
        return tokenid;
    }

    public void setTokenid(String tokenid) {
        this.tokenid = tokenid;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @Override
    public int compareTo(AppDataBaseOrm arg0) {
        return getId().compareTo(arg0.getId());
    }


}
