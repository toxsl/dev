package com.onlinetaxi.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by TOXSL\himanshu.gulati on 29/9/16.
 */
public class HistoryData implements Serializable{
    public String pickup;
    public String DropOff;
    public String  paymentmethod;
    public String date;
    public String currencySymbol;
    public String amount;
    public String reference;



    /*-------------driver------------------*/

    public String drivercode;
    public String driverRating;
    public  String drivername;

}
