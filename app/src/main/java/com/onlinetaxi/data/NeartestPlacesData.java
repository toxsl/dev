package com.onlinetaxi.data;

/**
 * Created by TOXSL\jyotpreet.kaur on 29/9/16.
 */
public class NeartestPlacesData {
    public String id;
    public String locationName;
    public double lati;
    public double longi;
    public String vicinity;
    public double distance;
}
