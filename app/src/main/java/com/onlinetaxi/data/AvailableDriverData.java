package com.onlinetaxi.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by TOXSL/utkarsh.shukla on 6/12/16.
 */

public class AvailableDriverData implements Serializable {
    public String isocode;
    public String driverlatitude;
    public String driverlongitude;
    public String companyCode;
    public String companyName;
    public String driverlogo;
    public String drivercode;
    public String driverContact;
    public String driverType;
    public String driverName;
    public String driverKM;
    public String drivervehicle;
    public String vehicleSeat;
    public String driverrating;
    public String driverStatus;


   /* public AvailableDriverData(Parcel in) {
        isocode = in.readString();
        driverlatitude = in.readString();
        driverlongitude = in.readString();
        companyCode = in.readString();
        companyName = in.readString();
        driverlogo = in.readString();
        drivercode = in.readString();
        driverContact = in.readString();
        driverType = in.readString();
        driverName = in.readString();
        driverKM = in.readString();
        drivervehicle = in.readString();
        vehicleSeat = in.readString();
        driverrating = in.readString();
        driverStatus = in.readString();
    }

    public static final Creator<AvailableDriverData> CREATOR = new Creator<AvailableDriverData>() {
        @Override
        public AvailableDriverData createFromParcel(Parcel in) {
            return new AvailableDriverData(in);
        }

        @Override
        public AvailableDriverData[] newArray(int size) {
            return new AvailableDriverData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(isocode);
        dest.writeString(driverlatitude);
        dest.writeString(driverlongitude);
        dest.writeString(companyCode);
        dest.writeString(companyName);
        dest.writeString(driverlogo);
        dest.writeString(drivercode);
        dest.writeString(driverContact);
        dest.writeString(driverType);
        dest.writeString(driverName);
        dest.writeString(driverKM);
        dest.writeString(drivervehicle);
        dest.writeString(vehicleSeat);
        dest.writeString(driverrating);
        dest.writeString(driverStatus);
    }*/
}
