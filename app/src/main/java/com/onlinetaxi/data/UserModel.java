package com.onlinetaxi.data;

/**
 * Created by Android on 10/28/2016.
 */

public class UserModel {

    public String UserIsoCode;
    public String UserUniqueID;
    public String UserEmail;
    public String UserPassword;
    public String UserLatitude;
    public String UserLongitude;
    public String UserGender;
    public String UserWebsite;
    public String UserFirstName;
    public String UserLastName;
    public String UserPhoneManufacturer;
    public String UserPhoneBrand;
    public String UserOsVersion;
    public String UserPoneModel;
    public String UserDeviceId;
    public String UserAddress;
    public String UserGPS_Status;
    public String UserStatus;

    public String getUserIsoCode() {
        return UserIsoCode;
    }

    public UserModel setUserIsoCode(String userIsoCode) {
        UserIsoCode = userIsoCode;
        return this;
    }

    public String getUserUniqueID() {
        return UserUniqueID;
    }

    public UserModel setUserUniqueID(String userUniqueID) {
        UserUniqueID = userUniqueID;
        return this;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public UserModel setUserEmail(String userEmail) {
        UserEmail = userEmail;
        return this;
    }

    public String getUserPassword() {
        return UserPassword;
    }

    public UserModel setUserPassword(String userPassword) {
        UserPassword = userPassword;
        return this;
    }

    public String getUserLatitude() {
        return UserLatitude;
    }

    public UserModel setUserLatitude(String userLatitude) {
        UserLatitude = userLatitude;
        return this;
    }

    public String getUserLongitude() {
        return UserLongitude;
    }

    public UserModel setUserLongitude(String userLongitude) {
        UserLongitude = userLongitude;
        return this;
    }

    public String getUserGender() {
        return UserGender;
    }

    public UserModel setUserGender(String userGender) {
        UserGender = userGender;
        return this;
    }

    public String getUserWebsite() {
        return UserWebsite;
    }

    public UserModel setUserWebsite(String userWebsite) {
        UserWebsite = userWebsite;
        return this;
    }

    public String getUserFirstName() {
        return UserFirstName;
    }

    public UserModel setUserFirstName(String userFirstName) {
        UserFirstName = userFirstName;
        return this;
    }

    public String getUserLastName() {
        return UserLastName;
    }

    public UserModel setUserLastName(String userLastName) {
        UserLastName = userLastName;
        return this;
    }

    public String getUserPhoneManufacturer() {
        return UserPhoneManufacturer;
    }

    public UserModel setUserPhoneManufacturer(String userPhoneManufacturer) {
        UserPhoneManufacturer = userPhoneManufacturer;
        return this;
    }

    public String getUserPhoneBrand() {
        return UserPhoneBrand;
    }

    public UserModel setUserPhoneBrand(String userPhoneBrand) {
        UserPhoneBrand = userPhoneBrand;
        return this;
    }

    public String getUserOsVersion() {
        return UserOsVersion;
    }

    public UserModel setUserOsVersion(String userOsVersion) {
        UserOsVersion = userOsVersion;
        return this;
    }

    public String getUserPoneModel() {
        return UserPoneModel;
    }

    public UserModel setUserPoneModel(String userPoneModel) {
        UserPoneModel = userPoneModel;
        return this;
    }

    public String getUserDeviceId() {
        return UserDeviceId;
    }

    public UserModel setUserDeviceId(String userDeviceId) {
        UserDeviceId = userDeviceId;
        return this;
    }

    public String getUserAddress() {
        return UserAddress;
    }

    public UserModel setUserAddress(String userAddress) {
        UserAddress = userAddress;
        return this;
    }

    public String getUserGPS_Status() {
        return UserGPS_Status;
    }

    public UserModel setUserGPS_Status(String userGPS_Status) {
        UserGPS_Status = userGPS_Status;
        return this;
    }

    public String getUserStatus() {
        return UserStatus;
    }

    public UserModel setUserStatus(String userStatus) {
        UserStatus = userStatus;
        return this;
    }



}
