package com.onlinetaxi.data;



import com.onlinetaxi.R;

import java.util.ArrayList;

public class PaymentTypeModel {

    public int ID;
    public String CardType;
    public Integer ImageDrawable;

    public static ArrayList<PaymentTypeModel> getAllCardTypes() {
        ArrayList<PaymentTypeModel> arrayList = new ArrayList<PaymentTypeModel>();


        try {
            Integer[] Images = new Integer[]{R.drawable.visalogo, R.drawable.mastercard, R.drawable.americanexpress, R.drawable.dinerclub, R.drawable.jcb};

            String[] values = new String[]{"Visa", "MasterCard",
                    "American Express", "Diners Club", "JCB"};

            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < values.length; ++i) {
                list.add(values[i]);
            }


            for (int i = 0; i < list.size(); i++) {
                PaymentTypeModel paymentTypeModel = new PaymentTypeModel();


                paymentTypeModel.setID(i);
                paymentTypeModel.setCardType(values[i]);
                paymentTypeModel.setImageDrawable(Images[i]);

                arrayList.add(paymentTypeModel);
            }


        } catch (Exception e) {
            System.out.println();
            e.printStackTrace();
        }


        return arrayList;


    }

    public int getID() {
        return ID;
    }

    public void setID(int iD) {
        ID = iD;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String cardType) {
        CardType = cardType;
    }

    public Integer getImageDrawable() {
        return ImageDrawable;
    }

    public void setImageDrawable(Integer imageDrawable) {
        ImageDrawable = imageDrawable;
    }


}
