package com.onlinetaxi.data;

/**
 * Created by TOXSL/utkarsh.shukla on 8/11/16.
 */
public class ManageDriverDataModel {
    public  int id;
    public String Menu;
    public static ManageDriverDataModel  manageDriverDataModel;

    public static ManageDriverDataModel getManageDriverDataModel() {
        return manageDriverDataModel;
    }

    public static void setManageDriverDataModel(ManageDriverDataModel manageDriverDataModel) {
        ManageDriverDataModel.manageDriverDataModel = manageDriverDataModel;
    }

    public String getMenuNumber() {
        return MenuNumber;
    }

    public ManageDriverDataModel setMenuNumber(String menuNumber) {
        MenuNumber = menuNumber;
        return this;
    }

    public String getMenu() {
        return Menu;
    }

    public ManageDriverDataModel setMenu(String menu) {
        Menu = menu;
        return this;
    }

    public String MenuNumber;
}
