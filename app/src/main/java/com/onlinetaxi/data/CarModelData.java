package com.onlinetaxi.data;

/**
 * Created by Android on 11/10/2016.
 */

public class CarModelData {

 public String CarModel;

    public String getCarModel() {
        return CarModel;
    }

    public CarModelData setCarModel(String carModel) {
        CarModel = carModel;
        return this;
    }
}
