package com.onlinetaxi.data;

/**
 * Created by Android on 10/21/2016.
 */

public class SharePaymentModel  {

    public String FriendName;
    public String Lastseen;

    public String getLastseen() {
        return Lastseen;
    }

    public SharePaymentModel setLastseen(String lastseen) {
        Lastseen = lastseen;
        return this;
    }

    public String getSharedAmount() {
        return SharedAmount;
    }

    public SharePaymentModel setSharedAmount(String sharedAmount) {
        SharedAmount = sharedAmount;
        return this;
    }

    public String SharedAmount;

    public String getFriendName() {
        return FriendName;
    }

    public SharePaymentModel setFriendName(String friendName) {
        FriendName = friendName;
        return this;
    }
}
