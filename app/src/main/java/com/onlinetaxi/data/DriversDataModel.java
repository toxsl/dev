package com.onlinetaxi.data;

import java.util.ArrayList;

/**
 * Created by Android on 11/24/2016.
 */

public class DriversDataModel {

    public String DriverCountryCode;
    public String DriverCompanyCode;
    public String DriverCarCompanyName;
    public String DriverLogo;
    public String DriverContact;
    public String DriverCode;
    public String DriverType;
    public String DriverName;
    public String DriverLatitude;
    public String DriverLongitude;
    public String DriverRate;
    public String DriverVehicle;
    public String DriverVehicleSeat;
    public String DriverRating;
    public String DriverStatus;
    public static ArrayList<DriversDataModel> arrayListDriver;

    public static ArrayList<DriversDataModel> getArrayListDriver() {
        return arrayListDriver;
    }

    public static void setArrayListDriver(ArrayList<DriversDataModel> arrayListDriver) {
        DriversDataModel.arrayListDriver = arrayListDriver;
    }



    public String getDriverCountryCode() {
        return DriverCountryCode;
    }

    public DriversDataModel setDriverCountryCode(String driverCountryCode) {
        DriverCountryCode = driverCountryCode;
        return this;
    }

    public String getDriverCompanyCode() {
        return DriverCompanyCode;
    }

    public DriversDataModel setDriverCompanyCode(String driverCompanyCode) {
        DriverCompanyCode = driverCompanyCode;
        return this;
    }

    public String getDriverCarCompanyName() {
        return DriverCarCompanyName;
    }

    public DriversDataModel setDriverCarCompanyName(String driverCarCompanyName) {
        DriverCarCompanyName = driverCarCompanyName;
        return this;
    }

    public String getDriverLogo() {
        return DriverLogo;
    }

    public DriversDataModel setDriverLogo(String driverLogo) {
        DriverLogo = driverLogo;
        return this;
    }

    public String getDriverContact() {
        return DriverContact;
    }

    public DriversDataModel setDriverContact(String driverContact) {
        DriverContact = driverContact;
        return this;
    }

    public String getDriverCode() {
        return DriverCode;
    }

    public DriversDataModel setDriverCode(String driverCode) {
        DriverCode = driverCode;
        return this;
    }

    public String getDriverType() {
        return DriverType;
    }

    public DriversDataModel setDriverType(String driverType) {
        DriverType = driverType;
        return this;
    }

    public String getDriverName() {
        return DriverName;
    }

    public DriversDataModel setDriverName(String driverName) {
        DriverName = driverName;
        return this;
    }

    public String getDriverLatitude() {
        return DriverLatitude;
    }

    public DriversDataModel setDriverLatitude(String driverLatitude) {
        DriverLatitude = driverLatitude;
        return this;
    }

    public String getDriverLongitude() {
        return DriverLongitude;
    }

    public DriversDataModel setDriverLongitude(String driverLongitude) {
        DriverLongitude = driverLongitude;
        return this;
    }

    public String getDriverRate() {
        return DriverRate;
    }

    public DriversDataModel setDriverRate(String driverRate) {
        DriverRate = driverRate;
        return this;
    }

    public String getDriverVehicle() {
        return DriverVehicle;
    }

    public DriversDataModel setDriverVehicle(String driverVehicle) {
        DriverVehicle = driverVehicle;
        return this;
    }

    public String getDriverVehicleSeat() {
        return DriverVehicleSeat;
    }

    public DriversDataModel setDriverVehicleSeat(String driverVehicleSeat) {
        DriverVehicleSeat = driverVehicleSeat;
        return this;
    }

    public String getDriverRating() {
        return DriverRating;
    }

    public DriversDataModel setDriverRating(String driverRating) {
        DriverRating = driverRating;
        return this;
    }

    public String getDriverStatus() {
        return DriverStatus;
    }

    public DriversDataModel setDriverStatus(String driverStatus) {
        DriverStatus = driverStatus;
        return this;
    }



}
