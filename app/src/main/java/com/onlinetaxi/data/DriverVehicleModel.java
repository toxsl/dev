package com.onlinetaxi.data;

/**
 * Created by Android on 11/16/2016.
 */

public class DriverVehicleModel {

    public String VehicleName;
    public String VehicleModel;
    public String Rating;
    public String Amount;

    public String getVehicleName() {
        return VehicleName;
    }

    public DriverVehicleModel setVehicleName(String vehicleName) {
        VehicleName = vehicleName;
        return this;
    }

    public String getVehicleModel() {
        return VehicleModel;
    }

    public DriverVehicleModel setVehicleModel(String vehicleModel) {
        VehicleModel = vehicleModel;
        return this;
    }

    public String getRating() {
        return Rating;
    }

    public DriverVehicleModel setRating(String rating) {
        Rating = rating;
        return this;
    }

    public String getAmount() {
        return Amount;
    }

    public DriverVehicleModel setAmount(String amount) {
        Amount = amount;
        return this;
    }
}
