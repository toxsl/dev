package com.onlinetaxi.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by TOXSL/utkarsh.shukla on 5/12/16.
 */

public class DriverProfileData  implements Parcelable {
    public String DrivrCompnyCode;
    public String DrivrCompnylogo;
    public String DriverCompanyAddress;
    public String DriverCompanyEmail;
    public String DriverCompanyTelno;
    public String DriverCompanyName;

    public String DriverPeakRate;
    public String DriverOffPeakRAte;


    public String DriverVehicleSeat;
    public String DriverCode;//uid
    public String DriverFirstName;
    public String Driverlastname;
    public String DriverType;


    public String Veahicle;
    public String Rating;
    public String Status;

    public String locality;
    public String coverage;


    public DriverProfileData(Parcel in) {
        DrivrCompnyCode = in.readString();
        DrivrCompnylogo = in.readString();
        DriverCompanyAddress = in.readString();
        DriverCompanyEmail = in.readString();
        DriverCompanyTelno = in.readString();
        DriverCompanyName = in.readString();
        DriverPeakRate = in.readString();
        DriverOffPeakRAte = in.readString();
        DriverVehicleSeat = in.readString();
        DriverCode = in.readString();
        DriverFirstName = in.readString();
        Driverlastname = in.readString();
        DriverType = in.readString();
        Veahicle = in.readString();
        Rating = in.readString();
        Status = in.readString();
        locality = in.readString();
        coverage = in.readString();
    }

    public static final Creator<DriverProfileData> CREATOR = new Creator<DriverProfileData>() {
        @Override
        public DriverProfileData createFromParcel(Parcel in) {
            return new DriverProfileData(in);
        }

        @Override
        public DriverProfileData[] newArray(int size) {
            return new DriverProfileData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(DrivrCompnyCode);
        dest.writeString(DrivrCompnylogo);
        dest.writeString(DriverCompanyAddress);
        dest.writeString(DriverCompanyEmail);
        dest.writeString(DriverCompanyTelno);
        dest.writeString(DriverCompanyName);
        dest.writeString(DriverPeakRate);
        dest.writeString(DriverOffPeakRAte);
        dest.writeString(DriverVehicleSeat);
        dest.writeString(DriverCode);
        dest.writeString(DriverFirstName);
        dest.writeString(Driverlastname);
        dest.writeString(DriverType);
        dest.writeString(Veahicle);
        dest.writeString(Rating);
        dest.writeString(Status);
        dest.writeString(locality);
        dest.writeString(coverage);
    }
}
