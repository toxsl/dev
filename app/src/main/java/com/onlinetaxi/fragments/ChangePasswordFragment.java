package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;

/**
 * Created by TOXSL\jyotpreet.kaur on 28/9/16.
 */
public class ChangePasswordFragment extends BaseFragment {
    private View view;
    private ImageView drawerIconIV;
    private TextView confirmTVF,newTVF,oldTVF;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

        confirmTVF= (TextView) view.findViewById(R.id.confirmTVF);
        confirmTVF.setTypeface(baseActivity.iconFont);
        newTVF= (TextView) view.findViewById(R.id.newTVF);
        newTVF.setTypeface(baseActivity.iconFont);
        oldTVF= (TextView) view.findViewById(R.id.oldTVF);
        oldTVF.setTypeface(baseActivity.iconFont);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }
}
