package com.onlinetaxi.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.activity.SplashActivity;
import com.onlinetaxi.data.UserModel;
import com.onlinetaxi.helper.AppController;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.MyLocationListener;
import com.onlinetaxi.helper.RegexValiations;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.utils.Const;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class LoginFragment extends BaseFragment implements BaseActivity.PermCallback {

    private TextView forgotPasswordTV;
    private TextView emailTVF, passwordTVF;
    EditText emailET, passwordET;
    FloatingActionButton submitFAB;
    /*private OTLoginTask otLoginTask;*/
    Dialog alertDialogProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_pager, container, false);
        baseActivity.setPermCallback(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        initFCM();

        forgotPasswordTV = (TextView) view.findViewById(R.id.forgotPasswordTV);
        forgotPasswordTV.setOnClickListener(this);

        emailTVF = (TextView) view.findViewById(R.id.emailTVF);
        emailTVF.setTypeface(baseActivity.iconFont);
        passwordTVF = (TextView) view.findViewById(R.id.passwordTVF);
        passwordTVF.setTypeface(baseActivity.iconFont);

        emailET = (EditText) view.findViewById(R.id.emailET);
        passwordET = (EditText) view.findViewById(R.id.passwordET);

        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgotPasswordTV:
                baseActivity.gotoForgotPassword();
                break;
            case R.id.submitFAB:

                if (checkValidation()) {
                    if (checkPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 99)) {
                        login();
                    }
                    break;
                }
        }

    }

    private void login() {
        if (ApplicationGlob.isConnectingToInternet(getActivity())) {

            String passingLatitude, passingLongitude;
            String GPS_Status = "OFF";
            MyLocationListener locationListener = new MyLocationListener(getActivity());

            if (SessionManager.getUserSplashLatitude(getActivity()).equals("UserSplashLatitude") || SessionManager.getUserSplashLongitude(getActivity()).equals("UserSplashLongitude")) {


                passingLongitude = locationListener.getLocationLongitude();
                passingLatitude = locationListener.getCurrentLatitude();

                SessionManager.setLatitudeFromLocationListner(getActivity(), passingLatitude);
                SessionManager.setLongitudeFromLocationListner(getActivity(), passingLongitude);

            } else {
                passingLatitude = SessionManager.getUserSplashLatitude(getActivity());
                passingLongitude = SessionManager.getUserSplashLongitude(getActivity());

            }

            if (locationListener.getGpsStatus() == true) {
                GPS_Status = "ON";
            }


            baseActivity.loginAPI(passingLatitude, passingLongitude, emailET.getText().toString(), passwordET.getText().toString(), true, "");

            /*otLoginTask = new OTLoginTask();
            otLoginTask.execute(emailET.getText().toString(),
                    passwordET.getText().toString(),
                    SessionManager.getUserCountryCode(getActivity()),
                    ApplicationGlob.Get_Device_Brand(),
                    ApplicationGlob.Get_Device_Manufacturer(),
                    ApplicationGlob.Get_DeviceModel(),
                    SessionManager.getUserDeviceToken(getActivity()),
                    passingLatitude,
                    passingLongitude,
                    GPS_Status
            );*/
        } else {
            ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
        }
    }


    private boolean checkValidation() {
        boolean ret = true;

        if (!RegexValiations.isEmailAddress(emailET, true))
            ret = false;

        if (!RegexValiations.hasText(passwordET))
            ret = false;

        return ret;
    }

    @Override
    public void permGranted(int resultCode) {
        login();

    }

    @Override
    public void permDenied(int resultCode) {
        showToast("You didn't have sufficient permissions");
    }


   /* private class OTLoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_user_login.php";
                params.put("email", paramsa[0]);
                params.put("Password", paramsa[1]);
                params.put("pisocode", paramsa[2]);
                params.put("pmanufacturer", paramsa[3]);
                params.put("pbrand", paramsa[4]);
                params.put("pmodel", paramsa[5]);
                params.put("pdeviceid", paramsa[6]);
                params.put("platitude", paramsa[7]);
                params.put("plongitude", paramsa[8]);
                params.put("pgps_status", paramsa[9]);
                params.put("pos_version", String.valueOf(android.os.Build.VERSION.SDK_INT));
                params.put("pip_address", "");
                params.put("pwebsite", "");
                params.put("pgender", "");
                params.put("plastname", "");
                params.put("pfirstname", "");
                params.put("psocial", "");
                params.put("uid", "");
                params.put("action", "action_user_login");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("OT Login Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");
                JSONObject userdata = jsoObject1.getJSONObject("user_data");
                baseActivity.store.saveString(Const.USER_STATUS, userdata.optString("status"));
                baseActivity.store.saveString(Const.USER_TYPE, userdata.optString("usertype"));

                if (returncode.equals("0")) {

                    JSONObject user_data_Object = jsoObject1.getJSONObject("user_data");
                    JSONObject transaction=user_data_Object.getJSONObject("transaction");
                    JSONObject settings=user_data_Object.getJSONObject("settings");

                    String Userid = user_data_Object.getString("uid");
                    baseActivity.store.saveString("Uid", Userid);
                    String UserFirstName = user_data_Object.optString("fname");
                    String UserLastName = user_data_Object.optString("lnme");
                    String UserEmail = user_data_Object.optString("email");
                    String UserContact = user_data_Object.optString("cellno");
                    String UserZipcode = user_data_Object.optString("zipcode");
                    String Usercity = user_data_Object.optString("city");
                    String UserState = user_data_Object.optString("State");


                    String MillageAmount=transaction.getString("millageamount");
                    SessionManager.setMillageAmount(getActivity(),MillageAmount);

                    String UserStatus=userdata.optString("status");
                    String UserType=userdata.optString("usertype");

                    JSONObject settingObject=user_data_Object.getJSONObject("settings");

                    String haversine_km = settingObject.optString("haversine_km");
                    String availability  = settingObject.optString("availability ");
                    String state_topic = settingObject.optString("state_topic");
                    String refresh = settingObject.optString("refresh");
                    String driverratekm = settingObject.optString("driverratekm");

                    SessionManager.setState_Topic(getActivity(),state_topic);

                    String refresh_value=settings.optString("refresh");

                    String userscurrency=transaction.optString("userscurrency");
                    SessionManager.setUsersCurrency(getActivity(),userscurrency);


                    SessionManager.setUserStatus(getActivity(),UserStatus);
                    SessionManager.setUserType(getActivity(),UserType);


                    SessionManager.setUserID(getActivity(), Userid);
                    SessionManager.setUserFirstName(getActivity(), UserFirstName);
                    SessionManager.setUserLastName(getActivity(), UserLastName);
                    SessionManager.setUserEmail(getActivity(), UserEmail);
                    SessionManager.setUserContactNumber(getActivity(), UserContact);
                    SessionManager.setUserAvailability(getActivity(), availability);
                    SessionManager.setHavesine_KM(getActivity(), haversine_km);
                    SessionManager.setMapRefreshTime(getActivity(), refresh);
                    SessionManager.setDriverRatePerKM(getActivity(), driverratekm);
                    SessionManager.setUserState(getActivity(), UserState);
                    SessionManager.setUsercity(getActivity(), Usercity);
                    SessionManager.setUserZipcode(getActivity(), UserZipcode);

                    if (!baseActivity.getSinchServiceInterface().isStarted() ) {
                        baseActivity.getSinchServiceInterface().startClient(Userid);
                        showToast("sinch service started");

                    }

                    Intent redirect = new Intent(getActivity(), MainActivity.class);
                    getActivity().startActivity(redirect);

                } else {
                    String excptn = jsoObject1.getString("message");
                      ToastOnUiThread(excptn);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void ToastOnUiThread(final String message) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity().getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();
                }
            });

        }

    }*/

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }

}
