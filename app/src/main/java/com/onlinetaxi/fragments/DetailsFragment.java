package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.driver.InformationCenterFragment;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.material.ButtonFlat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class DetailsFragment extends BaseFragment {

    private TextView forgotPasswordTV;
    private FloatingActionButton submitFAB;
    private ImageView drawerIconIV;
    private TextView pointsTV, estimatedTimeTV, totalAmountTV, paymentMethodTextTV,methodTypeTV;
    public AlertDialog dialog;
    private RelativeLayout numberPassengersRL, pickuptimeRL, paymentMethodRL;
    Dialog alertDialogPassenger, alertDialogPaymentMethod, alertDialogPickupTime, dialogpaymentCash;
    private TextView infiTVF;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
        totalAmountTV = (TextView) view.findViewById(R.id.totalAmountTV);
        paymentMethodTextTV = (TextView) view.findViewById(R.id.paymentMethodTextTV);
        methodTypeTV = (TextView) view.findViewById(R.id.methodTypeTV);
        estimatedTimeTV = (TextView) view.findViewById(R.id.estimatedTimeTV);
        pointsTV = (TextView) view.findViewById(R.id.pointsTV);
        numberPassengersRL = (RelativeLayout) view.findViewById(R.id.numberPassengersRL);
        pickuptimeRL = (RelativeLayout) view.findViewById(R.id.pickuptimeRL);
        paymentMethodRL = (RelativeLayout) view.findViewById(R.id.paymentMethodRL);
        numberPassengersRL.setOnClickListener(this);
        pickuptimeRL.setOnClickListener(this);
        paymentMethodRL.setOnClickListener(this);

        infiTVF = (TextView) view.findViewById(R.id.infiTVF);
        infiTVF.setTypeface(baseActivity.iconFont1);
        infiTVF.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:

                if (methodTypeTV.getText().toString().equals("Cash Payment")){
                    obligatingdialog();

                }else{

                    switch (methodTypeTV.getText().toString()) {
                        case "Card Payment":
                            baseActivity.getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.frame_container, new CardPaymentFragment())
                                    .commit();
                            break;
                        case "Millage Point":
                            baseActivity.getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.frame_container, new MillagePointsFragment())
                                    .commit();
                            break;
                        case "Split Payment":
                            baseActivity.getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.frame_container, new SharedPaymentFragment())
                                    .commit();
                            break;


                    }





                }


                break;
            case R.id.infiTVF:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new InformationCenterFragment()).addToBackStack(null).commit();

                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.pickuptimeRL:
                openPickupTimeDailog();
                break;
            case R.id.numberPassengersRL:
                openPassengesDailog();
                break;

            case R.id.paymentMethodRL:
                openAmountDailog();
                break;



        }
    }

        private void obligatingdialog() {
            View dialoglayout = View.inflate(baseActivity, R.layout.dialog_obligatiing_shayredpayement, null);
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
            builder.setView(dialoglayout);
            builder.setTitle("");

            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("ACCEPT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    baseActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frame_container, new ConfirmationFragment())
                            .commit();
                }
            });
            dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }

    private void openAmountDailog() {
        if (ApplicationGlob.isConnectingToInternet(getActivity())) {


            View dialoglayout = View.inflate(baseActivity, R.layout.chose_no_of_payment, null);
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
            builder.setView(dialoglayout);
            builder.setTitle("");
            final Spinner spinnerPaymentMEthod = (Spinner) dialoglayout.findViewById(R.id.spinnerPaymentMEthod);
            spinnerPaymentMEthod
                    .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @SuppressWarnings("unused")
                        public void onItemSelected(AdapterView<?> parent,
                                                   View view, int pos, long id) {
                            String item = parent.getItemAtPosition(pos).toString();

                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });


            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //    showToast("Changed Successfully");
                    switch (spinnerPaymentMEthod.getSelectedItem().toString()) {
                        case "Cash Payment":
                            methodTypeTV.setText("Cash Payment");
                            break;
                        case "Card Payment":
                            methodTypeTV.setText("Card Payment");
                            break;
                        case "Millage Point":
                            methodTypeTV.setText("Millage Point");
                            break;
                        case "Split Payment":
                            methodTypeTV.setText("Split Payment");
                            break;


                    }




                    dialog.dismiss();
                }
            });
            dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }




    else {

            ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
        }

    }

    public void openPassengesDailog() {


        if (ApplicationGlob.isConnectingToInternet(getActivity())) {


            View dialoglayout = View.inflate(baseActivity, R.layout.chose_no_of_passenger, null);
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
            builder.setView(dialoglayout);
            builder.setTitle("");

            final Spinner spinner = (Spinner) dialoglayout.findViewById(R.id.spinnerSP);

            List<Integer> pricearray = new ArrayList<Integer>();

            pricearray = ApplicationGlob.getNextfivePassenger(4);
            // Sort by address.
            Collections.sort(pricearray, ApplicationGlob.PassergerLowToHigh);

            ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(getActivity(),
                    R.layout.spinner_layout_passenger, R.id.textviewPickupTime,
                    pricearray);

            spinner.setAdapter(adapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @SuppressWarnings("unused")
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int pos, long id) {


                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialog.dismiss();
                }
            });
            builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    pointsTV.setText(spinner.getSelectedItem().toString());
                    dialog.dismiss();
                }
            });
            dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }

        else {

            ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
        }


    }

    private void openPickupTimeDailog() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_estimate_time, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showToast("Changed Successfully");
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


}
