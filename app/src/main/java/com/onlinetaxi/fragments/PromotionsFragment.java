package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;

/**
 * Created by TOXSL\jyotpreet.kaur on 28/9/16.
 */
public class PromotionsFragment extends BaseFragment {

    ImageView drawerIconIV;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promotions, container, false);

        drawerIconIV= (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

            }
        });
        return view;
    }
}
