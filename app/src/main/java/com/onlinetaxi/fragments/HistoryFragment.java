package com.onlinetaxi.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.HistoryAdapter;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class HistoryFragment extends BaseFragment {

    private ListView historyLV;
    private ImageView drawerIconIV;
    TextView change_currencyTV;
    private TextView total_balanceTV;
    private TextView total_taxTV, total_discountTV;

    private TextView totalBalanceTV, total_TaxTV, totalDiscountTV;

    private Dialog alertDialogProgressBar;
    History history;
    private String amount, discount, taxamount;
    ArrayList<HistoryData> historyDatas = new ArrayList<>();
    ArrayList<String> currencylist = new ArrayList<>();
    ArrayList<String> monthlist = new ArrayList<>();
    private String durationMonth = "";
    private String SelectedCurrency = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        initUI(view);

        history = new History();
        history.execute(SessionManager.getUserCountryCode(getActivity()),
                SessionManager.getUserID(getActivity()), durationMonth, SelectedCurrency);

        return view;
    }

    private void initUI(View view) {

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        change_currencyTV = (TextView) view.findViewById(R.id.change_currencyTV);
//        change_currencyTV.setOnClickListener(this);
        historyLV = (ListView) view.findViewById(R.id.historyLV);
        drawerIconIV.setOnClickListener(this);

        total_balanceTV = (TextView) view.findViewById(R.id.total_balanceTV);
        total_taxTV = (TextView) view.findViewById(R.id.total_taxTV);
        total_discountTV = (TextView) view.findViewById(R.id.total_discountTV);

        totalBalanceTV = (TextView) view.findViewById(R.id.totalBalanceTV);
        total_TaxTV = (TextView) view.findViewById(R.id.total_TaxTV);
        totalDiscountTV = (TextView) view.findViewById(R.id.totalDiscountTV);

        totalBalanceTV.setOnClickListener(this);
        total_TaxTV.setOnClickListener(this);
        totalDiscountTV.setOnClickListener(this);

        total_balanceTV.setOnClickListener(this);
        total_taxTV.setOnClickListener(this);
        total_discountTV.setOnClickListener(this);


        total_balanceTV.setText(SessionManager.getCurrencySymbol(getActivity()) + " " + SessionManager.getHistoryAmount(getActivity()));
        total_taxTV.setText(SessionManager.getCurrencySymbol(getActivity()) + " " + SessionManager.getHistorytaxamount(getActivity()));
        total_discountTV.setText(SessionManager.getCurrencySymbol(getActivity()) + " " + SessionManager.getHistorydiscount(getActivity()));

        historyDatas.clear();
        if (SessionManager.getHistoryData(getContext()) != null && !SessionManager.getHistoryData(getContext()).equals(""))

            /*historyDatas =baseActivity.getListData(Const.History_List,new HistoryData());*/
            historyDatas = SessionManager.getHistoryData(getContext());

        historyLV.setAdapter(new HistoryAdapter(baseActivity, 0, historyDatas));

        historyLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Fragment fragment = new ManageHistoryFragment();

                SessionManager.setSelectedHistoryPosition(getActivity(), "" + position);

                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;

            case R.id.total_balanceTV:
                changecurrencyDialog();
                break;
            case R.id.total_taxTV:
                changecurrencyDialog();
                break;
            case R.id.total_discountTV:
                changecurrencyDialog();
                break;
            case R.id.totalBalanceTV:
                selectMonth();
                break;
            case R.id.total_TaxTV:
                selectMonth();
                break;
            case R.id.totalDiscountTV:
                selectMonth();
                break;
        }
    }

    private void changecurrencyDialog() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_change_currency, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("Choose Currency");

        Spinner spinnerSP = (Spinner) dialoglayout.findViewById(R.id.spinnerSP);

        currencylist = SessionManager.getCurrencyList(getContext());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, currencylist);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerSP.setAdapter(dataAdapter);

        spinnerSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SelectedCurrency = currencylist.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                dialog.dismiss();
            }
        });
        builder.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                history = new History();
                history.execute(SessionManager.getUserCountryCode(getActivity()),
                        SessionManager.getUserID(getActivity()), durationMonth, SelectedCurrency);
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void selectMonth() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_select_history_of_month_, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("History for the Month");


        Spinner spinnerSP = (Spinner) dialoglayout.findViewById(R.id.spinnerSP);

        monthlist = SessionManager.getMonthList(getContext());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, monthlist);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerSP.setAdapter(dataAdapter);
        spinnerSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                durationMonth = monthlist.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("CHANGE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                history = new History();
                history.execute(SessionManager.getUserCountryCode(getActivity()),
                        SessionManager.getUserID(getActivity()), durationMonth, SelectedCurrency);

                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


    private class History extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_passenger_history.php";

                params.put("pisocode", paramsa[0]);
//                params.put("pisocode", "za");
                params.put("uid", paramsa[1]);
//                params.put("uid", "100");
                params.put("pduratationt", paramsa[2]);
                params.put("pcurrency", paramsa[3]);
                params.put("submit", "passenger_transact");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("Update Profile Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray Response = jsonObject.getJSONArray("Response");
                JSONObject obj = Response.getJSONObject(0);

                JSONObject summary = obj.getJSONObject("summary");
                amount = summary.optString("amount");
                SessionManager.setHistoryAmount(getActivity(), amount);
                taxamount = summary.optString("taxamount");
                SessionManager.setHistorytaxamount(getActivity(), taxamount);
                discount = summary.optString("discount");
                SessionManager.setHistorydiscount(getActivity(), discount);

                historyDatas.clear();
                JSONArray currency = summary.getJSONArray("currency");

                currencylist.clear();
                for (int i = 0; i < currency.length(); i++) {
                    JSONObject jsonObject1 = currency.getJSONObject(i);
                    currencylist.add(jsonObject1.optString("display"));
                }
                SessionManager.setCurrencyList(getContext(), currencylist);


                JSONArray duration = summary.getJSONArray("duration");
                monthlist.clear();
                for (int i = 0; i < duration.length(); i++) {
                    JSONObject jsonObject1 = duration.getJSONObject(i);
                    monthlist.add(jsonObject1.optString("display"));
                }
                SessionManager.setMonthList(getContext(), monthlist);


                JSONArray history = obj.getJSONArray("history");
                for (int i = 0; i < history.length(); i++) {
                    JSONObject object = history.getJSONObject(i);
                    HistoryData historyData = new HistoryData();
                    historyData.amount = object.optString("amount");
                    historyData.currencySymbol = object.optString("currencysymbol");
                    SessionManager.setCurrencySymbol(getActivity(), object.optString("currencysymbol"));
                    historyData.DropOff = object.optString("destination");
                    historyData.pickup = object.optString("pickup");
                    historyData.paymentmethod = object.optString("paymentmethod");
                    historyData.date = object.optString("dated");
                    historyData.reference = object.optString("reference");
                    // historyData.driver = object.optString("driver");
                    if (object.has("driver")) {
                        try {
                            JSONObject driver = object.getJSONObject("driver");
                            historyData.drivercode = driver.optString("drivercode");
                            historyData.driverRating=driver.optString("driver_rating");
                            historyData.drivername=driver.optString("drivername");
                        } catch (JSONException e) {

                        }
                    }


                    historyDatas.add(historyData);
                }
                SessionManager.setHistoryData(getContext(), historyDatas);
//                baseActivity.setListData(Const.History_List,historyDatas);
                total_balanceTV.setText(SessionManager.getCurrencySymbol(getActivity()) + " " + SessionManager.getHistoryAmount(getActivity()));
                total_taxTV.setText(SessionManager.getCurrencySymbol(getActivity()) + " " + SessionManager.getHistorytaxamount(getActivity()));
                total_discountTV.setText(SessionManager.getCurrencySymbol(getActivity()) + " " + SessionManager.getHistorydiscount(getActivity()));
                historyLV.setAdapter(new HistoryAdapter(baseActivity, 0, historyDatas));




            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }
}
