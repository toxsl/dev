package com.onlinetaxi.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.utils.Const;

import java.util.List;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class ProgressFragment extends BaseFragment implements BaseActivity.PermCallback, LocationListener {

    private TextView forgotPasswordTV;
    private LinearLayout messageLL;
    private FloatingActionButton submitFAB;
    View view;
    private SupportMapFragment availableMap;
    GoogleMap googlemap;
    private LocationManager mLocationManager;
    private AlertDialog.Builder alert;
    private Location currentLocation;
    private int count;
    private Marker pickupMarker;
    private String currentAddress;
    private LatLng pickupLatLng;
    private LatLng taxilatlng;
    private Marker taxiMarker;
    private ImageView drawerIconIV;
    private TextView infiTVF,calltvf;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        view = inflater.inflate(R.layout.fragment_progress, container, false);
        return view;
    }

    @Override
    public void onResume() {
        try {
            if (baseActivity.checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, 6)) {
                init();
            }
        } catch (InflateException e) {
        }

        baseActivity.setPermCallback(this);
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (baseActivity.checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, 6)) {
            init();
        }
    }

    private void init() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        mLocationManager = (LocationManager) baseActivity.getSystemService(Context.LOCATION_SERVICE);
        messageLL = (LinearLayout) view.findViewById(R.id.messageLL);
        messageLL.setOnClickListener(this);
        drawerIconIV.setOnClickListener(this);
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);

        calltvf= (TextView) view.findViewById(R.id.calltvf);
        calltvf.setTypeface(baseActivity.iconFont);

        infiTVF= (TextView) view.findViewById(R.id.infiTVF);
        infiTVF.setTypeface(baseActivity.iconFont1);

        submitFAB.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new PassengerMapAndClassicFragment()).commit();
                return true;
            }
        });
        initilizeMap();
    }

    private void initilizeMap() {
        availableMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        if (googlemap == null) {
            availableMap.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        googleMap.setMyLocationEnabled(false);
                        googleMap.getUiSettings().setZoomControlsEnabled(false);
                        googleMap.getUiSettings().setZoomGesturesEnabled(false);
                        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                            @Override
                            public boolean onMyLocationButtonClick() {
                                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                    return true;
                                } else {
                                    buildAlertMessageNoGps();

                                }
                                return false;
                            }
                        });
                        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                            @Override
                            public void onMapLoaded() {
                                setMarker();
                            }
                        });
                        ProgressFragment.this.googlemap = googleMap;
                    }


                }
            });

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgotPasswordTV:
                break;
            case R.id.messageLL:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ChatFragment()).commit();
                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;

        }
    }

    private void buildAlertMessageNoGps() {
        if (isAdded()) {
            if (alert == null) {
                alert = new AlertDialog.Builder(baseActivity);
                alert.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") DialogInterface dialog, @SuppressWarnings("unused") int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                alert = null;
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, @SuppressWarnings("unused") int id) {
                                dialog.cancel();
                                alert = null;
                            }
                        });
                alert.show();
            }
        }

    }


    private void setMarker() {
        if (currentLocation != null) {
            setCurrentMarker();
            baseActivity.latitude = currentLocation.getLatitude();
            baseActivity.longitude = currentLocation.getLongitude();
            GetNearLocations getlocations = new GetNearLocations();
            getlocations.execute();
        } else {
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (count < 5) {
                    count++;
                    currentLocation = getLastKnownLocation();
                    onLocationChanged(currentLocation);
                } else
                    showToast(baseActivity.getString(R.string.cannot_get_location_please_refresh));
            } else
                buildAlertMessageNoGps();
        }
    }

    private void setCurrentMarker() {
        pickupLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        taxilatlng = new LatLng(currentLocation.getLatitude() + 00.009, currentLocation.getLongitude());

        currentAddress = getAddress(currentLocation.getLatitude(), currentLocation.getLatitude());
        googlemap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude() + 00.009, currentLocation.getLongitude()), 15));
        setTaxiMarker();
    }

    private void setTaxiMarker() {
        if (taxiMarker == null)
            taxiMarker = googlemap.addMarker(new MarkerOptions().position(taxilatlng).draggable(true).title("Dropoff Location").snippet("taxi").icon(BitmapDescriptorFactory.fromResource(R.drawable.location_car)));
        else {
            taxiMarker.setPosition(taxilatlng);
        }
    }
    private Location getLastKnownLocation() {
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    mLocationManager.requestLocationUpdates(provider, Const.MIN_TIME_BW_UPDATES, Const.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    l = mLocationManager.getLastKnownLocation(provider);
                }
                if (l != null && (bestLocation == null || l.getAccuracy() > bestLocation.getAccuracy())) {
                    bestLocation = l;
                }
            }
        }
        return bestLocation;
    }

    @Override
    public void permGranted(int resultCode) {
        init();

    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        setMarker();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        currentLocation = getLastKnownLocation();
        onLocationChanged(currentLocation);
    }

    @Override
    public void onProviderDisabled(String s) {
        if (s.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
            buildAlertMessageNoGps();
    }
}
