package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.activity.SplashActivity;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.RegexValiations;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.utils.FontManager;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class RegisterFragment extends BaseFragment {
    TextView contactTVF, addressTVF, re_passwordTVF, passwordTVF, emailTVF;
    EditText contactET, addressET, re_passwordET, passwordET, emailET;
    FloatingActionButton submitFAB;
    RegisterUserTask registerTask;
    CoordinatorLayout parent;
    Dialog alertDialogProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_pager, container, false);
        init(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        submitFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationGlob.isConnectingToInternet(getActivity())) {
                    if (checkValidation()) {

                        String Uname, Upass, Ucontact, Uaddress;
                        Uname = emailET.getText().toString();
                        Upass = passwordET.getText().toString();
                        Ucontact = contactET.getText().toString();
                        Uaddress = addressET.getText().toString();


                        registerTask = new RegisterUserTask();
                        registerTask.execute(
                                SessionManager.getUserCountryCode(getActivity()),
                                Uname,
                                Upass,
                                Ucontact,
                                Uaddress);


                    }

                } else {

                    ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
                }
            }
        });

    }

    private void init(View view) {
        parent = (CoordinatorLayout) view.findViewById(R.id.parent);
        contactET = (EditText) view.findViewById(R.id.ContactET);
        emailET = (EditText) view.findViewById(R.id.EmailET);
        passwordET = (EditText) view.findViewById(R.id.PasswordET);
        re_passwordET = (EditText) view.findViewById(R.id.RePasswordET);
        addressET = (EditText) view.findViewById(R.id.PrimaryAddressET);
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);
        contactTVF = (TextView) view.findViewById(R.id.contactTVF);
        contactTVF.setTypeface(baseActivity.iconFont);
        addressTVF = (TextView) view.findViewById(R.id.addressTVF);
        addressTVF.setTypeface(baseActivity.iconFont);
        re_passwordTVF = (TextView) view.findViewById(R.id.re_passwordTVF);
        re_passwordTVF.setTypeface(baseActivity.iconFont);
        passwordTVF = (TextView) view.findViewById(R.id.passwordTVF);
        passwordTVF.setTypeface(baseActivity.iconFont);
        emailTVF = (TextView) view.findViewById(R.id.emailTVF);
        emailTVF.setTypeface(baseActivity.iconFont);

    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!RegexValiations.hasText(addressET))
            ret = false;

        if (!RegexValiations.isEmailAddress(emailET, true))
            ret = false;

        if (!RegexValiations.hasText(passwordET))
            ret = false;

        if (!RegexValiations.conpaireText(passwordET, re_passwordET))
            ret = false;
        if (!RegexValiations.hasText(contactET))
            ret = false;
        return ret;
    }


    private class RegisterUserTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_create_user.php";


                params.put("pisocode", paramsa[0]);
                params.put("pun", paramsa[1]);
                params.put("pemail", paramsa[1]);
                params.put("ppw", paramsa[2]);
                params.put("pcellno", paramsa[3]);
                params.put("pextaddr", paramsa[4]);
                params.put("pusertype", "P");
                params.put("pfirstname", "");
                params.put("plastname", "");
                params.put("pgender", "");
                params.put("pwebsite", "");
                params.put("pstatus", "UP");
                params.put("action", "Registration");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);
                    response = HttpUtility.readInputStreamToString();

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressbar(false);


            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");

                if (returncode.equals("0")) {

                    JSONObject user_data_Object = jsoObject1.getJSONObject("user_data");

                    baseActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.frame_container, new LoginFragment())
                            .commit();


                } else {
                    String excptn = jsoObject1.getString("message");
                    SnackBarOnUiThread(excptn);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void SnackBarOnUiThread(final String message) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    Snackbar snackbar = Snackbar
                            .make(parent, message, Snackbar.LENGTH_LONG);
                    snackbar.show();


                }
            });
        }

    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();


        } else {

            alertDialogProgressBar.dismiss();

        }

    }


}
