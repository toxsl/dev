package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TOXSL\jyotpreet.kaur on 28/9/16.
 */
public class CancellationFragment extends BaseFragment {
    private FloatingActionButton submitFAB;
    private ImageView drawerIconIV;

    private RadioButton othersRB, chngeMyMindRB, driverTolateRB;
    private RadioGroup actionRG, cancelResionRG;
    private RadioButton purchaseMilageRB, cancelTripRB, getMeAnotherRB;

    String driverToLate = "D", changeMyMind = "C", others = "O", getMeAnother = "G", cancelTrip = "C", purchaseMillage = "P";

    String pcancel_reason, pcancel_action;
    RatingAPI ratingAPI;
    Dialog alertDialogProgressBar;
    ArrayList<HistoryData> historyDatas = new ArrayList<>();
    private EditText destinationET,pickupaddressET;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cancellation, container, false);
        historyDatas = SessionManager.getHistoryData(getActivity());
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);


        othersRB = (RadioButton) view.findViewById(R.id.othersRB);
        chngeMyMindRB = (RadioButton) view.findViewById(R.id.chngeMyMindRB);
        driverTolateRB = (RadioButton) view.findViewById(R.id.driverTolateRB);

        purchaseMilageRB = (RadioButton) view.findViewById(R.id.purchaseMilageRB);
        cancelTripRB = (RadioButton) view.findViewById(R.id.cancelTripRB);
        getMeAnotherRB = (RadioButton) view.findViewById(R.id.getMeAnotherRB);

        actionRG = (RadioGroup) view.findViewById(R.id.actionRG);
        cancelResionRG = (RadioGroup) view.findViewById(R.id.cancelResionRG);

        pickupaddressET= (EditText) view.findViewById(R.id.pickupaddressET);
        pickupaddressET.setText(historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).pickup);
        destinationET= (EditText) view.findViewById(R.id.destinationET);
        destinationET.setText(historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).DropOff);

        drawerIconIV.setOnClickListener(this);
        submitFAB.setOnClickListener(this);
        submitFAB.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new PassengerMapAndClassicFragment()).commit();
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;

            case R.id.submitFAB:
                if (driverTolateRB.isChecked())
                    pcancel_reason = driverToLate;
                else if (chngeMyMindRB.isChecked())
                    pcancel_reason = changeMyMind;
                else if (othersRB.isChecked())
                    pcancel_reason = others;

                if (getMeAnotherRB.isChecked())
                    pcancel_action = getMeAnother;
                else if (cancelTripRB.isChecked())
                    pcancel_action = cancelTrip;
                else if (purchaseMilageRB.isChecked())
                    pcancel_action = purchaseMillage;


                ratingAPI = new RatingAPI();
                ratingAPI.execute(SessionManager.getUserCountryCode(getActivity())
                        , SessionManager.getUserID(getActivity())
                        , pcancel_reason
                        , pcancel_action
                        , historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).reference
                );

                break;
        }
    }

    private class RatingAPI extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_users_cancellation.php";
                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("pcancel_reason", paramsa[2]);
                params.put("pcancel_action", paramsa[3]);
                params.put("preference", paramsa[4]);
                params.put("plocation", "0.0");
                params.put("pickup_time", "0");
                params.put("pickup_km", "0");
                params.put("action", "cancellation");
                try {
                    HttpUtility.sendPostRequest(requestURL, params);
                    baseActivity.log(requestURL + "  " + params);
                    response = HttpUtility.readInputStreamToString();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);

            Log.i("result=", " " + result);
            try {
                if (result != null && !result.isEmpty() && result != " ") {
                    baseActivity.log(result);
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray Response = jsonObject.getJSONArray("Response");
                    JSONObject object = Response.getJSONObject(0);
                    if (object.optString("message").equalsIgnoreCase("Success")) {
                        if (object.optString("returncode").equals("0")) {
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        public void progressbar(boolean b) {

            if (b == true) {

                // NOTE : You can set the current value of BusyIndicator
                alertDialogProgressBar = new Dialog(getActivity(), R.style.YourCustomStyle);

                alertDialogProgressBar
                        .requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

                alertDialogProgressBar.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));

                alertDialogProgressBar.show();

            } else {

                alertDialogProgressBar.dismiss();

            }

        }

    }
}