package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;

/**
 * Created by TOXSL\jyotpreet.kaur on 28/9/16.
 */
public class AvailableTaxiFragment extends BaseFragment {

    private FloatingActionButton submitFAB;
    private ImageView drawerIconIV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_available_taxi, container, false);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ConfirmationFragment()).commit();
                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }
}
