package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.adapter.LocationsAdapter;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class PickupFragment extends BaseFragment {
    ListView pickupLV, pickupSuggestionsLV;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pickup_location, container, false);
        initUI();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        LocationsAdapter adapter = new LocationsAdapter(baseActivity, baseActivity.nearestLocations);
        pickupLV.setAdapter(adapter);
        pickupSuggestionsLV.setAdapter(adapter);
    }

    private void initUI() {
        pickupLV = (ListView) view.findViewById(R.id.pickupLV);
        pickupSuggestionsLV = (ListView) view.findViewById(R.id.pickupSuggestionsLV);
        pickupLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                baseActivity.pickupLocation = baseActivity.nearestLocations.get(i).locationName;
                if(baseActivity.destinationLocation.isEmpty()){
                    baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container,new PassengerMapAndClassicFragment()).addToBackStack(null).commit();

                }else {
                    baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container,new DetailsFragment()).addToBackStack(null).commit();
                }
            }
        });
        pickupSuggestionsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                baseActivity.pickupLocation = baseActivity.nearestLocations.get(i).locationName;
                if(baseActivity.destinationLocation.isEmpty()){
                    baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container,new PassengerMapAndClassicFragment()).addToBackStack(null).commit();

                }else {
                    baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container,new DetailsFragment()).addToBackStack(null).commit();
                }
            }
        });

    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


}
