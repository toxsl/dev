package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.DriverMapClassicPagerAdapter;
import com.onlinetaxi.adapter.PassengerMapClassicPagerAdapter;
import com.onlinetaxi.utils.SlidingTabLayout;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class PassengerMapAndClassicFragment extends BaseFragment {

    ImageView drawerIconIV;
    View view;
    private ViewPager pager;
    private CharSequence titles[] = new CharSequence[2];
    private PassengerMapClassicPagerAdapter adapter;
    private int noOfTabs = 2;
    private SlidingTabLayout tabs;
    private CoordinatorLayout coordinateCL;
    public static EditText pickupaddressET, destinationET;
    public static boolean runningPassengerMapActivity = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.passenger_map_classic, container, false);

        init();


        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        titles[0] = getString(R.string.map);
        titles[1] = getString(R.string.classic);
        adapter = new PassengerMapClassicPagerAdapter(getActivity().getSupportFragmentManager(), titles,
                noOfTabs);

        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorPrimary);
            }

            @Override
            public int getDividerColor(int position) {
                return getResources().getColor(R.color.transparent);
            }
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                    refreshMap();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);

    }

    public void refreshMap() {
        int position = pager.getCurrentItem();
        if(position==0) {
            PassengerMapFragment fg = (PassengerMapFragment) pager.getAdapter().instantiateItem(pager, pager.getCurrentItem());
            fg.refreshData();
        }else {
            PassengerClassicFragment fg = (PassengerClassicFragment)pager.getAdapter().instantiateItem(pager,pager.getCurrentItem());
            fg.refreshData();
        }
    }


    private void init() {

        tabs = (SlidingTabLayout) view.findViewById(R.id.tabs);
        pager = (ViewPager) view.findViewById(R.id.pager);
        destinationET = (EditText) view.findViewById(R.id.destinationET);
        pickupaddressET = (EditText) view.findViewById(R.id.pickupaddressET);
        destinationET.setOnClickListener(this);
        pickupaddressET.setOnClickListener(this);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.GoTaxiRL:
                baseActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_container, new DetailsFragment())
                        .commit();
                break;
            case R.id.pickupaddressET:
                Fragment fragment = new SetLocationFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("pager_position", 0);
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();
                fragment.setArguments(bundle);
                break;
            case R.id.destinationET:
                Fragment destination = new SetLocationFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putInt("pager_position", 1);
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, destination).commit();
                destination.setArguments(bundle1);
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {

          /*  if (checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 5)) {

                init();
            }*/
        } catch (InflateException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onStop() {
        super.onStop();

        runningPassengerMapActivity = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        runningPassengerMapActivity = false;
    }

    @Override
    public void onStart() {
        super.onStart();

        runningPassengerMapActivity = true;
    }

}
