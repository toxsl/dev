package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.NeartestPlacesData;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.utils.Const;
import com.onlinetaxi.utils.PrefStore;
import com.onlinetaxi.video.SinchService;
import com.toxsl.http.SyncEventListner;
import com.toxsl.http.SyncManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;


/**
 * Created by TOXSL\neeraj.narwal on 2/2/16.
 */
public class BaseFragment extends Fragment implements AdapterView.OnItemClickListener,
        View.OnClickListener, SyncEventListner, AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener {

    public BaseActivity baseActivity;
    public Dialog dialog;
    PrefStore store;
    SyncManager syncManager;
    public TextView txtMsgTV;
    Handler handler = new Handler();
    long timespan;
    private int reqCode;
    private SinchService.SinchServiceInterface mSinchServiceInterface;

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            getJourneyDetailSingleShot();
            handler.postDelayed(runnable, timespan);

        }
    };

    public void getJourneyDetailSingleShot() {
        int journeyId = store.getInt(Const.JOURNEY_ID);
        if (journeyId != 0) {
            syncManager.getFromServer(Const.API_JOURNEY_DETAIL_ID + journeyId, null, this);

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseActivity = (BaseActivity) getActivity();
        store = new PrefStore(baseActivity);
        syncManager = SyncManager.getInstance(getActivity());
        syncManager.setBaseUrl(Const.SERVER_REMOTE_URL, getString(R.string.app_name));
        progressDialog();
        baseActivity.cancelNotification(Const.ALL_NOTI);
    }


    public static void hideSoftKeyboard(BaseActivity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity
                    .getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }


    public boolean checkPermissions(String[] perms, int requestCode) {
        reqCode = requestCode;
        ArrayList<String> permsArray = new ArrayList<>();
        boolean hasPerms = true;
        for (String perm : perms) {
            if (ContextCompat.checkSelfPermission(getActivity(), perm) != PackageManager.PERMISSION_GRANTED) {
                permsArray.add(perm);
                hasPerms = false;
            }
        }
        if (!hasPerms) {
            String[] permsString = new String[permsArray.size()];
            for (int i = 0; i < permsArray.size(); i++) {
                permsString[i] = permsArray.get(i);
            }
            ActivityCompat.requestPermissions(getActivity(), permsString, 99);
            return false;
        } else
            return true;
    }


    public static void showSoftKeyboard(BaseActivity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(BaseActivity.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {

        }
    }

    public String getAddress(double lat, double lang) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(baseActivity);
            if (lat != 0 || lang != 0) {
                addresses = geocoder.getFromLocation(lat, lang, 1);
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                String country = addresses.get(0).getAddressLine(2);
                String state = addresses.get(0).getSubLocality();
                return address + ", " + city + ", " + (country != null ? country : "");
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        baseActivity.checkNetwork();
        getActivity().invalidateOptionsMenu();
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }
    public  class GetNearLocations extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + baseActivity.latitude + "," + baseActivity.longitude + "&radius=5000&types=5&sensor=true&key=AIzaSyCad3-AnMIpVCz58allhfsrv3Hh-3pnXZM";
            String resp = getJSON(url, 50000);
            parseJson(resp);
            return null;
        }


        public String getJSON(String url, int timeout) {
            HttpURLConnection c = null;
            try {
                URL u = new URL(url);
                c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setRequestProperty("Content-length", "0");
                c.setUseCaches(false);
                c.setAllowUserInteraction(false);
                c.setConnectTimeout(timeout);
                c.setReadTimeout(timeout);
                c.connect();
                int status = c.getResponseCode();

                switch (status) {
                    case 200:
                    case 201:
                        BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        String line;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();
                        return sb.toString();
                }

            } catch (MalformedURLException ex) {
            } catch (IOException ex) {
            } finally {
                if (c != null) {
                    try {
                        c.disconnect();
                    } catch (Exception ex) {
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }
    private void parseJson(String total) {
        try {
            baseActivity.nearestLocations.clear();

            if (total != null) {
                if (total.length() > 10) {
                    JSONObject obj = new JSONObject(total);
                    JSONArray array = obj.getJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        NeartestPlacesData neartestPlacesData = new NeartestPlacesData();
                        neartestPlacesData.vicinity = object.getString("vicinity");
                        neartestPlacesData.id = object.getString("id");
                        neartestPlacesData.locationName = object.getString("name");
                        JSONObject object1 = object.getJSONObject("geometry");
                        JSONObject object2 = object1.getJSONObject("location");
                        neartestPlacesData.lati = object2.getDouble("lat");
                        neartestPlacesData.longi = object2.getDouble("lng");
                        baseActivity. nearestLocations.add(neartestPlacesData);
                    }

                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onClick(View v) {

    }

    public void showToast(String msg) {
        baseActivity.showToast(msg);
    }

    public void showToast(String msg, int length) {
        baseActivity.showToast(msg, length);
    }

    private void progressDialog() {
        dialog = new Dialog(baseActivity);
        View view = View.inflate(baseActivity, R.layout.progress_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txtMsgTV = (TextView) view.findViewById(R.id.txtMsgTV);
        dialog.setCancelable(false);
    }

    @Override
    public void onPause() {
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        super.onPause();
    }


    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        if (controller.equalsIgnoreCase("journey") && action.equalsIgnoreCase("detail")) {
            if (status) {
            }
        }
    }


    public void initFCM() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        store.saveString(Const.DEVICE_TOKEN, refreshedToken);
        if (refreshedToken != null) {

            if(SessionManager.getUserDeviceToken(getActivity().getApplicationContext()).equals("UserDeviceToken")){

                SessionManager.setUserDeviceToken(getActivity().getApplicationContext(),refreshedToken);
            }

        }

    }

    @Override
    public void onSyncStart(boolean isNetworkAvail) {
        if (!isNetworkAvail)
            showToast(getString(R.string.no_network_avail));
        else if (getActivity() != null && !dialog.isShowing() && !getActivity().isFinishing()) {
            dialog.show();
        }
    }

    @Override
    public void onSyncFinish() {
        if (getActivity() != null && dialog.isShowing() && !getActivity().isFinishing()) {
            dialog.dismiss();
        }
    }

    @Override
    public void onSyncFailure(int code) {
        log("Error code: " + code);
        baseActivity.handleFailureCodes(code);
    }

    private void log(String s) {
        baseActivity.log(s);
    }

    @Override
    public void onSyncForbidden(int code, String string) {

    }

    @Override
    public void onSyncProgress(long progress, long length) {

    }

    @Override
    public void onSyncFailure(int statusCode, Header[] headers, String responseString, Throwable arg0) {

    }

    @Override
    public void onSyncProgress(int i, int i1) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

}
