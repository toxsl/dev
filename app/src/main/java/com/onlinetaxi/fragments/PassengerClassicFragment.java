package com.onlinetaxi.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;
import com.onlinetaxi.R;
import com.onlinetaxi.adapter.DriverClassicAdapter;
import com.onlinetaxi.adapter.PassengerClassicAdapter;
import com.onlinetaxi.data.AvailableDriverData;
import com.onlinetaxi.helper.SessionManager;

import java.util.ArrayList;

/**
 * Created by Android on 11/17/2016.
 */

public class PassengerClassicFragment extends BaseFragment {

    View view;
    ListView ClassicLV;
    private ArrayList<AvailableDriverData> availableDriverDatas = new ArrayList<>();
    PassengerClassicAdapter passengerClassicAdapter;

    ArrayList<AvailableDriverData> advertlist=new ArrayList<>();
    ArrayList<AvailableDriverData> favouritelist=new ArrayList<>();
    ArrayList<AvailableDriverData> sponsoredlist=new ArrayList<>();
    ArrayList<AvailableDriverData> standardlist=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.driver_classic, container, false);
        availableDriverDatas = SessionManager.getAvailableDriverData(getActivity());
        standardlist.clear();
        sponsoredlist.clear();
        favouritelist.clear();
        advertlist.clear();
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        /*ClassicLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new DetailsFragment()).commit();
            }
        });*/
    }

    private void initUI() {
        ClassicLV = (ListView) view.findViewById(R.id.manageRV);
        for (int i = 0; i < availableDriverDatas.size(); i++) {


            if (availableDriverDatas.get(i).driverType.equals("Advert"))
                advertlist.add(availableDriverDatas.get(i));
            else if (availableDriverDatas.get(i).driverType.equals("Favourite"))
                favouritelist.add(availableDriverDatas.get(i));
            else if (availableDriverDatas.get(i).driverType.equals("Sponsored"))
                sponsoredlist.add(availableDriverDatas.get(i));
            else
                standardlist.add(availableDriverDatas.get(i));
        }


        passengerClassicAdapter = new PassengerClassicAdapter(getActivity(), getActivity().getApplicationContext(), availableDriverDatas);
        ClassicLV.setAdapter(passengerClassicAdapter);

    }


    public void refreshData() {
        availableDriverDatas = SessionManager.getAvailableDriverData(getActivity());
        if (availableDriverDatas.size() == 0) {
            showToast("No Driver found");
        }
        passengerClassicAdapter = new PassengerClassicAdapter(getActivity(), getActivity().getApplicationContext(), availableDriverDatas);
        ClassicLV.setAdapter(passengerClassicAdapter);

    }


}
