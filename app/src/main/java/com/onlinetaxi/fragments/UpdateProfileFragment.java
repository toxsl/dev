package com.onlinetaxi.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.fragments.driver.DriverProfileFragment;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.MyLocationListener;
import com.onlinetaxi.helper.RegexValiations;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.utils.Const;
import com.onlinetaxi.utils.FontManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;


import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

/**
 * Created by TOXSL/utkarsh.shukla on 10/10/16.
 */
public class UpdateProfileFragment extends BaseFragment implements BaseActivity.PermCallback {

    private View view;
    private TextView emailTVF, addressTVF, numberTVF;
    private RelativeLayout submitLayout;
    Typeface iconFont1;
    ImageView userimg_change;
    EditText UserNameET, UserEmailET, UserContactET, UserAddressET;
    private Uri fileUri;
    long totalSize = 0;
    ProgressDialog loading;

    ProgressDialog dialog;
    UpdateProfileTask task;
    private Dialog alertDialogProgressBar;
    CoordinatorLayout parentLayout;
    String uploadImageResponse;
    private File croppedFile;
    private Uri selectedImageUri;
    private String fileUriPath = "";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static final int GALLERY_CAPTURE_IMAGE_REQUEST_CODE = 300;
    final int PIC_CROP = 400;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_update_profile, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        emailTVF = (TextView) view.findViewById(R.id.fontEmail);
        addressTVF = (TextView) view.findViewById(R.id.fontIconLocation);
        submitLayout = (RelativeLayout) view.findViewById(R.id.sendbutton);
        numberTVF = (TextView) view.findViewById(R.id.fontIconContact);
        userimg_change = (ImageView) view.findViewById(R.id.profile_user_pic);
        UserNameET = (EditText) view.findViewById(R.id.UserNameET);
        UserEmailET = (EditText) view.findViewById(R.id.UserEmailET);
        UserAddressET = (EditText) view.findViewById(R.id.UserAddressET);
        UserContactET = (EditText) view.findViewById(R.id.UserContactET);
        parentLayout = (CoordinatorLayout) view.findViewById(R.id.parentLayout);
        emailTVF.setTypeface(baseActivity.iconFont);
        addressTVF.setTypeface(baseActivity.iconFont);
        numberTVF.setTypeface(baseActivity.iconFont);

        baseActivity.setPermCallback(this);
        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontCircle), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontSend), iconFont1);


        submitLayout.setOnClickListener(this);


        try {
            UserNameET.setText(SessionManager.getUserFirstName(getActivity()));
            UserEmailET.setText(SessionManager.getUserEmail(getActivity()));
            UserContactET.setText(SessionManager.getUserContactNumber(getActivity()));
            UserAddressET.setText(SessionManager.getUserAddress(getActivity()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        userimg_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 99)) {
                    selectImageFromGallery();
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendbutton:

                if (ApplicationGlob.isConnectingToInternet(getActivity())) {
                    if (checkValidation()) {

                        task = new UpdateProfileTask();
                        task.execute(
                                SessionManager.getUserCountryCode(getActivity()),
                                SessionManager.getUserID(getActivity()),
                                UserNameET.getText().toString(),
                                UserEmailET.getText().toString(),
                                UserContactET.getText().toString(),
                                UserAddressET.getText().toString());

                    }

                } else {

                    ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
                }

                break;
        }
    }

    @Override
    public void permGranted(int resultCode) {
        selectImageFromGallery();
    }

    @Override
    public void permDenied(int resultCode) {

    }


    private class UpdateProfileTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_update_user_profile.php";

                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("pfirstname", paramsa[2]);
                params.put("plastname", "");
                params.put("pemail", paramsa[3]);
                params.put("pcontactno", paramsa[4]);
                params.put("paddress", paramsa[5]);
                params.put("action", "user_profile_update");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("Update Profile Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");


                if (returncode.equals("0")) {

                    SessionManager.setUserEmail(getActivity(), UserEmailET.getText().toString());
                    SessionManager.setUserFirstName(getActivity(), UserNameET.getText().toString());
                    SessionManager.setUserAddress(getActivity(), UserAddressET.getText().toString());
                    SessionManager.setUserContactNumber(getActivity(), UserContactET.getText().toString());

                    if (SessionManager.getUserType(getActivity()).equals(Const.USER_DRIVER))
                        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new DriverProfileFragment()).commit();
                    else if (SessionManager.getUserType(getActivity()).equals(Const.USER_PASSENGER))
                        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ProfileFragment()).commit();

                } else {
                    String excptn = jsoObject1.getString("message");
                    SnackBarOnUiThread(excptn);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void SnackBarOnUiThread(final String message) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Snackbar snackbar = Snackbar
                            .make(parentLayout, message, Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            });

        }

    }

    private void launchUploadActivity(boolean isImage) {
        Intent i = new Intent(getActivity(), UploadPhotoPassenger.class);
        i.putExtra("filePath", fileUri.getPath());
        i.putExtra("isImage", isImage);
        getActivity().startActivity(i);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                launchUploadActivity(true);


            } else if (resultCode == RESULT_CANCELED) {

            } else {

            }

        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                launchUploadActivity(false);

            } else if (resultCode == RESULT_CANCELED) {


            } else {

            }
        } else if (requestCode == GALLERY_CAPTURE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            selectedImageUri = data.getData();

            if (checkPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 99)) {
                fileUriPath = getPath(getActivity(), selectedImageUri);
            }
            performCrop();


        } else if (requestCode == PIC_CROP) {


            if (resultCode == RESULT_OK) {

                new UploadFileToServer().execute();


            } else {
                if (croppedFile != null) {
                    Log.i("Camera", "result cancel. Hence, deleting file: " + croppedFile.getPath());
                    Log.i("File deleted ", croppedFile.delete() + "");
                }

            }


        }
    }


    private void performCrop() {
        //call the standard crop action intent (the user device may not support it)

        try {

            File f = new File(fileUriPath);
            Uri contentUri = Uri.fromFile(f);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(contentUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280); // retrieve data on return
            cropIntent.putExtra("return-data", true);
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + File.separator + "directory_name_corp_chat" + File.separator + "directory_name_temp");
            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            try {


                if (checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 99)) {
                    croppedFile = File.createTempFile("TEMP_CROPPED_IMG_" + timeStamp, ".jpg", mediaStorageDir);
                }
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(croppedFile));
                startActivityForResult(cropIntent, PIC_CROP);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) { // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Snackbar snackbar = Snackbar
                    .make(parentLayout, errorMessage, Snackbar.LENGTH_LONG);
            snackbar.show();


           /* Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();*/
        }


    }


    private void uploadImage() {
        //Showing the progress dialog


        getActivity().runOnUiThread(new Runnable() {
            public void run() {

                loading = ProgressDialog.show(getActivity(), "Uploading...", "Please wait...", false, false);

            }
        });

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.FILE_UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        uploadImageResponse = s;

                        //Showing toast message of the response
                        Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        //Showing toast
                        Toast.makeText(getActivity(), volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                // String image = getStringImage(bitmap);

                File sourceFile = new File(croppedFile.getAbsolutePath());

                //Getting Image Name
                //  String name = editTextName.getText().toString().trim();

                //Creating parameters
                Map<String, String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("pisocode", SessionManager.getUserCountryCode(getActivity()));
                params.put("puserid", SessionManager.getUserID(getActivity()));
                params.put("pdocumentype", "PROFILE");
                params.put("pname", SessionManager.getUserFirstName(getActivity()));
                params.put("image", sourceFile.toString());
                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        //Adding request to the queue
        requestQueue.add(stringRequest);


    }


    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            super.onPreExecute();
            // progressbar(true);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {

            uploadImage();

            return uploadImageResponse;
        }


        @Override
        protected void onPostExecute(String result) {
            Log.e("yoyo", "Response from server: " + result);

            //    progressbar(false);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONObject jsonobj1 = jsonObject.getJSONObject("Response");
                JSONObject jsoObject2 = jsonobj1.getJSONObject("0");
                String returncode = jsoObject2.getString("returncode");

                if (returncode.equals("0")) {
                }





                 /*   String imgurl = jsoObject1.getString("imageurl");

                    SessionManager.setUserProfilePhoto(getApplicationContext(), imgurl);
                    //   Toast.makeText(UpdateProfileOfPassenger.this, "Profile pic updated successfully", Toast.LENGTH_SHORT).show();

                    //finish();
                    bitmap = BitmapFactory.decodeFile(croppedFile.getAbsolutePath());


                    guestuser.setImageBitmap(bitmap);
                    guestuser.setScaleType(ImageView.ScaleType.FIT_XY);
                    userimg_change.setImageBitmap(bitmap);
                    userimg_change.setScaleType(ImageView.ScaleType.FIT_XY);

                    Picasso.with(getApplicationContext())
                            .load(SessionManager.getUserProfilePhoto(getApplicationContext()))
                            .placeholder(R.drawable.nouserimage)
                            .error(R.drawable.nouserimage)
                            .into(userimg_change);

                    Picasso.with(getApplicationContext())
                            .load(SessionManager.getUserProfilePhoto(getApplicationContext()))
                            .placeholder(R.drawable.nouserimage)
                            .error(R.drawable.nouserimage)
                            .into(guestuser);


                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            Snackbar snackbar = Snackbar
                                    .make(parentLayout, "Problem occured in Uploading.", Snackbar.LENGTH_LONG);
                            snackbar.show();


                            //  Toast.makeText(UpdateProfileOfPassenger.this, "Problem occured in uploading", Toast.LENGTH_SHORT).show();
                        }
                    });*/


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            // showing the server response in an alert dialog
            //showAlert(result);

            //SessionManager.getUserProfilePhoto(getApplicationContext())
            super.onPostExecute(result);
        }
    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!RegexValiations.hasText(UserNameET))
            ret = false;

        if (!RegexValiations.hasText(UserContactET))
            ret = false;

        if (!RegexValiations.isEmailAddress(UserEmailET, true))
            ret = false;

        if (!RegexValiations.hasText(UserContactET))
            ret = false;
        return ret;
    }


    public void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                GALLERY_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {

            mediaStorageDir.mkdirs();


          /*  if (!mediaStorageDir.mkdirs()) {
                Log.d("TAG", "Oops! Failed create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }*/
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


}
