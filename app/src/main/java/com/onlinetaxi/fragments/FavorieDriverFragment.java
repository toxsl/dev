package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.activity.MainActivity;

/**
 * Created by TOXSL/utkarsh.shukla on 5/10/16.
 */
public class FavorieDriverFragment extends BaseFragment {
    private View view;
    private TextView rateDriverTV,reportDriverTV;
    private ImageView drawerIconIV;
    private TextView nameTVF,homeTVF,phoneTVF;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite_driver, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        rateDriverTV = (TextView) view.findViewById(R.id.rateDriverTV);
        rateDriverTV.setOnClickListener(this);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        reportDriverTV= (TextView) view.findViewById(R.id.reportDriverTV);
        reportDriverTV.setOnClickListener(this);

        nameTVF=(TextView)view.findViewById(R.id.nameTVF);
        nameTVF.setTypeface(baseActivity.iconFont);
        homeTVF=(TextView)view.findViewById(R.id.homeTVF);
        homeTVF.setTypeface(baseActivity.iconFont);
        phoneTVF=(TextView)view.findViewById(R.id.phoneTVF);
        phoneTVF.setTypeface(baseActivity.iconFont);
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        FragmentManager fragmentManager = getFragmentManager();
        switch (v.getId()) {
            case R.id.rateDriverTV:
                fragment = new RateDriverFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.reportDriverTV:
                fragment=new ReportDriverFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container,fragment).commit();
                break;

        }

    }
}
