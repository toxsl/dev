package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.CardAdapter;
import com.onlinetaxi.data.CardsModel;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.ormdata.AppDataBaseOrm;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class ProfileFragment extends BaseFragment {

    private Button submitFAB;
    private TextView editIV;
    private boolean isEditMode;
    private EditText emailET, phoneET;
    private TextView paswordTV, pointsTV, amountTV;
    private ImageView drawerIconIV;
    private TextView addressTVF, contactTVF, emailTVF;
    Dialog alertDialogProgressBar;
    List<AppDataBaseOrm> cardlist = new ArrayList<>();
    CardAdapter cardAdapter;
    private ListView listView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        submitFAB = (Button) view.findViewById(R.id.submitFAB);
        editIV = (TextView) view.findViewById(R.id.editIV);
        editIV.setTypeface(baseActivity.iconFont);
        emailET = (EditText) view.findViewById(R.id.emailET);
        phoneET = (EditText) view.findViewById(R.id.phoneET);
        paswordTV = (TextView) view.findViewById(R.id.paswordTV);
        pointsTV = (TextView) view.findViewById(R.id.pointsTV);

        listView = (ListView) view.findViewById(R.id.listviewCARD);

        amountTV = (TextView) view.findViewById(R.id.amountTV);

        addressTVF = (TextView) view.findViewById(R.id.addressTVF);
        addressTVF.setTypeface(baseActivity.iconFont);
        contactTVF = (TextView) view.findViewById(R.id.contactTVF);
        contactTVF.setTypeface(baseActivity.iconFont);
        emailTVF = (TextView) view.findViewById(R.id.emailTVF);
        emailTVF.setTypeface(baseActivity.iconFont);

        pointsTV.setOnClickListener(this);
        editIV.setOnClickListener(this);
        submitFAB.setOnClickListener(this);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);


        setData();

        drawerIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

            }
        });
        paswordTV.setOnClickListener(this);
    }

    private void setData() {
        emailET.setText(SessionManager.getUserEmail(getContext()));
        emailET.setTextColor(getResources().getColor(R.color.font_color));
        phoneET.setText(SessionManager.getUserContactNumber(getContext()));
        phoneET.setTextColor(getResources().getColor(R.color.font_color));
        amountTV.setText("R " + SessionManager.getMillageAmount(getContext()));


        try {
            cardlist = AppDataBaseOrm.getAllList(getActivity());

        } catch (Exception e) {
            // TODO: handle exception
        }

        cardAdapter = new CardAdapter(getActivity(), R.layout.custon_card_row, cardlist);
        listView.setAdapter(cardAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new AddcardFragment()).commit();
                break;
            case R.id.editIV:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new UpdateProfileFragment()).addToBackStack(null).commit();
                // edit(!isEditMode);
                break;
            case R.id.pointsTV:
                confirmbuymillageDialog();
                break;
            case R.id.paswordTV:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ChangePasswordFragment()).commit();
                break;


        }
    }

    private void confirmbuymillageDialog() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_purchase_millagepoint, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Purchase", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

    }

    private void edit(boolean b) {
        isEditMode = b;
        emailET.setEnabled(b);
        phoneET.setEnabled(b);
    }

}
