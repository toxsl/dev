package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.mobile.connect.PWConnect;
import com.mobile.connect.exception.PWError;
import com.mobile.connect.exception.PWException;
import com.mobile.connect.exception.PWProviderNotInitializedException;
import com.mobile.connect.listener.PWTransactionListener;
import com.mobile.connect.payment.PWCurrency;
import com.mobile.connect.payment.PWPaymentParams;
import com.mobile.connect.provider.PWTransaction;
import com.mobile.connect.service.PWProviderBinder;
import com.onlinetaxi.R;

import com.onlinetaxi.adapter.CardAdapter;
import com.onlinetaxi.data.CardsModel;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.CryptoUtil;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.ormdata.AppDataBaseOrm;
import com.onlinetaxi.utils.FontManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Android on 10/21/2016.
 */

public class CardPaymentFragment extends BaseFragment implements PWTransactionListener {

    ArrayList<AppDataBaseOrm> cardlist = new ArrayList<>();
    LinearLayout AddCardLL;
    ListView listView;
    CardAdapter cardAdapter;
    Typeface iconFont1;
    TextView fontStar1,fontStar2,fontStar3,fontStar4,fontStar5;
    private PWProviderBinder _binder;
    private static final String APPLICATIONIDENTIFIER = "peach.OnlineTaxi.mcommerce.test";
    private static final String PROFILETOKEN = "652534dcdd5c435eb6aab260ce37a5af";
    Dialog alertDialogProgressBar;
    CoordinatorLayout parent;
    public boolean IsSavedPAyment = false;
    public boolean paymnetselection = false;
    public static String PeachPAymentUniQueID = "";
    List<AppDataBaseOrm> list;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.card_payment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        getActivity().startService(new Intent(getActivity(),
                com.mobile.connect.service.PWConnectService.class));
        getActivity().bindService(new Intent(getActivity(),
                        com.mobile.connect.service.PWConnectService.class),
                _serviceConnection, Context.BIND_AUTO_CREATE);

        listView = (ListView) view.findViewById(R.id.listviewCARD);
        parent=(CoordinatorLayout)view.findViewById(R.id.parent);

        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        fontStar1=(TextView)view.findViewById(R.id.fontStar1);
        fontStar2=(TextView)view.findViewById(R.id.fontStar2);
        fontStar3=(TextView)view.findViewById(R.id.fontStar3);
        fontStar4=(TextView)view.findViewById(R.id.fontStar4);
        fontStar5=(TextView)view.findViewById(R.id.fontStar5);



        FontManager.markAsIconContainer(fontStar1,iconFont1);
        FontManager.markAsIconContainer(fontStar2,iconFont1);
        FontManager.markAsIconContainer(fontStar3,iconFont1);
        FontManager.markAsIconContainer(fontStar4,iconFont1);
        FontManager.markAsIconContainer(fontStar5,iconFont1);

        float rating = Math.round(Float.valueOf("3"));


        for (int i = 1; i <= rating; i++) {

            if (i == 1) {

                fontStar1.setTextColor(Color.parseColor("#e77c12"));
            }

            if (i == 2) {

                fontStar2.setTextColor(Color.parseColor("#e77c12"));
            }
            if (i == 3) {

                fontStar3.setTextColor(Color.parseColor("#e77c12"));
            }
            if (i == 4) {

                fontStar4.setTextColor(Color.parseColor("#e77c12"));
            }
            if (i == 5) {

                fontStar5.setTextColor(Color.parseColor("#e77c12"));
            }
        }

        callDatabase();



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                CryptoUtil cryptoUtil = new CryptoUtil();
                final String tokenid = cryptoUtil.decryptIt(list.get(position)
                        .getTokenid());
                final String wncrptedtokenid = list.get(position).getTokenid();
                final String cardnumber = list.get(position).getCardnumber();
                final String cardtype = list.get(position).getCardType();
                final long sugarid = list.get(position).getId();
                final String OriginalCardNumber = list.get(position).getOriginalCardnumber();




                selectCardandPay(tokenid,wncrptedtokenid,cardnumber,cardtype,sugarid,OriginalCardNumber);
            }
        });

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        callDatabase();
    }
    public void callDatabase() {

        try {
            list = AppDataBaseOrm.getAllList(getActivity());

        } catch (Exception e) {
            // TODO: handle exception
        }

       /* if (list.size() == 0) {

            layoutlistciew.setVisibility(View.GONE);

            linearLayoutbleow.setVisibility(View.INVISIBLE);
            layoutAbove.setVisibility(View.VISIBLE);


        } else {
            layoutlistciew.setVisibility(View.VISIBLE);

            linearLayoutbleow.setVisibility(View.VISIBLE);
            layoutAbove.setVisibility(View.GONE);

        }
*/
        Collections.reverse(list);

        CardAdapter cardAdapter = new CardAdapter(
                getActivity(), R.layout.custon_card_row, list);

        listView.setAdapter(cardAdapter);
        cardAdapter.notifyDataSetChanged();

    }


    private void selectCardandPay(final String tokenid,final String wncrptedtokenid,final String cardnumber,final String cardtype,final long sugarid,final String OriginalCardNumber) {
        View dialoglayout = View.inflate(baseActivity, R.layout.choose_card, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");
        LinearLayout layoutcheckbox = (LinearLayout) dialoglayout.findViewById(R.id.fds);
        layoutcheckbox.setVisibility(View.VISIBLE);

        final CheckBox checkBox = (CheckBox) dialoglayout
                .findViewById(R.id.checkBox1);

        if (SessionManager.getTokenNumber(getActivity())
                .toString().equals(tokenid)) {

            layoutcheckbox.setVisibility(View.INVISIBLE);

        } else {

        }

        layoutcheckbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                checkBox.setChecked(true);
                checkBox.invalidate();

            }
        });


        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("ACCEPT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                if (checkBox.isChecked()) {

                    SessionManager.setTokenNumber(
                            getActivity(), tokenid);

                    SessionManager.setStoredCardNumber(getActivity(), cardnumber);

                    AppDataBaseOrm.DeleteImformations(sugarid);

                    Log.e("tokenid", cardnumber);
                    Log.e("tokenid", cardtype);

                    Log.e("tokenid", wncrptedtokenid);

                    AppDataBaseOrm information = new AppDataBaseOrm(
                            cardnumber, wncrptedtokenid, cardtype,
                            SessionManager.getUserID(getActivity()), OriginalCardNumber);

                    AppDataBaseOrm.insertImformations(
                            getActivity(), information);

                }

                dialog.dismiss();

                // TODO Auto-generated method stub
            //    FromPAymentSelection = true;
                IsSavedPAyment = true;
                progressbar(true, "Preparing...");
                setStatusText("Preparing...");
                // collect input
                double amount = Double.parseDouble("10") * Double.parseDouble("10");

                final String ActualFair = String.format("%.2f", new BigDecimal(amount)).replaceAll("[$,]", ".");

                amount = Double.parseDouble(ActualFair);
                // create token parameters
                PWPaymentParams tokenParams = null;
                try {
                    tokenParams = _binder.getPaymentParamsFactory()
                            .createTokenPaymentParams(amount,
                                    PWCurrency.SOUTH_AFRICA_RAND,
                                    "nosubject", tokenid);
                    tokenParams.setCustomerGivenName(SessionManager.getUserFirstName(getActivity()));
                    tokenParams.setCustomerAddressStreet(SessionManager.getUserAddress(getActivity()));
                    tokenParams.setCustomerAddressZip(SessionManager.getUserZipcode(getActivity()));
                    tokenParams.setCustomerAddressCity(SessionManager.getUsercity(getActivity()));
                    tokenParams.setCustomerAddressState(SessionManager.getUserState(getActivity()));
                    tokenParams.setCustomIdentifier("");
                    tokenParams.setCustomerAddressCountryCode(SessionManager.getUserCountryCode(getActivity()));
                    tokenParams.setCustomerEmail(SessionManager.getUserEmail(getActivity()));

                   // Log.e("Payment SelOC params", "XXX" + " " + UserModel.getEmail());
                } catch (PWProviderNotInitializedException e) {
                    setStatusText("Error: Provider not initialized!");
                    e.printStackTrace();
                    return;
                } catch (PWException e) {
                    setStatusText("Error: Invalid Parameters!");
                    e.printStackTrace();
                    return;
                }

                //      setStatusText("Preparing...");
                currentTokenization = false; // now we do a debit
                // transaction
             //   FromPAymentSelection = true;
                paymnetselection = true;
                try {
                    //  _binder.createAndRegisterDebitTransaction(tokenParams);
                    _binder.createAndRegisterPreauthorizationTransaction(tokenParams);
                } catch (PWException e) {
                    setStatusText("Error: Could not contact Gateway!");
                    e.printStackTrace();
                }


            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }



    @Override
    public void transactionSucceeded(PWTransaction pwTransaction) {
        if (IsSavedPAyment == true) {
            if (!currentTokenization) { // our debit succeeded
                  // setStatusText("Charged token " + DriverModel.getDriverProfile().getDriverRate() + " RAND!");


                PeachPAymentUniQueID = pwTransaction.getProcessorUniqueIdentifier();
                Log.e("peach_payment_id: ",PeachPAymentUniQueID);

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        Snackbar snackbar = Snackbar
                                .make(parent, "Transaction Successfully done", Snackbar.LENGTH_LONG);
                        snackbar.show();

                    }
                });


              //  FromPAymentSelection = false;
              //  paymnetselection = false;


              /*  if (ApplicationGlob.isConnectingToInternet(getActivity())) {


                    TaskPayment_proc taskPayment_proc = new TaskPayment_proc();

                    taskPayment_proc.execute();


                } else {

                    ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
                }
*/



            }

        }
    }

    @Override
    public void transactionFailed(PWTransaction pwTransaction, PWError pwError) {
        if (IsSavedPAyment == true) {
         //   FromPAymentSelection = false;
            paymnetselection = false;
            setStatusText("Error contacting the gateway.");
            Log.e("com.paywzationActivity",
                    pwError.getErrorMessage());
        }
    }

    @Override
    public void creationAndRegistrationSucceeded(PWTransaction pwTransaction) {
        if (IsSavedPAyment == true) {
            // check if it is our registration transaction
            //  setStatusText("Processing...");
            if (currentTokenization) {
                // execute it
                try {
                    _binder.obtainToken(pwTransaction);
                } catch (PWException e) {
                    setStatusText("Invalid Transaction.");
                    e.printStackTrace();
                }
            } else {
                // execute it
                try {
                    //  _binder.debitTransaction(transaction);
                    _binder.preauthorizeTransaction(pwTransaction);
                } catch (PWException e) {
                    setStatusText("Invalid Transaction.");
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void creationAndRegistrationFailed(PWTransaction pwTransaction, PWError pwError) {

        if (IsSavedPAyment == true) {
            setStatusText("Error contacting the gateway.");
            Log.e("com.ionActivity",
                    pwError.getErrorMessage());
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            getActivity().unbindService(_serviceConnection);
            getActivity().stopService(new Intent(getActivity(),
                    com.mobile.connect.service.PWConnectService.class));
        } catch (Exception e) {

        }


    }

    private ServiceConnection _serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _binder = (PWProviderBinder) service;
            // we have a connection to the service
            try {
                _binder.initializeProvider(PWConnect.PWProviderMode.TEST,
                        APPLICATIONIDENTIFIER, PROFILETOKEN);

                _binder.addTransactionListener(CardPaymentFragment.this);
            } catch (PWException ee) {
                setStatusText("Error initializing the provider.");
                // error initializing the provider
                ee.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            _binder = null;
        }
    };
    private boolean currentTokenization;

    private void setStatusText(final String string) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {

                progressbar(true, string);

                Snackbar snackbar = Snackbar
                        .make(parent, string, Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        });
    }

    public void progressbar(boolean b, String string) {
        alertDialogProgressBar = new Dialog(getActivity(), R.style.YourCustomStyle);

        alertDialogProgressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.layout_progressbar);
        alertDialogProgressBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textView = (TextView) alertDialogProgressBar.findViewById(R.id.textView1);

        textView.setVisibility(View.INVISIBLE);
        textView.setText(string);

        if (b == true) {
            alertDialogProgressBar.show();
        } else {

            alertDialogProgressBar.dismiss();
        }


    }

}
