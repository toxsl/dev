package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.HistoryMenuListAdapter;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.data.HistoryMenuListData;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TOXSL/utkarsh.shukla on 6/10/16.
 */
public class ReportDriverFragment extends BaseFragment {

    private View view;
    private ImageView drawerIconIV;
    private EditText commentET;
    private CheckBox reportDriverCB;
    private FloatingActionButton submitFAB;
    private int checked = 0;
    ReportDriverAPI reportDriverAPI;
    private Dialog alertDialogProgressBar;
    ArrayList<HistoryData> historyDatas = new ArrayList<>();


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_report_driver, container, false);
        historyDatas = SessionManager.getHistoryData(getActivity());

        initUI();
        return view;
    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        reportDriverCB = (CheckBox) view.findViewById(R.id.reportDriverCB);

        reportDriverCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    checked = 1;
                else
                    checked = 0;
            }
        });

        commentET = (EditText) view.findViewById(R.id.commentET);
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;

            case R.id.submitFAB:
                if (commentET.getText().toString().trim().isEmpty())
                    Snackbar.make(view, "Please enter comment", Snackbar.LENGTH_SHORT).show();
                else {

                    int position = Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()));
                    if (historyDatas.get(position).drivercode!=null) {
                        reportDriverAPI = new ReportDriverAPI();
                        reportDriverAPI.execute(SessionManager.getUserCountryCode(getActivity()),//0
                                SessionManager.getUserID(getActivity()),//1
                                historyDatas.get(position).drivercode// 2
                                , historyDatas.get(position).reference,//3
                                commentET.getText().toString().trim(),//4
                                checked + "");//5
                    }
                    else {
                        Snackbar.make(view, "Something went wrong! try again latter.", Snackbar.LENGTH_LONG).show();
                    }
                }


                break;
        }
    }


    private class ReportDriverAPI extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za//mobile_report_users.php";

                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("report", paramsa[2]);
                params.put("preference", paramsa[3]);
                params.put("pcomments", paramsa[4]);
                params.put("pspammed", paramsa[5]);
                params.put("action", "report");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("Update Profile Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray Response = jsonObject.getJSONArray("Response");
                JSONObject object = Response.getJSONObject(0);
                if (object.optString("message").equalsIgnoreCase("Success")) {
                    if (object.optString("returncode").equals("0")) {
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);
            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }


}
