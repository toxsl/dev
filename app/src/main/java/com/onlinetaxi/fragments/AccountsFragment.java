package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.CardAdapter;
import com.onlinetaxi.adapter.FavListAdapter;
import com.onlinetaxi.data.CardsModel;
import com.onlinetaxi.data.FavListData;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.ormdata.AppDataBaseOrm;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by TOXSL/utkarsh.shukla on 25/10/16.
 */
public class AccountsFragment extends BaseFragment {
    private View view;
    private TextView cardTVF,StarTVF;
    private ListView cardListLV;
    private CardAdapter cardAdapter;
    List<AppDataBaseOrm> Cardlist = new ArrayList<>();
    private ImageView drawerIconIV;
    Dialog alertDialogProgressBar;
    ImageView BackIconIV;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_accounts, container, false);
        cardTVF = (TextView) view.findViewById(R.id.cardTVF);
        StarTVF = (TextView) view.findViewById(R.id.StarTVF);
        BackIconIV = (ImageView) view.findViewById(R.id.BackIconIV);

        cardListLV = (ListView) view.findViewById(R.id.cardListLV);
        Cardlist = AppDataBaseOrm.getAllList(getActivity().getApplicationContext());
        cardAdapter = new CardAdapter(baseActivity, 0,Cardlist);
        cardListLV.setAdapter(cardAdapter);
        cardTVF.setTypeface(baseActivity.iconFont1);
        StarTVF.setTypeface(baseActivity.iconFont1);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        cardTVF.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        cardListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


            }
        });


        cardListLV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

              // adapterView.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.light_grey));

                BackIconIV.setVisibility(View.VISIBLE);
                drawerIconIV.setVisibility(View.GONE);
                StarTVF.setVisibility(View.VISIBLE);
                cardTVF.setVisibility(View.GONE);

                return false;
            }
        });

        BackIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerIconIV.setVisibility(View.VISIBLE);
                BackIconIV.setVisibility(View.GONE);
                StarTVF.setVisibility(View.GONE);
                cardTVF.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardTVF:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new AddcardFragment()).addToBackStack(null).commit();
                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        cardAdapter = new CardAdapter(baseActivity, 0,Cardlist);
        cardListLV.setAdapter(cardAdapter);

    }


    private class ShowAllCards extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    progressbar(true, "");
                }
            });

        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_show_payment_card.php";
                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("submit", "show_payment_card");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("OT Login Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    progressbar(false,"");
                }
            });



            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");

                if (returncode.equals("0")) {
                    String massage=jsoObject1.getString("message");



                }else{

                    String massage=jsoObject1.getString("message");
                    ToastOnUiThread(massage);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void ToastOnUiThread(final String message) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity().getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    public void progressbar(boolean b, String string) {

        alertDialogProgressBar = new Dialog(getActivity(), R.style.YourCustomStyle);
        alertDialogProgressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.layout_progressbar);
        alertDialogProgressBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textView = (TextView) alertDialogProgressBar.findViewById(R.id.textView1);

        textView.setVisibility(View.INVISIBLE);
        textView.setText(string);

        if (b == true) {
            alertDialogProgressBar.show();
        } else {

            alertDialogProgressBar.dismiss();
        }


    }

}
