package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.HistoryAdapter;
import com.onlinetaxi.adapter.HistoryMenuListAdapter;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.data.HistoryMenuListData;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TOXSL/utkarsh.shukla on 6/10/16.
 */
public class ManageHistoryFragment extends BaseFragment {
    private View view;
    private ImageView drawerIconIV;
    private Dialog alertDialogProgressBar;
    HistoryMenu historyMenu;
    ArrayList<HistoryData> historyDatas = new ArrayList<>();
    private int position;
    private ListView historyMenulistLV;
    ArrayList<HistoryMenuListData> historyMenuListDatas = new ArrayList<>();
    HistoryMenuListAdapter historyMenuListAdapter;
    private TextView reffnumTV;
    private EditText destinationET, pickupaddressET;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_manage_history, container, false);
        historyDatas = SessionManager.getHistoryData(getActivity());
        position = Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()));
        initUI();
        if (getArguments().containsKey("position")) {
            position = getArguments().getInt("position");
        }



       /* pickupaddressET.setText(historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).pickup);
        destinationET.setText(historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).DropOff);*/

        historyMenu = new HistoryMenu();
        historyMenu.execute(SessionManager.getUserCountryCode(getActivity()),
                SessionManager.getUserID(getActivity()), historyDatas.get(position).reference);


        return view;
    }

    private void initUI() {
        reffnumTV = (TextView) view.findViewById(R.id.reffnumTV);
        reffnumTV.setText("Refrence: " + historyDatas.get(position).reference);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        historyMenulistLV = (ListView) view.findViewById(R.id.historyMenulistLV);
        historyMenuListDatas.clear();
        historyMenuListDatas = SessionManager.getHistoryMenuListData(getActivity());
        historyMenulistLV.setAdapter(new HistoryMenuListAdapter(baseActivity, 0, historyMenuListDatas));

        pickupaddressET = (EditText) view.findViewById(R.id.pickupaddressET);
        pickupaddressET.setText(historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).pickup);
        destinationET = (EditText) view.findViewById(R.id.destinationET);
        destinationET.setText(historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).DropOff);
        historyMenulistLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                click(Integer.parseInt(SessionManager.getHistoryMenuListData(getActivity()).get(position).menu_no));
            }
        });

    }

    public void click(int menuNo) {
        Fragment fragment = null;
        FragmentManager fragmentManager = getFragmentManager();
        switch (menuNo) {
            case 1://RateDriver
                fragment = new RateDriverFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                break;
            case 2://Re-book Trip
                fragment = new PassengerMapAndClassicFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                break;
            case 3://Cancel Trip
                fragment = new CancellationFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                break;
            case 4://View Trip
                fragment = new ProgressFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                break;
            case 5://Report Driver
                fragment = new ReportDriverFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                break;
            case 6://Show Favorite
                fragment = new FavorieDriverFragment();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();

                break;
            case 7://Remove Trip
                removeDialog();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    private void removeDialog() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_remove_trip, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private class HistoryMenu extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_passenger_history_menu.php";

                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("preference", paramsa[2]);
                params.put("action", "passenger_menu");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("Update Profile Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray Response = jsonObject.getJSONArray("Response");
                JSONObject obj = Response.getJSONObject(0);
                JSONArray data = obj.getJSONArray("data");

                historyMenuListDatas.clear();
                for (int i = 0; i < data.length(); i++) {
                    JSONObject object = data.getJSONObject(i);
                    HistoryMenuListData historyMenuListData = new HistoryMenuListData();
                    historyMenuListData.menu_name = object.optString("menu");
                    historyMenuListData.menu_no = object.getString("menu_no");
                    historyMenuListDatas.add(historyMenuListData);
                }
                SessionManager.setHistoryMenuListData(getActivity(), historyMenuListDatas);
                historyMenulistLV.setAdapter(new HistoryMenuListAdapter(baseActivity, 0, historyMenuListDatas));


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);
            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }

}
