package com.onlinetaxi.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.CardAdapter;
import com.onlinetaxi.data.CardsModel;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.ormdata.AppDataBaseOrm;
import com.onlinetaxi.utils.FontManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOXSL/utkarsh.shukla on 6/10/16.
 */
public class MillagePointsFragment extends BaseFragment {

    private View view;
    private ImageView drawerIconIV;
    Typeface iconFont1;
    LinearLayout AddCardLL;
    ListView listView;
    CardAdapter cardAdapter;

    List<AppDataBaseOrm> cardlist = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_millage_point, container, false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontDelete), iconFont1);

       /* AddCardLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ApplicationGlob.isConnectingToInternet(getActivity())) {

                    baseActivity.getSupportFragmentManager()
                            .beginTransaction().addToBackStack(null)
                            .replace(R.id.frame_container, new AddcardFragment())
                            .commit();


                } else {

                    ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
                }
            }
        });*/
    }

    private void initUI() {
      //  AddCardLL = (LinearLayout) view.findViewById(R.id.AddCardLL);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

        listView = (ListView) view.findViewById(R.id.listviewCARD);

        try {
            cardlist = AppDataBaseOrm.getAllList(getActivity());

        } catch (Exception e) {
            // TODO: handle exception
        }
        cardAdapter = new CardAdapter(getActivity(), R.layout.custon_card_row, cardlist);
        listView.setAdapter(cardAdapter);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }
}
