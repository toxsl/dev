package com.onlinetaxi.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.GeomagneticField;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.AvailableDriverData;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.mqtt.MqttAndroidClient;
import com.onlinetaxi.utils.Const;
import com.onlinetaxi.utils.FontManager;
import com.onlinetaxi.utils.LatLngInterpolator;
import com.onlinetaxi.utils.MapMarkerInterface;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Android on 11/21/2016.
 */

public class PassengerMapFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BaseActivity.PermCallback, LocationListener, MapMarkerInterface, MqttCallback {

    View view;
    private SupportMapFragment availableMap;
    GoogleMap googlemap;
    RelativeLayout GoTaxiRL;
    ImageView locationmarkerIV;
    private LocationManager mLocationManager;
    Dialog alertDialogPassenger, alertDialogTaxiIcon;
    protected GoogleApiClient mGoogleApiClient;
    private Location currentLocation;
    private int count;
    Typeface iconFont;
    private LatLng taxilatlng;
    private String currentAddress;
    private LatLng pickupLatLng;
    Double latitude;
    Double longitude;
    String Address;
    String clientId, uri;
    MqttAndroidClient mqttAndroidClient;
    private HashMap<String, AvailableDriverData> availableDriverHashmapDatas = new HashMap();
    private ArrayList<AvailableDriverData> availableDriverDatas = new ArrayList<>();


    private IMqttActionListener myMqttActionCallback = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
            baseActivity.log("O yeah!!!!,token: " + asyncActionToken);
            baseActivity.log("O yeah!!!!,topics: " + Arrays.toString(asyncActionToken.getTopics()));
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            baseActivity.log("mar gya re!!!!,token: " + asyncActionToken + " , exception: " + exception);
        }
    };
    private Timer timer;
    private LatLng latLng;
    private HashMap<String, Marker> allSetMarkersData = new HashMap<>();
    private Marker taxiMarker;
    private Marker myMarker;
//    private boolean isFirst;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.passenger_map, container, false);
        inItUI();
        baseActivity.RegisterMapInterface(this);
        uri = SessionManager.getMqttURI(getActivity());
        clientId = SessionManager.getMqttclientID(getActivity());
        mqttAndroidClient = baseActivity.getMqttclient();
        baseActivity.mqttAndroidClient.setCallback(this);
        availableDriverHashmapDatas.clear();
        availableDriverDatas.clear();
        SessionManager.setAvailableDriverData(getActivity(), availableDriverDatas);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontEmail), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontTaxi), iconFont);
        if (checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99)) {
            buildGoogleApiClient();
            initilizeMap();
        }


        locationmarkerIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                latitude = googlemap.getCameraPosition().target.latitude;
                longitude = googlemap.getCameraPosition().target.longitude;
                Address = ApplicationGlob.getCompleteAddressString(latitude, longitude, getActivity().getApplicationContext());
                alertDialogPassenger = new Dialog(getActivity());
                alertDialogPassenger.requestWindowFeature(Window.FEATURE_NO_TITLE);
                alertDialogPassenger.setContentView(R.layout.info_window_pickup_destination);
                alertDialogPassenger.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                alertDialogPassenger.show();
                TextView pickUpText = (TextView) alertDialogPassenger.findViewById(R.id.pickUpText);
                TextView destinationText = (TextView) alertDialogPassenger.findViewById(R.id.destinationText);
                pickUpText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PassengerMapAndClassicFragment.pickupaddressET.setText(Address);
                        alertDialogPassenger.dismiss();

                    }
                });
                destinationText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PassengerMapAndClassicFragment.destinationET.setText(Address);
                        alertDialogPassenger.dismiss();
                    }
                });
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    public void inItUI() {
        locationmarkerIV = (ImageView) view.findViewById(R.id.locationmarkerIV);
        GoTaxiRL = (RelativeLayout) view.findViewById(R.id.GoTaxiRL);
        mLocationManager = (LocationManager) baseActivity.getSystemService(Context.LOCATION_SERVICE);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    private void initilizeMap() {
        availableMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        if (googlemap == null) {
            availableMap.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {
                    googleMap.setInfoWindowAdapter(infoWindowAdapter);
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        googleMap.setMyLocationEnabled(true);
                        googleMap.setPadding(0, 0, 0, 150);
                        googleMap.getUiSettings().setMapToolbarEnabled(false);
                        googleMap.getUiSettings().setZoomControlsEnabled(true);
                        googleMap.getUiSettings().setZoomGesturesEnabled(true);
                        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                            @Override
                            public boolean onMyLocationButtonClick() {
                                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                    currentLocation = getLastKnownLocation();
                                    googlemap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15));
                                    return true;
                                } else {
                                    turnOnGPS_Dialog();
                                }
                                return false;
                            }
                        });
                        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                            @Override
                            public void onMapLoaded() {
                                setMarker();
                            }
                        });
                        googlemap = googleMap;
                        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                                getActivity(), R.raw.map_style);
                        googlemap.setMapStyle(style);

                        googlemap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                            @Override
                            public void onCameraIdle() {

                                Iterator myVeryOwnIterator = allSetMarkersData.keySet().iterator();
                                while (myVeryOwnIterator.hasNext()) {
                                    String key = (String) myVeryOwnIterator.next();
                                    Marker value = allSetMarkersData.get(key);
                                    value.remove();
                                }

                                allSetMarkersData.clear();
                                latLng = googlemap.getCameraPosition().target;
                                LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Spherical();
                                if (myMarker != null)
                                    animateMarkerToGB(latLng,
                                            latLngInterpolator, myMarker);
                                else
                                    myMarker = googlemap.addMarker(new MarkerOptions().position(latLng).draggable(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_car)).title("My Location"));


                                publishMessage(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
                                availableDriverHashmapDatas.clear();
                                availableDriverDatas.clear();
                                SessionManager.setAvailableDriverData(getActivity(), availableDriverDatas);
                                subscribeToTopic(String.valueOf(latLng.latitude) + String.valueOf(latLng.longitude));
                            }

                        });
                    }
                }
            });


        }


    }

    public void refreshData() {
        if (latLng == null) {
            Location location = getLastKnownLocation();
            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        }
        publishMessage(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
        subscribeToTopic(String.valueOf(latLng.latitude) + String.valueOf(latLng.longitude));
    }

    @Override
    public void onResume() {
        super.onResume();
        startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    public void startTimer() {
        stopTimer();
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (handler != null)
                    handler.post(task);
            }
        };
        timer.schedule(timerTask, 1000, Integer.parseInt(SessionManager.getMapRefreshTime(getActivity())) * 1000);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    Runnable task = new Runnable() {
        @Override
        public void run() {
            if (latLng == null) {
                Location location = getLastKnownLocation();
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
            }
            baseActivity.loginAPI(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude), "", "", false, SessionManager.getUserID(getActivity()));

        }
    };
    private GoogleMap.InfoWindowAdapter infoWindowAdapter = new GoogleMap.InfoWindowAdapter() {
        @Override
        public View getInfoWindow(Marker marker) {
            alertDialogTaxiIcon = new Dialog(getActivity());
            alertDialogTaxiIcon.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogTaxiIcon.setContentView(R.layout.info_window_taxi_icon);
            alertDialogTaxiIcon.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            alertDialogTaxiIcon.show();
            TextView textViewAccept = (TextView) alertDialogTaxiIcon.findViewById(R.id.textViewAccept);
            TextView textViewCancel = (TextView) alertDialogTaxiIcon.findViewById(R.id.textViewCancel);
            textViewAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment = new DetailsFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
                    alertDialogTaxiIcon.dismiss();
                }
            });
            textViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialogTaxiIcon.dismiss();
                }
            });
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    };


    public void setMarker() {
        if (currentLocation != null) {
            //  setCurrentMarker();
            baseActivity.latitude = currentLocation.getLatitude();
            baseActivity.longitude = currentLocation.getLongitude();
            GetNearLocations getlocations = new GetNearLocations();
            getlocations.execute();
        } else {
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (count < 5) {
                    count++;
                    currentLocation = getLastKnownLocation();
                    onLocationChanged(currentLocation);
                } else
                    showToast(baseActivity.getString(R.string.cannot_get_location_please_refresh));
            } else
                turnOnGPS_Dialog();
        }
        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            publishMessage(currentLocation.getLatitude() + "", currentLocation.getLongitude() + "");
            subscribeToTopic(String.valueOf(currentLocation.getLatitude()) + String.valueOf(currentLocation.getLongitude()));
            googlemap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15));
            myMarker = googlemap.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())).draggable(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_car)).title("My Location"));
        } else {
            turnOnGPS_Dialog();
        }
    }

    private void subscribeToTopic(String return_topic) {//Subscribe on mQTT
        String topic = return_topic;
        SessionManager.setReturnTopic(getActivity(), topic);
        int qos = 0;
        try {
            IMqttToken subToken = mqttAndroidClient.subscribe(topic, qos);
            subToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    baseActivity.log("Subscribed to topic : " + Arrays.toString(asyncActionToken.getTopics()));
                    // The message was published
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    baseActivity.log("oops! Not subscribed to : " + Arrays.toString(asyncActionToken.getTopics()));
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribeToTopic on the specified topic e.g. using wildcards
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void setCurrentMarker() {
        pickupLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        taxilatlng = new LatLng(currentLocation.getLatitude() + 00.009, currentLocation.getLongitude());
        currentAddress = getAddress(currentLocation.getLatitude(), currentLocation.getLatitude());
        googlemap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15));
        setTaxiMarker();
        // setDriverMarker();

    }

    public void publishMessage(String latitude, String longitude) {//publish on mQTT
        String topic = SessionManager.getState_Topic(getActivity());
        JSONObject object = new JSONObject();
        try {
            object.put("Isocode", "" + SessionManager.getUserCountryCode(getActivity()));
            object.put("Passengercode", "" + SessionManager.getUserID(getActivity()));
            object.put("Latitude", latitude);
            object.put("Longitude", longitude);
            object.put("Return_topic", "" + latitude + longitude);
            object.put("Status", SessionManager.getUserAvailability(getActivity()));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        byte[] encodedPayload = new byte[0];
        try {
            encodedPayload = object.toString().getBytes("UTF-8");
            MqttMessage message = new MqttMessage(encodedPayload);
            mqttAndroidClient.publish(topic, message, this, myMqttActionCallback);
        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }
    }

    private void setTaxiMarker() {
        if (taxiMarker == null)
            taxiMarker = googlemap.addMarker(new MarkerOptions().position(taxilatlng).draggable(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_car)));
        else {
            taxiMarker.setPosition(taxilatlng);
        }
    }


    private void setTaxiMarker(double lat, double lonng, AvailableDriverData availableDriverData) {
        LatLng latLng = new LatLng(lat, lonng);
        if (allSetMarkersData.containsKey(availableDriverData.drivercode)) {
            updateOldMarkerPos(availableDriverData, allSetMarkersData.get(availableDriverData.drivercode));
            return;
        }
        Marker taxiMarker = googlemap.addMarker(new MarkerOptions().position(latLng).draggable(true).title(availableDriverData.driverName).icon(BitmapDescriptorFactory.fromResource(R.drawable.location_car)));
        baseActivity.log("init Rotation: " + taxiMarker.getRotation());
        allSetMarkersData.put(availableDriverData.drivercode, taxiMarker);

    }

    private void updateOldMarkerPos(AvailableDriverData availableDriverData, Marker oldMarker) {
        baseActivity.loge("lat: " + availableDriverData.driverlatitude + " ,long: " + availableDriverData.driverlongitude);
        LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Spherical();

        animateMarkerToGB(new
                        LatLng(Double.parseDouble(availableDriverData.driverlatitude), Double.parseDouble(availableDriverData.driverlongitude)),
                latLngInterpolator, oldMarker);
    }


    public float getAngle(Point target) {
        return (float) Math.toDegrees(Math.atan2(target.x, target.y));
    }

    void animateMarkerToGB(final LatLng finalPosition, final LatLngInterpolator latLngInterpolator, final Marker oldMarker) {
        final LatLng startPosition = oldMarker.getPosition();
        final Handler handler = new Handler();

        double lat1 = startPosition.latitude;
        double lng1 = startPosition.longitude;
        double lat2 = finalPosition.latitude;
        double lng2 = finalPosition.longitude;


        double dLon = (lng2 - lng1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
        double brng = Math.toDegrees((Math.atan2(y, x)));


        baseActivity.log("rotate direction--------------   " + brng + ", old rotation: " + oldMarker.getRotation());
        float ca = oldMarker.getRotation();

        if (ca == 0) {
            ca += 0.1;
        } else if (Math.abs(ca) == 180) {
            ca -= 0.1;
        }
        if (0 < ca && ca < 180) {
            if (0 < brng && brng < 180) {
                if (brng > ca)
                    brng = ca - brng;
            } else if (0 > brng && brng > -180) {
                if (-ca > brng)
                    brng = 360 - (-ca + brng);
            }
        } else if (ca > -180 && ca < 0) {
            if (-180 < brng && brng < 0) {
                if (brng > ca)
                    brng = 360 + brng;
                else
                    brng = ca - brng;
            } else if (0 < brng && brng < 180) {
                if (brng > -ca)
                    brng = -(360 - brng);
            }
        }


        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 3000;
        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {                 // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);
                oldMarker.setPosition(latLngInterpolator.interpolate(v, startPosition, finalPosition));
                //Repeat till progress is complete.
                if (t < 1) {                     // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    static public void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 200;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;
//                rot = Math.abs(rot) > 180 ? rot / 2 : rot;
                Log.e("PMF", "rot: " + rot + " ,torotation: " + toRotation + " ,startrota: " + startRotation);
                marker.setRotation(rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public void turnOnGPS_Dialog() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************
            PendingResult<LocationSettingsResult> result1 =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result1.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result1) {
                    final com.google.android.gms.common.api.Status status = result1.getStatus();
                    final LocationSettingsStates state = result1.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The mqttAndroidClient can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }


    }


    private Location getLastKnownLocation() {
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    mLocationManager.requestLocationUpdates(provider, Const.MIN_TIME_BW_UPDATES, Const.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    l = mLocationManager.getLastKnownLocation(provider);
                }
                if (l != null && (bestLocation == null || l.getAccuracy() > bestLocation.getAccuracy())) {
                    bestLocation = l;
                }
            }
        }
        return bestLocation;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void permGranted(int resultCode) {

        buildGoogleApiClient();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();

    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
//        setMarker();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        currentLocation = getLastKnownLocation();
        onLocationChanged(currentLocation);
    }

    @Override
    public void onProviderDisabled(String s) {
        if (s.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
            turnOnGPS_Dialog();
    }


    @Override
    public void OnLoadedMarkerData(MqttMessage mqttMessage) {
        // update marker code map not null check
    }

    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        if (topic.equals(SessionManager.getReturnTopic(getActivity()))) {
            baseActivity.log("Change Icons......." + message.toString());
            JSONObject object = new JSONObject("" + message);
            AvailableDriverData availableDriverData = new AvailableDriverData();
            availableDriverData.driverlogo = object.optString("logo");
            availableDriverData.isocode = object.optString("isocode");
            availableDriverData.companyName = object.optString("companyname");
            availableDriverData.driverContact = object.optString("contact");
            availableDriverData.companyCode = object.optString("companycode");
            availableDriverData.driverType = object.optString("DriverType");
            availableDriverData.driverKM = object.optString("Driverratekm");
            availableDriverData.driverStatus = object.optString("Status");
            availableDriverData.drivercode = object.optString("drivercode");
            availableDriverData.driverrating = object.optString("Rating");
            availableDriverData.vehicleSeat = object.optString("Vehicleseat");
            availableDriverData.driverlatitude = object.optString("latitude");
            availableDriverData.driverlongitude = object.optString("longitude");
            availableDriverData.driverName = object.optString("Drivername");
            availableDriverData.drivervehicle = object.optString("Vehicle");

            if (myMarker != null) {
                myMarker.remove();
                myMarker = null;
            }
            availableDriverHashmapDatas.put(availableDriverData.drivercode, availableDriverData);
            availableDriverDatas.add(availableDriverData);
            SessionManager.setAvailableDriverData(getActivity(), availableDriverDatas);
            if (object.optString("Status").equalsIgnoreCase("AVAILABLE"))
                setTaxiMarker(Double.parseDouble(availableDriverHashmapDatas.get(availableDriverData.drivercode).driverlatitude),
                        Double.parseDouble(availableDriverHashmapDatas.get(availableDriverData.drivercode).driverlongitude),
                        availableDriverHashmapDatas.get(availableDriverData.drivercode));
        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }


}
