package com.onlinetaxi.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.adapter.FavListAdapter;
import com.onlinetaxi.data.FavListData;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 19/10/16.
 */
public class FragmentFavorite extends BaseFragment {

    private View view;
    private ListView favListLV;
    private ImageView sacnIV;

    private FavListAdapter favListAdapter;
    ArrayList<FavListData> favListDatas = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite, container, false);
        favListLV= (ListView) view.findViewById(R.id.favListLV);
        sacnIV= (ImageView) view.findViewById(R.id.sacnIV);
        sacnIV.setOnClickListener(this);

        favListAdapter = new FavListAdapter(baseActivity, 0,favListDatas);
        favListLV.setAdapter(favListAdapter);

        favListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                // When clicked, show a toast with the TextView text
                Fragment fragment=new FavorieDriverFragment();
                getFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sacnIV:
                Fragment fragment=new ScanDriverFragment();
                getFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
                break;


        }
    }
}
