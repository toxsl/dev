package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.HistoryMenuListAdapter;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.data.HistoryMenuListData;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TOXSL/utkarsh.shukla on 6/10/16.
 */
public class RateDriverFragment extends BaseFragment {
    private View view;
    private FloatingActionButton submitFAB;
    private ImageView drawerIconIV;
    ArrayList<HistoryData> historyDatas = new ArrayList<>();
    Rating rating;
    private RatingBar PriceRB, PunctualityRB, RatingbarProfessionlity;
    private EditText commentET;
    Dialog alertDialogProgressBar;
    private TextView userNameTV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_rate_driver, container, false);
        historyDatas = SessionManager.getHistoryData(getActivity());
        initUI();

        return view;
    }

    private void initUI() {
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

        userNameTV= (TextView) view.findViewById(R.id.userNameTV);
        userNameTV.setText(historyDatas.get(Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()))).drivername);

        PriceRB = (RatingBar) view.findViewById(R.id.PriceRB);
        RatingbarProfessionlity = (RatingBar) view.findViewById(R.id.RatingbarProfessionlity);
        PunctualityRB = (RatingBar) view.findViewById(R.id.PunctualityRB);
        commentET = (EditText) view.findViewById(R.id.commentET);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:
                sendRating();
                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    private void sendRating() {


        int position = Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()));
        String comment = commentET.getText().toString().trim();
        rating = new Rating();
        rating.execute(SessionManager.getUserCountryCode(getActivity()),
                SessionManager.getUserID(getActivity()),
                historyDatas.get(position).drivercode,
                historyDatas.get(position).reference,
                "" + RatingbarProfessionlity.getRating(),
                "" + PunctualityRB.getRating(),
                "" + PriceRB.getRating(),
                comment);
    }


    private class Rating extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_rate_users.php";

                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("rating", paramsa[2]);
                params.put("preference", paramsa[3]);
                params.put("prating_1", paramsa[4]);//Professionalism
                params.put("prating_2", paramsa[5]);//Punctuality
                params.put("prating_3", paramsa[6]);//Price
                params.put("pcomment", paramsa[7]);
                params.put("action", "users_rating");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("Update Profile Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray Response = jsonObject.getJSONArray("Response");

                JSONObject obj = Response.getJSONObject(0);
                if (obj.optString("message").equalsIgnoreCase("Success"))
                    if (obj.optString("returncode").equals("0")) {
                        getActivity().getSupportFragmentManager().popBackStack();
                    } else {
                        Snackbar.make(view,obj.optString("message"),Snackbar.LENGTH_LONG).show();
                    }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);
            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }

}
