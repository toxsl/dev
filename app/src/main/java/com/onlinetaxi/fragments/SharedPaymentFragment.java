package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.SharedCostFriendsAdapter;
import com.onlinetaxi.data.SharePaymentModel;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.material.ButtonFlat;
import com.onlinetaxi.utils.FontManager;

import java.util.ArrayList;

/**
 * Created by TOXSL\jyotpreet.kaur on 28/9/16.
 */
public class SharedPaymentFragment extends BaseFragment {
    private View view;
    private ImageView drawerIconIV;
    RelativeLayout submitRL;
    private TextView cardNumberTV;
    Typeface iconFont, iconFont1;
    ListView listviewTop, listviewBottom;
    TextView fontAddUser, fontRemoveUser;
    private Dialog alertDialogPaymentMethod;
    EditText etEmail;
    SharedCostFriendsAdapter ToplistAdapter;
    LinearLayout parentLL,BottomlistLL,ToplistLL,TitleLL;
    RelativeLayout BottomRL;
    EditText pickupaddressET, destinationET;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sharedpayment, container, false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontCircle), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontSend), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontAddUser), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontRemoveUser), iconFont);

        ArrayList<SharePaymentModel> sharedFriendlist = new ArrayList<>();
        SharePaymentModel paymentModel = new SharePaymentModel();
        paymentModel.setFriendName("Olukunle Agunloye");

        sharedFriendlist.add(paymentModel);

        ToplistAdapter = new SharedCostFriendsAdapter(getActivity(), R.layout.share_cost_frnds_row, sharedFriendlist);
        listviewTop.setAdapter(ToplistAdapter);
        listviewBottom.setAdapter(ToplistAdapter);

        listviewTop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                adapterView.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.white));

                showChangeAmountDailog();


                /*fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);*/
            }
        });

        listviewBottom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                adapterView.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.white));
               /* fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);*/
            }
        });

        listviewTop.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

                adapterView.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.light_grey));
                fontRemoveUser.setVisibility(View.VISIBLE);
                fontAddUser.setVisibility(View.GONE);
                return false;
            }
        });

        listviewBottom.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

                adapterView.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.light_grey));
                fontRemoveUser.setVisibility(View.VISIBLE);
                fontAddUser.setVisibility(View.GONE);
                return false;
            }
        });

        submitRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProceedToNextPageDailog();
            }
        });

    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        submitRL = (RelativeLayout) view.findViewById(R.id.sendbutton);

        listviewTop = (ListView) view.findViewById(R.id.listViewTop);
        listviewBottom = (ListView) view.findViewById(R.id.listViewBottom);
        fontAddUser = (TextView) view.findViewById(R.id.fontAddUser);
        fontAddUser.setOnClickListener(this);
        fontRemoveUser = (TextView) view.findViewById(R.id.fontRemoveUser);
        fontRemoveUser.setOnClickListener(this);
        parentLL = (LinearLayout) view.findViewById(R.id.parentLL);
        parentLL.setOnClickListener(this);
        destinationET = (EditText) view.findViewById(R.id.destinationET);
        destinationET.setOnClickListener(this);
        pickupaddressET = (EditText) view.findViewById(R.id.pickupaddressET);
        pickupaddressET.setOnClickListener(this);
        ToplistLL=(LinearLayout)view.findViewById(R.id.ToplistLL);
        ToplistLL.setOnClickListener(this);
        BottomlistLL=(LinearLayout)view.findViewById(R.id.BottomlistLL);
        BottomlistLL.setOnClickListener(this);
        BottomRL=(RelativeLayout)view.findViewById(R.id.BottomRL);
        BottomRL.setOnClickListener(this);
        TitleLL=(LinearLayout)view.findViewById(R.id.TitleLL);
        TitleLL.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;

            case R.id.fontAddUser:
                showEmailAddDialog();
                break;
            case R.id.fontRemoveUser:

                break;
            case R.id.parentLL:
                fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);
                break;
            case R.id.pickupaddressET:
                fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);
                break;
            case R.id.destinationET:
                fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);
                break;
            case R.id.ToplistLL:
                fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);
                break;
            case R.id.BottomlistLL:
                fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);
                break;
            case R.id.BottomRL:
                fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);
                break;
            case R.id.TitleLL:
                fontAddUser.setVisibility(View.VISIBLE);
                fontRemoveUser.setVisibility(View.GONE);
                break;

        }
    }


    private void paymentProcessDialog() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_payment_shayredpayment, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("Payment");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("PROCEED", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public void showEmailAddDialog() {



        View dialoglayout = View.inflate(baseActivity, R.layout.sendfriend_request, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("PROCEED", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (ApplicationGlob.isConnectingToInternet(getActivity())) {

                    String input = etEmail.getText().toString();
                    input = input.replace(" ", "");
                    if (ApplicationGlob.isEmailValid(input)) {


                    } else {
                        Toast.makeText(getActivity(), "Email Address is not valid", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
                }
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

    }

    private void showProceedToNextPageDailog() {



        View dialoglayout = View.inflate(baseActivity, R.layout.share_cost_proceed, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("PROCEED", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                baseActivity.getSupportFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.frame_container, new SpSharedCostFragment())
                        .commit();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();



    }



    private void showChangeAmountDailog() {



        View dialoglayout = View.inflate(baseActivity, R.layout.change_share_cost, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialog.dismiss();

            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();



    }


}
