package com.onlinetaxi.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;

/**
 * Created by TOXSL/utkarsh.shukla on 10/10/16.
 */
public class FriendsFragment extends BaseFragment {

    private View view;
    TextView changeAmountTV;
    ImageView drawerIconIV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_friends, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        changeAmountTV= (TextView) view.findViewById(R.id.changeAmountTV);
        drawerIconIV= (ImageView) view.findViewById(R.id.drawerIconIV);
        changeAmountTV.setOnClickListener(this);
        drawerIconIV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.changeAmountTV:
                changeAmountDialog();
                break;
        }
    }

    private void changeAmountDialog() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_change_amount, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity,R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("Lanre Akindayomi");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showToast("Changed Successfully");
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }
}
