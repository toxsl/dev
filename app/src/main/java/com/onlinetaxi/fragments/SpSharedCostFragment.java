package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;

import com.onlinetaxi.adapter.CardAdapter;
import com.onlinetaxi.adapter.SpSharedAmountAdapter;
import com.onlinetaxi.data.CardsModel;
import com.onlinetaxi.data.SharePaymentModel;
import com.onlinetaxi.helper.ApplicationGlob;


import com.onlinetaxi.material.ButtonFlat;
import com.onlinetaxi.ormdata.AppDataBaseOrm;
import com.onlinetaxi.utils.FontManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 10/24/2016.
 */

public class SpSharedCostFragment extends BaseFragment {

    private View view;
    Typeface iconFont1;
    SpSharedAmountAdapter adapter;
    ListView listView,listviewCARD;
//    LinearLayout AddCardLL;
    private ImageView drawerIconIV;
    private Button submitFAB;
    private Dialog alertDialogPaymentMethod;
    List<AppDataBaseOrm> cardlist = new ArrayList<>();
    CardAdapter cardAdapter;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.shared_cost, container, false);
        initUI();
        return view;


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontDelete), iconFont1);

        ArrayList<SharePaymentModel> sharedFriendlist = new ArrayList<>();
        SharePaymentModel paymentModel = new SharePaymentModel();
        paymentModel.setFriendName("Olukunle Agunloye");
        paymentModel.setLastseen("2 Mins ago");
        paymentModel.setSharedAmount("10.00");
        sharedFriendlist.add(paymentModel);

        adapter = new SpSharedAmountAdapter(getActivity(), R.layout.share_amout_frnd_row, sharedFriendlist);
        listView.setAdapter(adapter);


       /* AddCardLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ApplicationGlob.isConnectingToInternet(getActivity())) {

                    baseActivity.getSupportFragmentManager()
                            .beginTransaction().addToBackStack(null)
                            .replace(R.id.frame_container, new AddcardFragment())
                            .commit();


                } else {

                    ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
                }

            }
        });*/


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                PaymentconfirmationDialog();

            }
        });


}

    public void initUI(){

        listView=(ListView)view.findViewById(R.id.listOfSharedCost);
//        AddCardLL = (LinearLayout) view.findViewById(R.id.AddCardLL);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        submitFAB = (Button) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
        drawerIconIV.setOnClickListener(this);


        listviewCARD = (ListView) view.findViewById(R.id.listviewCARD);



        try {
            cardlist = AppDataBaseOrm.getAllList(getActivity());

        } catch (Exception e) {
            // TODO: handle exception
        }
        cardAdapter = new CardAdapter(getActivity(), R.layout.custon_card_row, cardlist);
        listviewCARD.setAdapter(cardAdapter);


       /* listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                selectCardandPay();
            }
        });*/
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;

            case R.id.submitFAB:
                ShowProceedPopup();
                break;
        }


    }

    private void PaymentconfirmationDialog() {


        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_confirm_payment, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


    public void ShowProceedPopup(){


        View dialoglayout = View.inflate(baseActivity, R.layout.share_cost_detail, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("PROCEED", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();



    }


}
