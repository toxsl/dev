package com.onlinetaxi.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.data.HistoryMenuListData;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.video.AudioCallScreenActivity;
import com.onlinetaxi.video.SinchService;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.calling.Call;

import java.util.ArrayList;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class ChatFragment extends BaseFragment {

    private TextView forgotPasswordTV, userLocationTV, callTVF;
    private ImageView drawerIconIV;
    ArrayList<HistoryData> historyDatas = new ArrayList<>();
    private int position;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        userLocationTV = (TextView) view.findViewById(R.id.userLocationTV);
        callTVF = (TextView) view.findViewById(R.id.callTVF);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        callTVF.setTypeface(baseActivity.iconFont);
        callTVF.setOnClickListener(this);
        drawerIconIV.setOnClickListener(this);
        historyDatas=SessionManager.getHistoryData(getActivity());
        position = Integer.parseInt(SessionManager.getSelectedHistoryPosition(getActivity()));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.callTVF:
                callButtonClicked();
                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    private void callButtonClicked() {
        String userName = historyDatas.get(position).drivercode;
    //    String userName ="193";
        baseActivity.log("user to be called>>>>  "+userName);
        if (userName.isEmpty()) {
            baseActivity.showToast("Please enter a user to call");
            return;
        }

        try {
            Call call = baseActivity.getSinchServiceInterface().callUser(userName);


            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                baseActivity.showToast("Service is not started. Try stopping the service and starting it again before " + "placing a call.");
                return;
            }
            String callId = call.getCallId();
            Intent callScreen = new Intent(getActivity(), AudioCallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            startActivity(callScreen);


        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{e.getRequiredPermission()}, 0);
        }

    }
}
