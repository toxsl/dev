package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.SetLocationFragmentsAdapter;
import com.onlinetaxi.utils.SlidingTabLayout;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class SetLocationFragment extends BaseFragment {

    private ViewPager pager;
    private CharSequence titles[] = new CharSequence[2];
    private SetLocationFragmentsAdapter adapter;
    private int noOfTabs = 2;
    private SlidingTabLayout tabs;
    private TextView pickupTV;
    private EditText pickupET;
    int pager_position;
    private ImageView drawerIconIV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set_location, container, false);
        pager_position = getArguments().getInt("pager_position");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view) {
        pickupTV = (TextView) view.findViewById(R.id.pickupTV);
        pickupET = (EditText) view.findViewById(R.id.pickupET);
        pager = (ViewPager) view.findViewById(R.id.pager);
        titles[0] = getString(R.string.pickup_location);
        titles[1] = getString(R.string.destination);
        adapter = new SetLocationFragmentsAdapter(getChildFragmentManager(), titles,
                noOfTabs);
        tabs = (SlidingTabLayout) view.findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorPrimary);
            }

            @Override
            public int getDividerColor(int position) {
                return getResources().getColor(R.color.transparent);
            }
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setHeadingText(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
        pager.setCurrentItem(pager_position);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    private void setHeadingText(int position) {
        switch (position) {
            case 0:
                pickupET.setHint("Enter Pickup Location");
                pickupTV.setText(baseActivity.getString(R.string.pickup_location));
                break;
            case 1:
                pickupET.setHint("Enter Your Destination");
                pickupTV.setText(baseActivity.getString(R.string.destination));
                break;
        }
    }


}
