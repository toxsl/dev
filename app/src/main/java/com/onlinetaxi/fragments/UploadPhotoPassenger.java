package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.helper.SessionManager;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;


/**
 * Created by Android on 11/9/2016.
 */

public class UploadPhotoPassenger extends BaseActivity {

    long totalSize = 0;
    Bitmap cropped;
    private CropImageView mCropImageView;
    private ProgressBar progressBar;
    private String filePath = null;
    private TextView txtPercentage;
    //private VideoView vidPreview;
    private Button btnUpload;
    private Context mContext;
    private Dialog alertDialogProgressBar;
    String uploadImageResponse;
    private File croppedFile;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);


        setContentView(R.layout.activity_upload);
        this.mContext = this;
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);
        btnUpload = (Button) findViewById(R.id.btnUpload);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mCropImageView = (CropImageView) findViewById(R.id.CropImageView);
        // vidPreview = (VideoView) findViewById(R.id.videoPreview);

        // Receiving the data from previous activity
        Intent i = getIntent();

        // image or video path that is captured in previous activity
        filePath = i.getStringExtra("filePath");

        // boolean flag to identify the media type, image or video
        boolean isImage = i.getBooleanExtra("isImage", true);

        if (filePath != null) {
            // Displaying the image or video on the screen
            previewMedia(isImage);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
        }

        btnUpload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // uploading the file to server
                cropped = mCropImageView.getCroppedImage(200, 200);
                if (cropped != null) {
                    mCropImageView.setImageBitmap(cropped);
                }
                new UploadFileToServer().execute();
            }
        });

    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadImage();
        }


        @Override
        protected void onPostExecute(String result) {
            Log.e("yoyo", "Response from server: " + result);

            progressbar(false);

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");

                if (returncode.equals("0")) {


                 /*   String imgurl = jsoObject1.getString("imageurl");

                    SessionManager.setUserProfilePhoto(getApplicationContext(), imgurl);
                    //   Toast.makeText(UpdateProfileOfPassenger.this, "Profile pic updated successfully", Toast.LENGTH_SHORT).show();

                    //finish();
                    bitmap = BitmapFactory.decodeFile(croppedFile.getAbsolutePath());


                    guestuser.setImageBitmap(bitmap);
                    guestuser.setScaleType(ImageView.ScaleType.FIT_XY);
                    userimg_change.setImageBitmap(bitmap);
                    userimg_change.setScaleType(ImageView.ScaleType.FIT_XY);

                    Picasso.with(getApplicationContext())
                            .load(SessionManager.getUserProfilePhoto(getApplicationContext()))
                            .placeholder(R.drawable.nouserimage)
                            .error(R.drawable.nouserimage)
                            .into(userimg_change);

                    Picasso.with(getApplicationContext())
                            .load(SessionManager.getUserProfilePhoto(getApplicationContext()))
                            .placeholder(R.drawable.nouserimage)
                            .error(R.drawable.nouserimage)
                            .into(guestuser);


                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            Snackbar snackbar = Snackbar
                                    .make(parentLayout, "Problem occured in Uploading.", Snackbar.LENGTH_LONG);
                            snackbar.show();


                            //  Toast.makeText(UpdateProfileOfPassenger.this, "Problem occured in uploading", Toast.LENGTH_SHORT).show();
                        }
                    });*/
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // showing the server response in an alert dialog
            //showAlert(result);

            //SessionManager.getUserProfilePhoto(getApplicationContext())
            super.onPostExecute(result);
        }
    }


    private String uploadImage(){
        //Showing the progress dialog


        final ProgressDialog loading = ProgressDialog.show(UploadPhotoPassenger.this,"Uploading...","Please wait...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.FILE_UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        uploadImageResponse=s;
                        //Showing toast message of the response
                        Toast.makeText(UploadPhotoPassenger.this, s , Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        uploadImageResponse=volleyError.getMessage().toString();

                        //Showing toast
                        Toast.makeText(UploadPhotoPassenger.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                // String image = getStringImage(bitmap);

                File sourceFile = new File(croppedFile.getAbsolutePath());

                //Getting Image Name
                //  String name = editTextName.getText().toString().trim();

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("pisocode", SessionManager.getUserCountryCode(UploadPhotoPassenger.this));
                params.put("puserid", SessionManager.getUserID(UploadPhotoPassenger.this));
                params.put("pdocumentype", "PROFILE");
                params.put("pname", SessionManager.getUserFirstName(UploadPhotoPassenger.this));
                params.put("image", sourceFile.toString());
                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(UploadPhotoPassenger.this);

        //Adding request to the queue
        requestQueue.add(stringRequest);

        return uploadImageResponse;
    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(UploadPhotoPassenger.this,
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }


    private void previewMedia(boolean isImage) {
        // Checking whether captured media is image or video
        if (isImage) {
            mCropImageView.setVisibility(View.VISIBLE);
            //vidPreview.setVisibility(View.GONE);
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // down sizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

            mCropImageView.setImageBitmap(bitmap);
        } else {
            mCropImageView.setVisibility(View.GONE);

        }
    }

}
