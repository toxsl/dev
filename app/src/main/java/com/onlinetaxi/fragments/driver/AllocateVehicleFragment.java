package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.AllocateVehicleAdapter;
import com.onlinetaxi.data.DriverVehicleModel;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by Android on 11/16/2016.
 */

public class AllocateVehicleFragment extends BaseFragment {
    private ImageView drawerIconIV;
    private RecyclerView vehicleRV;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.allocate_vehicle,container,false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        vehicleRV.setLayoutManager(mLayoutManager);
        vehicleRV.setItemAnimator(new DefaultItemAnimator());
        vehicleRV.setHasFixedSize(true);


        ArrayList<String> list=new ArrayList<>();
        vehicleRV.setAdapter(new AllocateVehicleAdapter(baseActivity,list));


    }

    private void initUI() {
        vehicleRV= (RecyclerView) view.findViewById(R.id.manageLV);
        drawerIconIV= (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
        }
    }

}
