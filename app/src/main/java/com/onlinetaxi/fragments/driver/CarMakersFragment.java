package com.onlinetaxi.fragments.driver;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.CarMakersAdapter;
import com.onlinetaxi.data.CarMakerModel;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.MaterialSearchViewCarMakersActivity;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by TOXSL/utkarsh.shukla on 7/11/16.
 */
public class CarMakersFragment extends BaseFragment {
    private View view;
    private ImageView drawerIconIV;
    public static TextView searchTV, profileTV;
    private ListView vichleListLV;
    Dialog alertDialogProgressBar;
    private CarMakersTask task;
    CoordinatorLayout parent;
    MaterialSearchViewCarMakersActivity searchView;
    public static LinearLayout layoutSearch, toplayout;
    private static final int MAX_RESULTS = 1;
    public static final int REQUEST_VOICE = 42;
    CarMakersAdapter adapter;
    public static String CarMaker;
    ArrayList<CarMakerModel> list;
    public static LinearLayout MainLayout;
    private static CarMakersFragment mInstance = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_vehicle_maker, container, false);
        initUI();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (SessionManager.getCarMakerDataSP(getActivity()).equals("CarMakerDataSP")) {
            if (ApplicationGlob.isConnectingToInternet(getActivity())) {

                task = new CarMakersTask();
                task.execute();

            } else {

                ApplicationGlob.ToastShowInterNetConnection(getActivity(), getActivity());
            }

        } else {
            setDataFromSP();

        }


        vichleListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    CarMaker = list.get(i).getCarMaker();

                    getFragmentManager().beginTransaction().replace(R.id.frame_container, new CarModelsFragment()).addToBackStack(null).commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


        MaterialSearchViewCarMakersActivity.mVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onVoiceClicked();
            }
        });

        MaterialSearchViewCarMakersActivity.mSearchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
                // When user changed the Text
                CarMakersFragment.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });

        MaterialSearchViewCarMakersActivity.mSearchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    showKeyboard(MaterialSearchViewCarMakersActivity.mSearchEditText);

                }
            }
        });

        MainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new NewVehicleFragment()).addToBackStack(null).commit();
            }
        });


    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        searchTV = (TextView) view.findViewById(R.id.searchTV);
        parent = (CoordinatorLayout) view.findViewById(R.id.parent);
        searchTV.setTypeface(baseActivity.iconFont1);
        searchTV.setOnClickListener(this);
        vichleListLV = (ListView) view.findViewById(R.id.vichleListLV);
        toplayout = (LinearLayout) view.findViewById(R.id.headerLL);
        profileTV = (TextView) view.findViewById(R.id.profileTV);
        layoutSearch = (LinearLayout) view.findViewById(R.id.laySearch);
        searchView = (MaterialSearchViewCarMakersActivity) view.findViewById(R.id.search_view);
        MainLayout = (LinearLayout) view.findViewById(R.id.MainLayout);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.searchTV:

                layoutSearch.setVisibility(View.VISIBLE);
                searchTV.setVisibility(View.GONE);
                toplayout.setVisibility(View.GONE);
                profileTV.setVisibility(View.GONE);
                searchView.openSearch();

                break;
        }
    }

    /*public static CarMakersFragment getInstance() {
        if (mInstance == null) {
            mInstance = new CarMakersFragment();
        }
        return mInstance;
    }*/



    private class CarMakersTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_car_makers.php";

                params.put("submit", "car_makers_proc");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("OT Login Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            list = new ArrayList<CarMakerModel>();

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                SessionManager.setCarMakerDataSP(getActivity(), result);
                JSONObject jsonObject = new JSONObject(result);
                JSONArray array = jsonObject.getJSONArray("Response");
                JSONObject jsonObject2 = array.getJSONObject(0);
                String returncode = jsonObject2.getString("returncode");

                if (returncode.equals("0")) {

                    JSONArray userProducts = jsonObject2
                            .getJSONArray("message");

                    for (int i = 0; i < userProducts.length(); i++) {

                        JSONObject singleproduct = (JSONObject) userProducts
                                .get(i);

                        CarMakerModel model = new CarMakerModel();

                        model.setCarMaker(singleproduct.getString("carmake"));

                        list.add(i, model);

                    }


                    adapter = new CarMakersAdapter(list, getActivity(), getActivity());
                    vichleListLV.setAdapter(adapter);


                } else {
                    String excptn = jsonObject2.getString("message");
                    SnackBarOnUiThread(excptn);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void SnackBarOnUiThread(final String message) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    Snackbar snackbar = Snackbar
                            .make(parent, message, Snackbar.LENGTH_LONG);
                    snackbar.show();


                }
            });
        }

    }


    public void setDataFromSP() {

        ArrayList<CarMakerModel> listlocal = new ArrayList<CarMakerModel>();

        try {
            JSONObject jsonObject = new JSONObject(SessionManager.getCarMakerDataSP(getActivity()));
            JSONArray array = jsonObject.getJSONArray("Response");
            JSONObject jsonObject2 = array.getJSONObject(0);
            String returncode = jsonObject2.getString("returncode");

            if (returncode.equals("0")) {

                JSONArray userProducts = jsonObject2
                        .getJSONArray("message");

                for (int i = 0; i < userProducts.length(); i++) {

                    JSONObject singleproduct = (JSONObject) userProducts
                            .get(i);

                    CarMakerModel model = new CarMakerModel();

                    model.setCarMaker(singleproduct.getString("carmake"));

                    listlocal.add(i, model);

                }


                adapter = new CarMakersAdapter(listlocal, getActivity(), getActivity());
                vichleListLV.setAdapter(adapter);


                if (ApplicationGlob.isConnectingToInternet(getActivity())) {

                    task = new CarMakersTask();
                    task.execute();

                } else {

                    ApplicationGlob.ToastShowInterNetConnection(getActivity(), getActivity());
                }


            } else {
                String excptn = jsonObject2.getString("message");
                SnackBarOnUiThread(excptn);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void SnackBarOnUiThread(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {

                Snackbar snackbar = Snackbar
                        .make(parent, message, Snackbar.LENGTH_LONG);
                snackbar.show();


            }
        });
    }


    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }


    private void onVoiceClicked() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getActivity().getApplicationContext().getString(br.com.mauker.materialsearchview.R.string.hint_prompt));
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_RESULTS); // Quantity of results we want to receive

        startActivityForResult(intent, REQUEST_VOICE);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK) {
                ArrayList<String> textMatchList = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                if (!textMatchList.isEmpty()) {
                    String Query = textMatchList.get(0);
                    MaterialSearchViewCarMakersActivity.mSearchEditText.setText(Query);
                }
                //Result code for various error.
            } else if (resultCode == RecognizerIntent.RESULT_NETWORK_ERROR) {
                //  Common.showToast(this, "Network Error");
            } else if (resultCode == RecognizerIntent.RESULT_NO_MATCH) {
                //  Common.showToast(this, "No Match");
            } else if (resultCode == RecognizerIntent.RESULT_SERVER_ERROR) {
                //  Common.showToast(this, "Server Error");
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showKeyboard(View view) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1 && view.hasFocus()) {
            view.clearFocus();
        }

        view.requestFocus();

        if (!isHardKeyboardAvailable()) {
            InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, 0);

        }
    }

    private boolean isHardKeyboardAvailable() {
        return getActivity().getApplicationContext().getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }


}
