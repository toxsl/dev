package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.adapter.CompanyHistoryDetailAdapter;
import com.onlinetaxi.data.CompanyData;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 15/11/16.
 */
public class CompanyHistoryDetailsFragment extends BaseFragment {
    private View view;
    private ListView viewAllLV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_company_history_details,container,false);

        viewAllLV= (ListView) view.findViewById(R.id.viewAllLV);
        ArrayList<CompanyData> companyDatas = new ArrayList<>();
        viewAllLV.setAdapter(new CompanyHistoryDetailAdapter(baseActivity, 0, companyDatas));
        viewAllLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Fragment fragment = new CompanyHistoryDetailsFinalFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();

            }
        });


        return view;
    }
}
