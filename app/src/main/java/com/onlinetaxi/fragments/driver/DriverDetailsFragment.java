package com.onlinetaxi.fragments.driver;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.AddcardFragment;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.utils.FontManager;


/**
 * Created by Android on 11/18/2016.
 */

public class DriverDetailsFragment extends BaseFragment {

    View view;
    private ImageView drawerIconIV;
    private RelativeLayout GoTaxiRL;
    Typeface iconFont, iconFont1;
    private TextView infiTVF;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.driver_detail_fragment, container, false);
            init();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

      /*  iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontEmail), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontTaxi), iconFont);


        FontManager.markAsIconContainer(view.findViewById(R.id.fontEmail), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontTaxi), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.Backgroud1), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.Backgroud2), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.Backgroud3), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.person), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.clock), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.CC), iconFont);*/


    }

    private void init() {

        iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontEmail), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontTaxi), iconFont);


        FontManager.markAsIconContainer(view.findViewById(R.id.fontEmail), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontTaxi), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.Backgroud1), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.Backgroud2), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.Backgroud3), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.person), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.clock), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.CC), iconFont);

        GoTaxiRL = (RelativeLayout) view.findViewById(R.id.GoTaxiRL);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        GoTaxiRL.setOnClickListener(this);


        infiTVF = (TextView) view.findViewById(R.id.infiTVF);
        infiTVF.setTypeface(baseActivity.iconFont1);
        infiTVF.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.GoTaxiRL:

                break;
            case R.id.infiTVF:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new InformationCenterFragment()).addToBackStack(null).commit();

                break;


        }
    }

}
