package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onlinetaxi.R;
import com.onlinetaxi.fragments.BaseFragment;

/**
 * Created by TOXSL/utkarsh.shukla on 15/11/16.
 */
public class CompanyHistoryDetailsFinalFragment extends BaseFragment {

    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_company_history_final,container,false);
        return view;
    }
}
