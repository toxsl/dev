package com.onlinetaxi.fragments.driver;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.ManageDriverAdapter;
import com.onlinetaxi.adapter.ManageVehiclePagerAdapter;
import com.onlinetaxi.data.ManageDriverDataModel;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.utils.FontManager;
import com.sembozdemir.viewpagerarrowindicator.library.ViewPagerArrowIndicator;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 8/11/16.
 */
public class ManageVehicleFragment extends BaseFragment {

    private View view;

    private ArrayList<ManageDriverDataModel> manageDriverDatas = new ArrayList<>();
    private ImageView drawerIconIV;
    Typeface iconFont;
    ViewPagerArrowIndicator viewPagerArrowIndicator;
    ViewPager viewPager;
    ManageVehiclePagerAdapter pagerAdapter;
    LinearLayout ShowFeatureLL;

    RelativeLayout ManageRL;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_manage_vehicle, container, false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontEmail), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontTaxi), iconFont);


        ArrayList<String> list = new ArrayList<>();
        list.add("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQYDQ9yfQvG-cxd_Zb8sJ3b2MmaxtSAHnG_okE6OWYqKqVxHxf7Kw");
        list.add("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRwq_Xbp5hxjMwqKgBYOaL48AplRfjICD09htshWEf5Uo-FmMx6hA");
        list.add("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTANeNCGesYB5wR37GegMoIKMTMsfAU0kmvPHCr1i8zKrLAaJ-l");
        list.add("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQ9PDXqY2tTDF6Ei-4mk0SoUEZ4eB5N5y9oakrbw2tItH_Ra8j89Q");
        list.add("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcShXC5TVU68m2bdOot10dAOMpvnCSTFYm1Bf7-3pXHQjJGahRfM");


        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        viewPagerArrowIndicator = (ViewPagerArrowIndicator) view.findViewById(R.id.viewPagerArrowIndicator);
        pagerAdapter = new ManageVehiclePagerAdapter(list, getActivity());
        viewPager.setAdapter(pagerAdapter);


        viewPagerArrowIndicator.bind(viewPager);


        viewPagerArrowIndicator.setArrowIndicatorRes(R.drawable.arrow_lf,
                R.drawable.arrow_rg);

    }

    private void initUI() {

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        ShowFeatureLL = (LinearLayout) view.findViewById(R.id.ShowFeatureLL);
        ShowFeatureLL.setOnClickListener(this);
        ManageRL = (RelativeLayout) view.findViewById(R.id.ManageRL);
        ManageRL.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
            case R.id.ManageRL:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new ManageVehicleMenuFragment()).addToBackStack(null).commit();
                break;

            case R.id.ShowFeatureLL:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new VehicleFeatureFragment()).addToBackStack(null).commit();

                break;
        }
    }
}
