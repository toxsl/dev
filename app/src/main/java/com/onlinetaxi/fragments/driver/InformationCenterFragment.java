package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;

/**
 * Created by TOXSL/utkarsh.shukla on 23/11/16.
 */
public class InformationCenterFragment extends BaseFragment{
    View view;
    private RelativeLayout completedRL;
    private ImageView drawerIconIV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_information_center_driver,container,false);
        initUI();
        return view;
    }

    private void initUI() {

        completedRL= (RelativeLayout) view.findViewById(R.id.completedRL);
        completedRL.setOnClickListener(this);

        drawerIconIV= (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.completedRL:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new CompanyFragment()).addToBackStack(null).commit();
                break;
        }
    }
}
