package com.onlinetaxi.fragments.driver;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.DriverRatingListAdapter;
import com.onlinetaxi.data.CompanyData;
import com.onlinetaxi.data.DriverRatingData;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.fragments.UpdateProfileFragment;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TOXSL/utkarsh.shukla on 16/11/16.
 */
public class DriverProfileFragment extends BaseFragment {

    View view;
    private TextView editIV, contactTVF, emailTVF, nameTVF;
    private ImageView drawerIconIV;
    private ListView ratingLV;
    private TextView contactTV, emailTV, nameTV, dateTV, amountTV;
    DriverRatingListAdapter driverRatingListAdapter;
    Dialog alertDialogProgressBar;
    private ProfileDetails checkTask;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_drivers_profile, container, false);
        initUI();

        return view;
    }

    private void initUI() {

        ArrayList<DriverRatingData> driverRatingDatas = new ArrayList<>();

        nameTVF = (TextView) view.findViewById(R.id.nameTVF);
        emailTVF = (TextView) view.findViewById(R.id.emailTVF);
        contactTVF = (TextView) view.findViewById(R.id.contactTVF);
        editIV = (TextView) view.findViewById(R.id.editIV);
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        ratingLV = (ListView) view.findViewById(R.id.ratingLV);

        nameTV = (TextView) view.findViewById(R.id.nameTV);
        emailTV = (TextView) view.findViewById(R.id.emailTV);
        contactTV = (TextView) view.findViewById(R.id.contactTV);
        amountTV = (TextView) view.findViewById(R.id.amountTV);
        dateTV = (TextView) view.findViewById(R.id.dateTV);

        ratingLV.setAdapter(new DriverRatingListAdapter(baseActivity, 0, driverRatingDatas));

        editIV.setTypeface(baseActivity.iconFont);
        emailTVF.setTypeface(baseActivity.iconFont);
        contactTVF.setTypeface(baseActivity.iconFont);
        nameTVF.setTypeface(baseActivity.iconFont);


        editIV.setOnClickListener(this);
        drawerIconIV.setOnClickListener(this);

        setData();

        ratingLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new DriverRatingListFragment()).addToBackStack(null).commit();

            }
        });

    }

    private void setData() {
        nameTV.setText(SessionManager.getUserFirstName(getActivity()) + " " + SessionManager.getUserLastName(getActivity()));
        contactTV.setText(SessionManager.getUserContactNumber(getActivity()));
        emailTV.setText(SessionManager.getUserEmail(getActivity()));
        amountTV.setText(SessionManager.getMillageAmount(getActivity()));
        dateTV.setText(SessionManager.getpaydate(getActivity()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.editIV:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new UpdateProfileFragment()).addToBackStack(null).commit();

                break;


        }
    }

    private class ProfileDetails extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_drivers_profile.php";
                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("submit", "ot_drivers_profile_proc");

                baseActivity.log(requestURL + "  " + params);
                try {
                    HttpUtility.sendPostRequest(requestURL, params);
                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();


                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

            // ToastOnUiThread(result);

            Log.i("result=", " " + result);
            try {
                if (result != null && !result.isEmpty() && result != " ") {
                    baseActivity.log(result);
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray Response = jsonObject.getJSONArray("Response");

                    JSONObject object = Response.getJSONObject(0);
                    JSONObject company_data = object.getJSONObject("company_data");
                    JSONObject priviledges = company_data.getJSONObject("priviledges");
                    JSONObject driver_data = company_data.getJSONObject("driver_data");

                    String Amount = driver_data.optString("amount");
                    String paydate = driver_data.optString("paydate");

                    String firstname = driver_data.optString("firstname");
                    String lastname = driver_data.optString("lastname");

                    SessionManager.setMillageAmount(getActivity(), Amount);
                    SessionManager.setpaydate(getActivity(),paydate);
                    SessionManager.setUserFirstName(getActivity(), firstname);
                    SessionManager.setUserLastName(getActivity(), lastname);
                    baseActivity.store.saveString("taxi_admin", priviledges.optString("taxi_admin"));
                    baseActivity.store.saveString("taxi_opera", priviledges.optString("taxi_opera"));


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }

}
