package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.adapter.ManageAssociationListAdapter;
import com.onlinetaxi.data.AssociationData;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 22/11/16.
 */
public class ManageAssociationAdminFragment extends BaseFragment{
    View view;
    private TextView rightTFA,leftTFA;
    private ListView associansLV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_manage_association_admin,container,false);

        initUI();

        return view;
    }

    private void initUI() {
        leftTFA= (TextView) view.findViewById(R.id.leftTFA);
        leftTFA.setTypeface(baseActivity.iconFont1);
        rightTFA= (TextView) view.findViewById(R.id.rightTFA);
        rightTFA.setTypeface(baseActivity.iconFont1);

        ArrayList<AssociationData> associationDatas = new ArrayList<>();


        associansLV= (ListView) view.findViewById(R.id.associansLV);
        associansLV.setAdapter(new ManageAssociationListAdapter(baseActivity,0,associationDatas));

    }
}
