package com.onlinetaxi.fragments.driver;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.CarMakersAdapter;
import com.onlinetaxi.adapter.CarModelAdapter;
import com.onlinetaxi.data.CarMakerModel;
import com.onlinetaxi.data.CarModelData;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.MaterialSearchViewCarMakersActivity;
import com.onlinetaxi.helper.MaterialSearchViewCarModelActivity;
import com.onlinetaxi.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by TOXSL/utkarsh.shukla on 7/11/16.
 */
public class CarModelsFragment extends BaseFragment {
    private View view;
    private ImageView drawerIconIV;
    public static TextView searchTV,profileTV;
    private ListView vichleListLV;
    private String[] arrayList;
    private int requestCode;
    private Dialog action_bar_dialog;
    private AutoCompleteTextView seachLocationATV;
    private String selectedAddress;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    Dialog alertDialogProgressBar;
    private CarModelsTask task;
    CoordinatorLayout parent;
    MaterialSearchViewCarModelActivity searchView;
    public static LinearLayout layoutSearch,toplayout;
    private static final int MAX_RESULTS = 1;
    public static final int REQUEST_VOICE = 42;
    CarModelAdapter adapter;
    ArrayList<CarModelData> list;
    public static String CarModel;
    public static LinearLayout MainLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_vehile_model,container,false);
        initUI();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (SessionManager.getCarModelDataSP(getActivity()).equals("CarModelDataSP")) {
            if (ApplicationGlob.isConnectingToInternet(getActivity())) {

                task=new CarModelsTask();
                task.execute(CarMakersFragment.CarMaker);

            }else{

                ApplicationGlob.ToastShowInterNetConnection(getActivity(),getActivity());
            }

        }else{
            setDataFromSP();

        }


        vichleListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                try {
                    CarModel=list.get(i).getCarModel();
                    getFragmentManager().beginTransaction().replace(R.id.frame_container, new CarYearFragment()).addToBackStack(null).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



        MaterialSearchViewCarModelActivity.mVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onVoiceClicked();
            }
        });

        MaterialSearchViewCarModelActivity.mSearchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
                // When user changed the Text
                CarModelsFragment.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub

            }
        });

        MaterialSearchViewCarModelActivity.mSearchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    showKeyboard(MaterialSearchViewCarModelActivity.mSearchEditText);

                }
            }
        });

        MainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new NewVehicleFragment()).addToBackStack(null).commit();
            }
        });


    }



    private void initUI() {
        drawerIconIV=(ImageView)view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        searchTV= (TextView) view.findViewById(R.id.searchTV);
        parent=(CoordinatorLayout)view.findViewById(R.id.parent);
        searchTV.setTypeface(baseActivity.iconFont1);
        searchTV.setOnClickListener(this);
        vichleListLV= (ListView) view.findViewById(R.id.vichleListLV);
        toplayout=(LinearLayout)view.findViewById(R.id.headerLL);
        profileTV=(TextView)view.findViewById(R.id.profileTV);
        layoutSearch=(LinearLayout)view.findViewById(R.id.laySearch);
        searchView = (MaterialSearchViewCarModelActivity)view.findViewById(R.id.search_view);
        MainLayout = (LinearLayout) view.findViewById(R.id.MainLayout);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.searchTV:

                layoutSearch.setVisibility(View.VISIBLE);
                searchTV.setVisibility(View.GONE);
                toplayout.setVisibility(View.GONE);
                profileTV.setVisibility(View.GONE);
                searchView.openSearch();

                break;
        }
    }


    private class CarModelsTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressbar(true);
        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_car_models.php";

                params.put("pmodel", paramsa[0]);
                params.put("submit", "car_makers_proc");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL+"  "+params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("OT Login Response",""+response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            progressbar(false);

             list=new ArrayList<CarModelData>();

            // ToastOnUiThread(result);
            baseActivity.log(result);
            try {
                SessionManager.setCarModelDataSP(getActivity(),result);
                JSONObject jsonObject = new JSONObject(result);
                JSONArray array = jsonObject.getJSONArray("Response");
                JSONObject jsonObject2 = array.getJSONObject(0);
                String returncode = jsonObject2.getString("returncode");

                if (returncode.equals("0")) {

                    JSONArray userProducts = jsonObject2
                            .getJSONArray("message");

                    for (int i = 0; i < userProducts.length(); i++) {

                        JSONObject singleproduct = (JSONObject) userProducts
                                .get(i);

                        CarModelData model=new CarModelData();

                        model.setCarModel(singleproduct.getString("carmodel"));

                        list.add(i, model);

                    }


                    adapter=new CarModelAdapter(list,getActivity(),getActivity());
                    vichleListLV.setAdapter(adapter);



                } else {
                    String excptn = jsonObject2.getString("message");
                    SnackBarOnUiThread(excptn);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void SnackBarOnUiThread(final String message) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    Snackbar snackbar = Snackbar
                            .make(parent, message, Snackbar.LENGTH_LONG);
                    snackbar.show();


                }
            });
        }

    }



    public void   setDataFromSP(){

        ArrayList<CarModelData> listlocal=new ArrayList<CarModelData>();

        try {
            JSONObject jsonObject = new JSONObject(SessionManager.getCarModelDataSP(getActivity()));
            JSONArray array = jsonObject.getJSONArray("Response");
            JSONObject jsonObject2 = array.getJSONObject(0);
            String returncode = jsonObject2.getString("returncode");

            if (returncode.equals("0")) {

                JSONArray userProducts = jsonObject2
                        .getJSONArray("message");

                for (int i = 0; i < userProducts.length(); i++) {

                    JSONObject singleproduct = (JSONObject) userProducts
                            .get(i);

                    CarModelData model=new CarModelData();

                    model.setCarModel(singleproduct.getString("carmodel"));

                    listlocal.add(i, model);

                }


                adapter=new CarModelAdapter(listlocal,getActivity(),getActivity());
                vichleListLV.setAdapter(adapter);



                if (ApplicationGlob.isConnectingToInternet(getActivity())) {

                    task=new CarModelsTask();
                    task.execute(CarMakersFragment.CarMaker);

                }else{

                    ApplicationGlob.ToastShowInterNetConnection(getActivity(),getActivity());
                }



            } else {
                String excptn = jsonObject2.getString("message");
                SnackBarOnUiThread(excptn);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    private void SnackBarOnUiThread(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {

                Snackbar snackbar = Snackbar
                        .make(parent, message, Snackbar.LENGTH_LONG);
                snackbar.show();


            }
        });
    }



    public void progressbar(boolean b) {

        if (b == true) {

            // NOTE : You can set the current value of BusyIndicator
            alertDialogProgressBar = new Dialog(getActivity(),
                    R.style.YourCustomStyle);

            alertDialogProgressBar
                    .requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogProgressBar.setContentView(R.layout.layout_progressbar);

            alertDialogProgressBar.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));

            alertDialogProgressBar.show();

        } else {

            alertDialogProgressBar.dismiss();

        }

    }



    private void onVoiceClicked() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getActivity().getApplicationContext().getString(br.com.mauker.materialsearchview.R.string.hint_prompt));
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_RESULTS); // Quantity of results we want to receive

        startActivityForResult(intent, REQUEST_VOICE);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK) {
                ArrayList<String> textMatchList = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                if (!textMatchList.isEmpty()) {
                    String Query = textMatchList.get(0);
                    MaterialSearchViewCarModelActivity.mSearchEditText.setText(Query);
                }
                //Result code for various error.
            } else if (resultCode == RecognizerIntent.RESULT_NETWORK_ERROR) {
                //  Common.showToast(this, "Network Error");
            } else if (resultCode == RecognizerIntent.RESULT_NO_MATCH) {
                //  Common.showToast(this, "No Match");
            } else if (resultCode == RecognizerIntent.RESULT_SERVER_ERROR) {
                //  Common.showToast(this, "Server Error");
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showKeyboard(View view) {
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1 && view.hasFocus()) {
            view.clearFocus();
        }

        view.requestFocus();

        if (!isHardKeyboardAvailable()) {
            InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, 0);

        }
    }
    private boolean isHardKeyboardAvailable() {
        return getActivity().getApplicationContext().getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }


}
