package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;

/**
 * Created by TOXSL/utkarsh.shukla on 7/11/16.
 */
public class TarrifFragment extends BaseFragment {
    private ImageView drawerIconIV;
    private Button submitFAB;


    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tarrif, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        submitFAB= (Button) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.submitFAB:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new RegisterDriverFragment()).addToBackStack(null).commit();

                break;

        }
    }
}
