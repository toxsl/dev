package com.onlinetaxi.fragments.driver;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.ManageDriverMenuAdapter;
import com.onlinetaxi.data.ManageDriverDataModel;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.fragments.ConfirmationFragment;

import java.util.ArrayList;


public class ManageVehicleMenuFragment extends BaseFragment {
    private View view;
    private ListView manageLV;
    private ArrayList<ManageDriverDataModel> menulist = new ArrayList<>();
    private ImageView drawerIconIV;
    ManageDriverMenuAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.manage_vehicle_menu, container, false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        menulist.clear();

        ManageDriverDataModel model = new ManageDriverDataModel();
        model.setMenu("Tariff");
        menulist.add(0, model);

        ManageDriverDataModel model1 = new ManageDriverDataModel();
        model1.setMenu("Allocate");
        menulist.add(1, model1);

        ManageDriverDataModel model2 = new ManageDriverDataModel();
        model2.setMenu("Promote");
        menulist.add(2, model2);

        ManageDriverDataModel model3 = new ManageDriverDataModel();
        model3.setMenu("Picture");
        menulist.add(3, model3);

        ManageDriverDataModel model4 = new ManageDriverDataModel();
        model4.setMenu("Permission");
        menulist.add(4, model4);

        ManageDriverDataModel model5 = new ManageDriverDataModel();
        model5.setMenu("Finance");
        menulist.add(5, model5);

        ManageDriverDataModel model6 = new ManageDriverDataModel();
        model6.setMenu("Remove");
        menulist.add(6, model6);

        adapter = new ManageDriverMenuAdapter(menulist, getActivity(), getActivity());
        manageLV.setAdapter(adapter);


        manageLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ManageDriverDataModel.setManageDriverDataModel(menulist.get(i));


                switch (menulist.get(i).getMenu()) {
                    case "Tariff":
                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new TarrifFragment()).addToBackStack(null).commit();

                        break;
                    case "Allocate":

                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new AllocateVehicleFragment()).addToBackStack(null).commit();
                        break;

                    case "Promote":

                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new PromoteVehicleFragment()).addToBackStack(null).commit();
                        break;
                    case "Picture":

                        //   getFragmentManager().beginTransaction().replace(R.id.frame_container, new ManageVehicleFragment()).addToBackStack(null).commit();
                        break;
                    case "Permission":

                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new VehiclePermissionFragment()).addToBackStack(null).commit();
                        break;
                    case "Remove":

                        removeVehicledialog();

                        break;

                    case "Finance":

                        break;


                    default:

                }


            }
        });


    }

    private void initUI() {
        manageLV = (ListView) view.findViewById(R.id.manageLV);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
        }
    }

    private void removeVehicledialog() {
        View dialoglayout = View.inflate(baseActivity, R.layout.dialog_remove_vehicle, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(baseActivity, R.style.YourDialogStyle);
        builder.setView(dialoglayout);
        builder.setTitle("");

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("ACCEPT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }


}