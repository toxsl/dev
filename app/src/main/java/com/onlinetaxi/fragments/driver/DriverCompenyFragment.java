package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;


/**
 * Created by TOXSL/utkarsh.shukla on 7/11/16.
 */
public class DriverCompenyFragment extends BaseFragment {

    private View view;
    private TextView websiteTVF, contectTVF, emailTVF, compnyAddressTVF, compnyNameTVF;
    private ImageView drawerIconIV;
    private Button submitFAB;
    private EditText compnyNameET,compnyAddressET,emailET,contectET,websiteET;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_company, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        compnyNameTVF = (TextView) view.findViewById(R.id.compnyNameTVF);
        compnyNameTVF.setTypeface(baseActivity.iconFont1);

        compnyAddressTVF = (TextView) view.findViewById(R.id.compnyAddressTVF);
        compnyAddressTVF.setTypeface(baseActivity.iconFont);

        emailTVF = (TextView) view.findViewById(R.id.emailTVF);
        emailTVF.setTypeface(baseActivity.iconFont);

        contectTVF = (TextView) view.findViewById(R.id.contectTVF);
        contectTVF.setTypeface(baseActivity.iconFont);

        websiteTVF = (TextView) view.findViewById(R.id.websiteTVF);
        websiteTVF.setTypeface(baseActivity.iconFont);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

        submitFAB = (Button) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.submitFAB:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new DriverVechileDetailsFragment()).commit();

                break;
        }
    }
}
