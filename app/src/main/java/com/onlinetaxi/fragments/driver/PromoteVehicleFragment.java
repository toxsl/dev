package com.onlinetaxi.fragments.driver;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.utils.FontManager;

/**
 * Created by Android on 11/15/2016.
 */

public class PromoteVehicleFragment extends BaseFragment {

    View view;
    private ImageView drawerIconIV;
Typeface iconFont1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_promote_vehicle, container, false);
        inItUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontCircle), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontSend), iconFont1);


    }

    public void inItUI() {

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
        }
    }
}
