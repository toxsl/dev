package com.onlinetaxi.fragments.driver;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.utils.FontManager;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

/**
 * Created by TOXSL\jyotpreet.kaur on 28/9/16.
 */
public class DriverCreatePromotionFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    private ImageView drawerIconIV;
    private View view;
    RelativeLayout GoTaxiRL;
    LinearLayout layoutDatePicker;
    String date;
    Typeface iconFont1,iconFont;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.create_driver_promotion, container, false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        iconFont1 = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontCircle), iconFont1);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontSend), iconFont1);

        iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontLoyalty), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontDate), iconFont);


        layoutDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                         DriverCreatePromotionFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAccentColor(Color.parseColor("#e69331"));
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });



    }

    @Override
    public void onResume() {
        super.onResume();

        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }


    private void initUI() {

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        GoTaxiRL = (RelativeLayout) view.findViewById(R.id.GoTaxiRL);
        GoTaxiRL.setOnClickListener(this);
        layoutDatePicker=(LinearLayout)view.findViewById(R.id.layoutDatePicker);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
            case R.id.ManageRL:
              //  getFragmentManager().beginTransaction().replace(R.id.frame_container, new ManageVehicleMenuFragment()).addToBackStack(null).commit();
                break;


        }
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date = year + "/" + (++monthOfYear) + "/" + dayOfMonth;
        Toast.makeText(getActivity().getApplicationContext(), "Selected Date " + date, Toast.LENGTH_SHORT).show();
    }
}
