package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.ManageDriverAdapter;
import com.onlinetaxi.adapter.ManageDriverMenuAdapter;
import com.onlinetaxi.data.ManageDriverDataModel;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by Android on 11/14/2016.
 */

public class ManageDriverMenuFragment extends BaseFragment {
    private View view;
    private ListView manageLV;
    private ArrayList<ManageDriverDataModel> menulist = new ArrayList<>();
    private ImageView drawerIconIV;
    ManageDriverMenuAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.manage_driver_menu, container, false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        menulist.clear();

        ManageDriverDataModel model = new ManageDriverDataModel();
        model.setMenu("Associations");
        menulist.add(0, model);

        ManageDriverDataModel model1 = new ManageDriverDataModel();
        model1.setMenu("Company");
        menulist.add(1, model1);

        ManageDriverDataModel model2 = new ManageDriverDataModel();
        model2.setMenu("Taxi Vehicles");
        menulist.add(2, model2);

        ManageDriverDataModel model3 = new ManageDriverDataModel();
        model3.setMenu("Taxi Drivers");
        menulist.add(3, model3);

        ManageDriverDataModel model4 = new ManageDriverDataModel();
        model4.setMenu("Passenger Services");
        menulist.add(4, model4);

        adapter = new ManageDriverMenuAdapter(menulist, getActivity(), getActivity());
        manageLV.setAdapter(adapter);


        manageLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ManageDriverDataModel.setManageDriverDataModel(menulist.get(i));


                switch (menulist.get(i).getMenu()) {
                    case "Taxi Drivers":
                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new ViewDriverAdminFragment()).addToBackStack(null).commit();
                        break;
                    case "Taxi Vehicles":
                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new ManageVehicleFragment()).addToBackStack(null).commit();
                        break;
                    case "Passenger Services":
                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new ManageServicesDriverFragment()).addToBackStack(null).commit();
                        break;
                    case "Associations":
                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new ManageAssociationAdminFragment()).addToBackStack(null).commit();
                        break;
                    case "Company":
                        getFragmentManager().beginTransaction().replace(R.id.frame_container, new ManageCompanyFragment()).addToBackStack(null).commit();
                        break;


                    default:

                }


            }
        });


    }

    private void initUI() {
        manageLV = (ListView) view.findViewById(R.id.manageLV);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
        }
    }
}