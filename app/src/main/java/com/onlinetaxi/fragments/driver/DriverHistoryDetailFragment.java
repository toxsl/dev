package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.fragments.ProgressFragment;

/**
 * Created by TOXSL/tkarsh.shukla on 17/11/16.
 */
public class DriverHistoryDetailFragment extends BaseFragment {
    View view;
    private ImageView drawerIconIV;
    private TextView disputeRequestTV, completeTripTV, ratePassengerTV, cancelTripTV, viewProgressTV, promoteTaxiTV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_history_detail, container, false);
        initUI();
        return view;


    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

        disputeRequestTV = (TextView) view.findViewById(R.id.disputeRequestTV);
        completeTripTV = (TextView) view.findViewById(R.id.completeTripTV);
        viewProgressTV = (TextView) view.findViewById(R.id.viewProgressTV);
        ratePassengerTV = (TextView) view.findViewById(R.id.ratePassengerTV);
        cancelTripTV = (TextView) view.findViewById(R.id.cancelTripTV);
        promoteTaxiTV = (TextView) view.findViewById(R.id.promoteTaxiTV);


        ratePassengerTV.setOnClickListener(this);
        viewProgressTV.setOnClickListener(this);
        cancelTripTV.setOnClickListener(this);
        completeTripTV.setOnClickListener(this);
        promoteTaxiTV.setOnClickListener(this);
        disputeRequestTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.disputeRequestTV:
                break;
            case R.id.completeTripTV:
                break;
            case R.id.viewProgressTV:
                baseActivity.getSupportFragmentManager().beginTransaction().
                        replace(R.id.frame_container, new ProgressFragment()).
                        addToBackStack(null).commit();
                break;
            case R.id.ratePassengerTV:

                baseActivity.getSupportFragmentManager().beginTransaction().
                        replace(R.id.frame_container, new RatePassengerFragment()).
                        addToBackStack(null).commit();
                break;
            case R.id.cancelTripTV:
                baseActivity.getSupportFragmentManager().beginTransaction().
                        replace(R.id.frame_container, new DriverCancellationFragment()).
                        addToBackStack(null).commit();
                break;
            case R.id.promoteTaxiTV:
                break;


        }
    }
}
