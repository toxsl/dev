package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.adapter.CardAdapter;
import com.onlinetaxi.adapter.CompanyHistoryDetailAdapter;
import com.onlinetaxi.data.CardsModel;
import com.onlinetaxi.data.CompanyData;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.fragments.AddcardFragment;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.fragments.RateDriverFragment;
import com.onlinetaxi.ormdata.AppDataBaseOrm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TOXSL/utkarsh.shukla on 15/11/16.
 */
public class CompanyHistory extends BaseFragment {
    private View view;
    private ListView companyLV;
    private TextView viemoreTV;
    private Button submitFAB;
    private ListView listView;
    List<AppDataBaseOrm> cardlist = new ArrayList<>();
    CardAdapter cardAdapter;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_company_fragment, container, false);

        initUI();
        ArrayList<CompanyData> companyDatas = new ArrayList<>();
        companyLV = (ListView) view.findViewById(R.id.companyLV);
        companyLV.setAdapter(new CompanyHistoryDetailAdapter(baseActivity, 0, companyDatas));


        viemoreTV= (TextView) view.findViewById(R.id.viemoreTV);
        viemoreTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new CompanyHistoryDetailsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
            }
        });

        submitFAB= (Button) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new AddcardFragment()).commit();
            }
        });

        return view;
    }

    private void initUI() {
        listView = (ListView) view.findViewById(R.id.listviewCARD);

        cardlist = AppDataBaseOrm.getAllList(getActivity());

        cardAdapter = new CardAdapter(getActivity(), R.layout.custon_card_row, cardlist);
        listView.setAdapter(cardAdapter);
    }
}
