package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.DriverHistoryAdapter;
import com.onlinetaxi.data.HistoryData;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 17/11/16.
 */
public class HistoryNotAdminDriverFragment extends BaseFragment {
    View view;

    private ListView historyLV;
    private ImageView drawerIconIV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history_driver_not_admin, container, false);



        initUI();
        return view;
    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        historyLV = (ListView) view.findViewById(R.id.historyLV);


        ArrayList<HistoryData> historyDatas = new ArrayList<>();
        historyLV.setAdapter(new DriverHistoryAdapter(baseActivity, 0, historyDatas));
        historyLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Fragment fragment = new DriverHistoryDetailFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }
}
