package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;

/**
 * Created by TOXSL/utkarsh.shukla on 10/11/16.
 */
public class ManageDriversFragment extends BaseFragment{

    private
    View view;
    private TextView permissionTV;
    private ImageView drawerIconIV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_manage_drivers,container,false);
        permissionTV= (TextView) view.findViewById(R.id.permissionTV);
        drawerIconIV= (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        permissionTV.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.permissionTV:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new PermissionsFragment()).commit();

                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
        }
    }
}
