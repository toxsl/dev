package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.AllocateVehicleAdapter;
import com.onlinetaxi.adapter.ManageVehiclePagerAdapter;
import com.onlinetaxi.fragments.BaseFragment;

import com.sembozdemir.viewpagerarrowindicator.library.ViewPagerArrowIndicator;

import java.util.ArrayList;

/**
 * Created by Android on 11/15/2016.
 */

public class VehicleFeatureFragment extends BaseFragment {

    View view;
    private ImageView drawerIconIV;
    RecyclerView recycler_view;
    Button btnAll;
    ViewPagerArrowIndicator viewPagerArrowIndicator;
    ViewPager viewPager;
    ManageVehiclePagerAdapter pagerAdapter;
    AllocateVehicleAdapter featureAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.fragment_vehicle_feature,container,false);
      inItUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

/*
        ArrayList<String> Featurelist=new ArrayList<>();
        Featurelist.add("Air Condition");
        Featurelist.add("Child Friendly");
        Featurelist.add("Allow Pet");
        Featurelist.add("Meal On Board");
        Featurelist.add("USB");
        Featurelist.add("TV");
        Featurelist.add("Games");
        Featurelist.add("GPS");
        Featurelist.add("Smoking");
        Featurelist.add("Sleep");
        Featurelist.add("Disablity");
        Featurelist.add("Wifi");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setHasFixedSize(true);
        featureAdapter=new AllocateVehicleAdapter(getActivity(),Featurelist);
        recycler_view.setAdapter(featureAdapter);*/

        ArrayList<String> list=new ArrayList<>();
        list.add("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQYDQ9yfQvG-cxd_Zb8sJ3b2MmaxtSAHnG_okE6OWYqKqVxHxf7Kw");
        list.add("https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRwq_Xbp5hxjMwqKgBYOaL48AplRfjICD09htshWEf5Uo-FmMx6hA");
        list.add("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTANeNCGesYB5wR37GegMoIKMTMsfAU0kmvPHCr1i8zKrLAaJ-l");
        list.add("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQ9PDXqY2tTDF6Ei-4mk0SoUEZ4eB5N5y9oakrbw2tItH_Ra8j89Q");
        list.add("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcShXC5TVU68m2bdOot10dAOMpvnCSTFYm1Bf7-3pXHQjJGahRfM");


        viewPager = (ViewPager)view. findViewById(R.id.viewPager);
        viewPagerArrowIndicator = (ViewPagerArrowIndicator) view.findViewById(R.id.viewPagerArrowIndicator);
        pagerAdapter=new ManageVehiclePagerAdapter(list,getActivity());
        viewPager.setAdapter(pagerAdapter);


        viewPagerArrowIndicator.bind(viewPager);


        viewPagerArrowIndicator.setArrowIndicatorRes(R.drawable.arrow_lf,
                R.drawable.arrow_rg);


    }
    public void inItUI(){

        drawerIconIV= (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
       // recycler_view=(RecyclerView)view.findViewById(R.id.recycler_view);
        btnAll=(Button)view.findViewById(R.id.btnAll);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
        }
    }
}
