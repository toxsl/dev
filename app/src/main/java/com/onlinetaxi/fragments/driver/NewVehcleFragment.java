package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;

/**
 * Created by TOXSL/utkarsh.shukla on 8/11/16.
 */
public class NewVehcleFragment extends BaseFragment {

    private View view;
    private TextView carYearTVF, carModelTVF, carMakeTVF;
    private ImageView drawerIconIV;
    private Button submitFAB;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_vehcile, container, false);
        initUI();
        return view;
    }

    private void initUI() {
        carMakeTVF = (TextView) view.findViewById(R.id.carMakeTVF);
        carMakeTVF.setTypeface(baseActivity.iconFont);

        carModelTVF = (TextView) view.findViewById(R.id.carModelTVF);
        carModelTVF.setTypeface(baseActivity.iconFont);
        carYearTVF = (TextView) view.findViewById(R.id.carYearTVF);
        carYearTVF.setTypeface(baseActivity.iconFont);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        submitFAB = (Button) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new TarrifFragment()).addToBackStack(null).commit();

                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }
}
