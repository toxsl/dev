package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;

/**
 * Created by TOXSL/utkarsh.shukla on 22/11/16.
 */
public class ManageServicesDriverFragment extends BaseFragment {
    View view;
    private ImageView drawerIconIV;

    private RadioButton hourlyRB, kilometerRB;
    private EditText calloutFeeET, hourltRateET, offPeakPeriodET, PeakPeriodET;
//    private TextView dropdoenFT;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_managr_services_driver, container, false);

        initUI();
        return view;
    }

    private void initUI() {
        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

/*        dropdoenFT = (TextView) view.findViewById(R.id.dropdoenFT);
        dropdoenFT.setTypeface(baseActivity.iconFont1);*/

        calloutFeeET = (EditText) view.findViewById(R.id.calloutFeeET);
        hourltRateET = (EditText) view.findViewById(R.id.hourltRateET);
        offPeakPeriodET = (EditText) view.findViewById(R.id.offPeakPeriodET);
        PeakPeriodET = (EditText) view.findViewById(R.id.PeakPeriodET);

        hourlyRB = (RadioButton) view.findViewById(R.id.hourlyRB);
        kilometerRB = (RadioButton) view.findViewById(R.id.kilometerRB);


        if (hourlyRB.isChecked()) {
            calloutFeeET.setVisibility(View.VISIBLE);
            hourltRateET.setVisibility(View.VISIBLE);
            offPeakPeriodET.setVisibility(View.GONE);
            PeakPeriodET.setVisibility(View.GONE);

        } else if (kilometerRB.isChecked()) {
            calloutFeeET.setVisibility(View.GONE);
            hourltRateET.setVisibility(View.GONE);
            offPeakPeriodET.setVisibility(View.VISIBLE);
            PeakPeriodET.setVisibility(View.VISIBLE);
        }


        hourlyRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    calloutFeeET.setVisibility(View.VISIBLE);
                    hourltRateET.setVisibility(View.VISIBLE);
                    offPeakPeriodET.setVisibility(View.GONE);
                    PeakPeriodET.setVisibility(View.GONE);
                }else {
                    calloutFeeET.setVisibility(View.GONE);
                    hourltRateET.setVisibility(View.GONE);
                    offPeakPeriodET.setVisibility(View.VISIBLE);
                    PeakPeriodET.setVisibility(View.VISIBLE);
                }

            }
        });

        kilometerRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    calloutFeeET.setVisibility(View.GONE);
                    hourltRateET.setVisibility(View.GONE);
                    offPeakPeriodET.setVisibility(View.VISIBLE);
                    PeakPeriodET.setVisibility(View.VISIBLE);
                } else {
                    calloutFeeET.setVisibility(View.VISIBLE);
                    hourltRateET.setVisibility(View.VISIBLE);
                    offPeakPeriodET.setVisibility(View.GONE);
                    PeakPeriodET.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

                break;
        }
    }
}
