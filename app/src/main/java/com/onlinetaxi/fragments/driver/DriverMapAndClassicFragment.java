package com.onlinetaxi.fragments.driver;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.DriverMapClassicPagerAdapter;
import com.onlinetaxi.adapter.LoginFragmentsAdapter;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.fragments.driver.DriverClassicFragment;
import com.onlinetaxi.fragments.driver.DriverMapFragment;
import com.onlinetaxi.utils.SlidingTabLayout;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class DriverMapAndClassicFragment extends BaseFragment {

    ImageView drawerIconIV;
    View viewMap, viewClassic;
    private TabHost mTabHost;
    View view;
    private ViewPager pager;
    private CharSequence titles[] = new CharSequence[2];
    private DriverMapClassicPagerAdapter adapter;
    private int noOfTabs = 2;
    private SlidingTabLayout tabs;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.driver_map_classic,container,false);

        init();


        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        titles[0] = getString(R.string.map);
        titles[1] = getString(R.string.classic);
        adapter = new DriverMapClassicPagerAdapter(getActivity().getSupportFragmentManager(), titles,
                noOfTabs);

        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.colorPrimary);
            }

            @Override
            public int getDividerColor(int position) {
                return getResources().getColor(R.color.transparent);
            }
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);

    }

    private void init() {

        tabs = (SlidingTabLayout)view. findViewById(R.id.tabs);
        pager = (ViewPager)view. findViewById(R.id.pager);
        drawerIconIV = (ImageView)view. findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
            case R.id.GoTaxiRL:
               /* Fragment submit = new SetLocationFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putInt("pager_position", 0);
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, submit).commit();
                submit.setArguments(bundle2);*/
                break;

        }
    }
    @Override
    public void onResume() {
        super.onResume();
        try {

          /*  if (checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 5)) {

                init();
            }*/
        } catch (InflateException e) {
            e.printStackTrace();
        }

    }



}
