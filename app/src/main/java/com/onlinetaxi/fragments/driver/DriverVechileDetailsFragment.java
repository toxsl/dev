package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.fragments.BaseFragment;

/**
 * Created by TOXSL/utkarsh.shukla on 7/11/16.
 */
public class DriverVechileDetailsFragment extends BaseFragment {
    private View view;
    private TextView carYearTVF, carModelTVF, carMakeTVF;
    private TextView MakerTV, ModelTV, YearTV;
    private Button submitFAB;
    private ImageView drawerIconIV;
    private LinearLayout carMakerLL,carModelLL,carYearLL;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_vichle_details, container, false);
        initUI();
        return view;
    }

    private void initUI() {


        MakerTV = (TextView) view.findViewById(R.id.MakerTV);
        ModelTV = (TextView) view.findViewById(R.id.ModelTV);
        YearTV = (TextView) view.findViewById(R.id.YearTV);

        carMakeTVF = (TextView) view.findViewById(R.id.carMakeTVF);
        carMakeTVF.setTypeface(baseActivity.iconFont);

        carModelTVF = (TextView) view.findViewById(R.id.carModelTVF);
        carModelTVF.setTypeface(baseActivity.iconFont);
        carYearTVF = (TextView) view.findViewById(R.id.carYearTVF);
        carYearTVF.setTypeface(baseActivity.iconFont);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);
        submitFAB = (Button) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);


        carMakerLL= (LinearLayout) view.findViewById(R.id.carMakerLL);
        carMakerLL.setOnClickListener(this);
        carModelLL= (LinearLayout) view.findViewById(R.id.carModelLL);
        carModelLL.setOnClickListener(this);
        carYearLL= (LinearLayout) view.findViewById(R.id.carYearLL);
        carYearLL.setOnClickListener(this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (CarYearFragment.fromYearPage == true) {
            CarYearFragment.fromYearPage=false;

            MakerTV.setText(CarMakersFragment.CarMaker);
            ModelTV.setText(CarModelsFragment.CarModel);
            YearTV.setText(CarYearFragment.CarYear);

        }
        else{
            MakerTV.setText("Car Maker");
            ModelTV.setText("Car Model");
            YearTV.setText("Car Year");

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submitFAB:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new TarrifFragment()).addToBackStack(null).commit();

                break;
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;

            case R.id.carMakerLL:
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new CarMakersFragment()).addToBackStack(null).commit();
                break;
            case R.id.carModelLL:
                Toast.makeText(getActivity(),"Select Car Maker",Toast.LENGTH_SHORT).show();
                break;
            case R.id.carYearLL:
                Toast.makeText(getActivity(),"Select Car Maker",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
