package com.onlinetaxi.fragments.driver;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.BaseActivity;
import com.onlinetaxi.data.DriverProfileData;
import com.onlinetaxi.fragments.BaseFragment;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.mqtt.MqttAndroidClient;
import com.onlinetaxi.mqtt.MqttCallbackHandler;
import com.onlinetaxi.utils.Const;
import com.onlinetaxi.utils.FontManager;
import com.onlinetaxi.utils.MyMqttConnectedCallback;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Android on 11/17/2016.
 */

public class DriverMapFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BaseActivity.PermCallback, LocationListener, MyMqttConnectedCallback, MqttCallback {


    View view;
    private SupportMapFragment availableMap;
    GoogleMap googlemap;
    RelativeLayout GoTaxiRL;
    ImageView locationmarkerIV;
    private LocationManager mLocationManager;
    Dialog alertDialogPassenger, alertDialogTaxiIcon;
    protected GoogleApiClient mGoogleApiClient;
    private Location currentLocation;
    private int count;
    Typeface iconFont;
    ImageView TaximarkerIV;
    MqttAndroidClient mqttAndroidClient;
    private MqttCallback mqttCallback;
    private double distance;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.driver_map, container, false);
        mqttAndroidClient = baseActivity.getMqttclient();

        baseActivity.setMyMqttConnectedCallback(this);
        baseActivity.mqttAndroidClient.setCallback(this);


        inItUI();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        iconFont = FontManager.getTypeface(getActivity(), FontManager.FONTAWESOMEWebFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontEmail), iconFont);
        FontManager.markAsIconContainer(view.findViewById(R.id.fontTaxi), iconFont);
        TaximarkerIV.setImageResource(R.drawable.taxi_vector);

        if (checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99)) {
            buildGoogleApiClient();
            initilizeMap();
        }

    }


    private void subscribeToTopic(String return_topic) {//Subscribe on mQTT topic
        String topic = return_topic;
        //    SessionManager.setReturnTopic(getActivity(), topic);
        int qos = 0;
        try {
            IMqttToken subToken = mqttAndroidClient.subscribe(topic, qos);
            subToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    baseActivity.log("Subscribed to topic : " + Arrays.toString(asyncActionToken.getTopics()));
                    // The message was published
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    baseActivity.log("oops! Not subscribed to : " + Arrays.toString(asyncActionToken.getTopics()));
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribeToTopic on the specified topic e.g. using wildcards
                }
            });

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    public void inItUI() {

        locationmarkerIV = (ImageView) view.findViewById(R.id.locationmarkerIV);
        GoTaxiRL = (RelativeLayout) view.findViewById(R.id.GoTaxiRL);
        TaximarkerIV = (ImageView) view.findViewById(R.id.locationmarkerIV);
        mLocationManager = (LocationManager) baseActivity.getSystemService(Context.LOCATION_SERVICE);

    }


    protected synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

    }


    private void initilizeMap() {
        availableMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        if (googlemap == null) {
            availableMap.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {
                    googleMap.setInfoWindowAdapter(infoWindowAdapter);
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        googleMap.setMyLocationEnabled(true);
                        googleMap.setPadding(0, 0, 0, 150);
                        googleMap.getUiSettings().setMapToolbarEnabled(false);
                        googleMap.getUiSettings().setZoomControlsEnabled(true);
                        googleMap.getUiSettings().setZoomGesturesEnabled(true);
                        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                            @Override
                            public boolean onMyLocationButtonClick() {
                                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                                    currentLocation = getLastKnownLocation();
                                    googlemap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15));

                                    return true;
                                } else {
                                    turnOnGPS_Dialog();

                                }
                                return false;
                            }
                        });
                        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                            @Override
                            public void onMapLoaded() {
                                setMarker();
                            }
                        });
                        DriverMapFragment.this.googlemap = googleMap;
                        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                                getActivity(), R.raw.map_style);
                        googlemap.setMapStyle(style);

                    }
                }
            });

        }

    }


    private GoogleMap.InfoWindowAdapter infoWindowAdapter = new GoogleMap.InfoWindowAdapter() {
        @Override
        public View getInfoWindow(Marker marker) {


            alertDialogTaxiIcon = new Dialog(getActivity());

            alertDialogTaxiIcon.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogTaxiIcon.setContentView(R.layout.info_window_taxi_icon);
            alertDialogTaxiIcon.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            alertDialogTaxiIcon.show();

            TextView textViewAccept = (TextView) alertDialogTaxiIcon.findViewById(R.id.textViewAccept);
            TextView textViewCancel = (TextView) alertDialogTaxiIcon.findViewById(R.id.textViewCancel);

            textViewAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    alertDialogTaxiIcon.dismiss();
                }
            });
            textViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    alertDialogTaxiIcon.dismiss();
                }
            });


            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;

        }
    };

    private void setMarker() {
        if (currentLocation != null) {
            baseActivity.latitude = currentLocation.getLatitude();
            baseActivity.longitude = currentLocation.getLongitude();
            GetNearLocations getlocations = new GetNearLocations();
            getlocations.execute();


        } else {
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (count < 5) {
                    count++;
                    currentLocation = getLastKnownLocation();
                    onLocationChanged(currentLocation);
                } else
                    showToast(baseActivity.getString(R.string.cannot_get_location_please_refresh));
            } else
                turnOnGPS_Dialog();
        }

        if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            googlemap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15));

        } else {

            turnOnGPS_Dialog();
        }

    }


    public void turnOnGPS_Dialog() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result1 =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result1.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result1) {
                    final com.google.android.gms.common.api.Status status = result1.getStatus();
                    final LocationSettingsStates state = result1.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void permGranted(int resultCode) {

        buildGoogleApiClient();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();

    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        setMarker();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        currentLocation = getLastKnownLocation();
        onLocationChanged(currentLocation);
    }

    @Override
    public void onProviderDisabled(String s) {
        if (s.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
            turnOnGPS_Dialog();
    }


    private Location getLastKnownLocation() {
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    mLocationManager.requestLocationUpdates(provider, Const.MIN_TIME_BW_UPDATES, Const.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    l = mLocationManager.getLastKnownLocation(provider);
                }
                if (l != null && (bestLocation == null || l.getAccuracy() > bestLocation.getAccuracy())) {
                    bestLocation = l;
                }
            }
        }
        return bestLocation;
    }


    @Override
    public void onMqttConnected() {
        subscribeToTopic(SessionManager.getState_Topic(getActivity()));
    }

    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        baseActivity.log("" + message);
        //  String msg = new String(message.getPayload());

        JSONObject object = new JSONObject("" + message);

        String lat = object.optString("Latitude");
        String lonng = object.optString("Longitude");
        String Return_topic = object.optString("Return_topic");
        baseActivity.log("returntopic______" + Return_topic);

        if (currentLocation != null) {
            double currentlat = currentLocation.getLatitude();
            double currentlong = currentLocation.getLongitude();

            double destlat = Double.parseDouble(lat);
            double destlong = Double.parseDouble(lonng);

            distance = baseActivity.HaversineFormula(currentlat, currentlong, destlat, destlong);
            if (distance <= Double.parseDouble(SessionManager.getDriverRadius(getActivity())))
                publishMessage(String.valueOf(currentlat), String.valueOf(currentlong), Return_topic, distance);

        }
    }

    public void publishMessage(String latitude, String longitude, String Return_topic, double distance) {//publish on mQTT
        String topic = Return_topic;
        JSONObject object = new JSONObject();

        DriverProfileData driverProfileData = new DriverProfileData(Parcel.obtain());
        driverProfileData = SessionManager.getDriverProfileData(getActivity());
        if (driverProfileData != null) {
            try {

                object.put("isocode", "" + SessionManager.getUserCountryCode(getActivity()));
                object.put("latitude", latitude);
                object.put("longitude", longitude);
                object.put("companycode", driverProfileData.DrivrCompnyCode);
                object.put("companyname", driverProfileData.DriverCompanyName);
                object.put("logo", driverProfileData.DrivrCompnylogo);
                object.put("contact", driverProfileData.DriverCompanyTelno);
                object.put("drivercode", driverProfileData.DriverCode);
                object.put("DriverType", driverProfileData.DriverType);
                object.put("Drivername", driverProfileData.DriverFirstName + " " + driverProfileData.Driverlastname);
                object.put("Driverratekm", driverProfileData.DriverOffPeakRAte);
                object.put("Vehicle", driverProfileData.Veahicle);
                object.put("Vehicleseat", driverProfileData.DriverVehicleSeat);
                object.put("Rating", driverProfileData.Rating);
                object.put("Status", driverProfileData.Status);
              /*  object.put("locality",driverProfileData.locality);
                object.put("coverage",driverProfileData.coverage);*/


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        byte[] encodedPayload = new byte[0];
        try {
            encodedPayload = object.toString().getBytes("UTF-8");
            baseActivity.log("bublished data>>>>  " + encodedPayload + "   >>>>>>>" + topic);
            MqttMessage message = new MqttMessage(encodedPayload);
            mqttAndroidClient.publish(topic, message, this, myMqttActionCallback);
        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }
    }

    private IMqttActionListener myMqttActionCallback = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
            baseActivity.log("O yeah!!!!,token: " + asyncActionToken);
            baseActivity.log("O yeah!!!!,topics: " + Arrays.toString(asyncActionToken.getTopics()));
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            baseActivity.log("mar gya re!!!!,token: " + asyncActionToken + " , exception: " + exception);
        }
    };

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
}
