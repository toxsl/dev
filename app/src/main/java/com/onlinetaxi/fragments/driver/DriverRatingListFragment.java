package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.FeedBackListAdapter;
import com.onlinetaxi.data.CompanyData;
import com.onlinetaxi.data.FeedBackListData;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by TOXSL/utkarsh.shukla on 16/11/16.
 */
public class DriverRatingListFragment extends BaseFragment {

    View view;
    private ListView ratingListLV;
    FeedBackListAdapter feedBackListAdapter;
    private ImageView drawerIconIV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_driver_ratinglist,container,false);

        initUI();

        return view;
    }

    private void initUI() {
        ArrayList<FeedBackListData>feedBackListDatas = new ArrayList<>();

        ratingListLV= (ListView) view.findViewById(R.id.ratingListLV);
        ratingListLV.setAdapter(new FeedBackListAdapter(baseActivity, 0,feedBackListDatas));

        drawerIconIV= (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);

            }
        });


    }
}
