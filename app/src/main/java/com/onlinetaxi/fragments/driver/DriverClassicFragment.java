package com.onlinetaxi.fragments.driver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.onlinetaxi.R;
import com.onlinetaxi.adapter.DriverClassicAdapter;
import com.onlinetaxi.fragments.BaseFragment;

import java.util.ArrayList;

/**
 * Created by Android on 11/17/2016.
 */

public class DriverClassicFragment extends BaseFragment {

    View view;
    ListView ClassicLV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.driver_classic,container,false);
        initUI();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        ArrayList<String> list=new ArrayList<>();
        ClassicLV.setAdapter(new DriverClassicAdapter(getActivity(),getActivity().getApplicationContext(),list));

        ClassicLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                getFragmentManager().beginTransaction().replace(R.id.frame_container, new DriverDetailsFragment()).commit();

            }
        });

    }

    private void initUI() {
        ClassicLV= (ListView) view.findViewById(R.id.manageRV);

    }

}
