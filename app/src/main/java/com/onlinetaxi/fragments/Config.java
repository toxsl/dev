package com.onlinetaxi.fragments;

/**
 * Created by halatedzi on 2016/02/27.
 */
public class Config {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // type of push messages
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;
    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String FILE_UPLOAD_URL = "http://37.139.25.236/ot/ot-app/V1/uploadedimages/upload.php";
    //public static final String FILE_UPLOAD_URL = "http://192.168.1.10/upload.php";
    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Online Taxi";
    // flag to identify whether to show single line
    // or multi line text in push notification tray
    public static boolean appendNotificationMessages = true;
}

