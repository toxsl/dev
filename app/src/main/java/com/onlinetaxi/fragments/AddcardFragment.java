package com.onlinetaxi.fragments;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.connect.PWConnect;
import com.mobile.connect.exception.PWError;
import com.mobile.connect.exception.PWException;
import com.mobile.connect.exception.PWProviderNotInitializedException;
import com.mobile.connect.listener.PWTokenObtainedListener;
import com.mobile.connect.listener.PWTransactionListener;
import com.mobile.connect.payment.PWPaymentParams;
import com.mobile.connect.payment.credit.PWCreditCardType;
import com.mobile.connect.provider.PWTransaction;
import com.mobile.connect.service.PWProviderBinder;
import com.onlinetaxi.R;
import com.onlinetaxi.activity.MainActivity;
import com.onlinetaxi.adapter.SpinnerAdapter;
import com.onlinetaxi.data.PaymentTypeModel;
import com.onlinetaxi.helper.ApplicationGlob;
import com.onlinetaxi.helper.CryptoUtil;
import com.onlinetaxi.helper.HttpUtility;
import com.onlinetaxi.helper.RegexValiations;
import com.onlinetaxi.helper.SessionManager;
import com.onlinetaxi.material.RippleView;
import com.onlinetaxi.ormdata.AppDataBaseOrm;
import com.onlinetaxi.utils.Const;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by TOXSL\paramveer.rana on 26/9/16.
 */
public class AddcardFragment extends BaseFragment implements
        PWTokenObtainedListener, PWTransactionListener {
    private Spinner spinner;
    private ImageView drawerIconIV;
    private PWProviderBinder _binder;
    private static final String APPLICATIONIDENTIFIER = "peach.OnlineTaxi.mcommerce.test";
    private static final String PROFILETOKEN = "652534dcdd5c435eb6aab260ce37a5af";
    Dialog alertDialogProgressBar;
    CoordinatorLayout parent;
    public static String TranstionId = "";
    public static String TranstionState = "Sucess";
    EditText editTextHolderName, editTextCardNumber, editTextCardExpireMonth,
            editTextCardcvv, editTextYear;
    String LocalCardNumber, LocalTokennumber, LocalCardType, OriginalCardNumber;
    Button submitFAB;
    String holder, cardnumber, cvv, month, year, StringCardType;
    private boolean currentTokenization;
    CreateCardTask task;

    /* String[] textArray = {"Choose Card", "Visa", "Master Card", "American Express", "Diners Club", "JCB"};
     Integer[] imageArray = {0, R.drawable.visalogo, R.drawable.mastercard,
             R.drawable.americanexpress, R.drawable.dinerclub, R.drawable.jcb};
 */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_card, container, false);

        drawerIconIV = (ImageView) view.findViewById(R.id.drawerIconIV);
        drawerIconIV.setOnClickListener(this);

        spinner = (Spinner) view.findViewById(R.id.spinner);
        editTextHolderName = (EditText) view.findViewById(R.id.holderNameET);
        editTextCardNumber = (EditText) view.findViewById(R.id.CardNumberET);
        editTextCardExpireMonth = (EditText) view.findViewById(R.id.ExpireMonthET);
        editTextCardcvv = (EditText) view.findViewById(R.id.CvvET);
        editTextYear = (EditText) view.findViewById(R.id.ExpireYearET);
        submitFAB = (Button) view.findViewById(R.id.submitFAB);
        parent = (CoordinatorLayout) view.findViewById(R.id.parent);

        SpinnerAdapter adapter = new SpinnerAdapter(baseActivity, R.layout.spinner_value_layout, PaymentTypeModel.getAllCardTypes());
        spinner.setAdapter(adapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().startService(new Intent(getActivity(),
                com.mobile.connect.service.PWConnectService.class));
        getActivity().bindService(new Intent(getActivity(),
                        com.mobile.connect.service.PWConnectService.class),
                _serviceConnection, Context.BIND_AUTO_CREATE);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressWarnings("unused")
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int pos, long id) {
                PaymentTypeModel item = (PaymentTypeModel) parent.getItemAtPosition(pos);

                StringCardType = item.getCardType();

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        submitFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ApplicationGlob.isConnectingToInternet(getActivity())) {


                    if (checkValidation()) {

                        //    progressbar(true, "");
                        holder = editTextHolderName.getText().toString();
                        cardnumber = editTextCardNumber.getText().toString();

                        cvv = editTextCardcvv.getText().toString();
                        // extract month
                        month = editTextCardExpireMonth.getText().toString();
                        year = editTextYear.getText().toString();

                        PWPaymentParams paymentParams = null;

                        PWCreditCardType cardType = PWCreditCardType.VISA;
                        try {

                            switch (StringCardType) {
                                case "Visa":
                                    cardType = PWCreditCardType.VISA;
                                    break;

                                case "MasterCard":
                                    cardType = PWCreditCardType.MASTERCARD;
                                    break;
                                case "American Express":
                                    cardType = PWCreditCardType.AMEX;
                                    break;

                                case "Diners Club":
                                    cardType = PWCreditCardType.DINERS;
                                    break;

                                case "JCB":
                                    cardType = PWCreditCardType.JCB;
                                    break;

                                default:
                                    break;
                            }

                        } catch (Exception e) {

                        }

                        OriginalCardNumber = cardnumber;
                        String last4 = cardnumber == null || cardnumber.length() < 4 ? cardnumber
                                : cardnumber.substring(cardnumber.length() - 4);
                        String Cradircard = cardType.toString();

                        LocalCardNumber = last4;
                        LocalCardType = Cradircard;
                        //   FromPAymentImformation = true;

                        try {
                            paymentParams = _binder.getPaymentParamsFactory()
                                    .createCreditCardTokenizationParams(holder, cardType,
                                            cardnumber, year, month, cvv);

                        } catch (PWProviderNotInitializedException e) {
                            getErrorMessage("Error: Provider not initialized!");

                            e.printStackTrace();
                            return;
                        } catch (PWException e) {
                            getErrorMessage("Error: Invalid Parameters!");


                            e.printStackTrace();
                            return;
                        }

                        setStatusText("Preparing...");
                        currentTokenization = true;

                        try {
                            _binder.createAndRegisterObtainTokenTransaction(paymentParams);
                        } catch (PWException e) {
                            getErrorMessage("Error: Could not contact Gateway!");

                            e.printStackTrace();
                        }


                    }
                } else {


                    ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
                }
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unbindService(_serviceConnection);
        getActivity().stopService(new Intent(getActivity(),
                com.mobile.connect.service.PWConnectService.class));
    }


    public String getErrorMessage(String context) {
        Snackbar snackbar = Snackbar
                .make(parent, context, Snackbar.LENGTH_LONG);
        snackbar.show();
        return context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.drawerIconIV:
                ((MainActivity) baseActivity).mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    @Override
    public void obtainedToken(String token, PWTransaction pwTransaction) {

        TranstionId = pwTransaction.getProcessorUniqueIdentifier();
        TranstionState = pwTransaction.getState().toString();

        CryptoUtil cryptoUtil = new CryptoUtil();


        try {
            LocalCardNumber = cryptoUtil.encryptIt(LocalCardNumber);
            LocalCardType = cryptoUtil.encryptIt(LocalCardType);
            LocalTokennumber = cryptoUtil.encryptIt(token.toString());

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }




        if (ApplicationGlob.isConnectingToInternet(getActivity())) {

            task = new CreateCardTask();

            task.execute(SessionManager.getUserCountryCode(getActivity()),
                    SessionManager.getUserID(getActivity()),
                    holder,
                    StringCardType.toUpperCase(),
                    OriginalCardNumber,
                    LocalTokennumber,
                    cvv,
                    month,
                    year);

        } else {

            ApplicationGlob.ToastShowInterNetConnection(getActivity().getApplicationContext(), getActivity());
        }


        getActivity().runOnUiThread(new Runnable() {
            public void run() {


                Snackbar snackbar = Snackbar
                        .make(parent, "Card Created Successfully", Snackbar.LENGTH_LONG);
                snackbar.show();

            }
        });


    }

    private class CreateCardTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    progressbar(true, "");
                }
            });

        }

        @Override
        protected String doInBackground(String... paramsa) {
            {
                String response = "";
                Map<String, String> params = new HashMap<String, String>();
                String requestURL = "http://mobile.onlinetaxi.co.za/mobile_create_payment_card.php";
                params.put("pisocode", paramsa[0]);
                params.put("uid", paramsa[1]);
                params.put("pcardname", paramsa[2]);
                params.put("pcardtype", paramsa[3]);
                params.put("pcardnumber", paramsa[4]);
                params.put("ptoken", paramsa[5]);
                params.put("pcvv", paramsa[6]);
                params.put("pexpire_month", paramsa[7]);
                params.put("pexpire_year", paramsa[8]);
                params.put("submit", "payment_card");


                try {
                    HttpUtility.sendPostRequest(requestURL, params);

                    baseActivity.log(requestURL + "  " + params);

                    response = HttpUtility.readInputStreamToString();

                    Log.e("OT Login Response", "" + response);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                HttpUtility.disconnect();

                return response;
            }
        }


        @Override
        protected void onPostExecute(String result) {

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    progressbar(false,"");
                }
            });



            try {

                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("Response");
                JSONObject jsoObject1 = jsonArray.getJSONObject(0);
                String returncode = jsoObject1.getString("returncode");

                if (returncode.equals("0")) {
                    String massage=jsoObject1.getString("message");

                    List<AppDataBaseOrm> list;


                    list = AppDataBaseOrm.getSpecificCard(OriginalCardNumber);

                    if (list.size() == 0) {

                        AppDataBaseOrm information = new AppDataBaseOrm(
                                LocalCardNumber, LocalTokennumber, LocalCardType,
                                SessionManager.getUserID(getActivity().getApplicationContext()), OriginalCardNumber);

                        AppDataBaseOrm.insertImformations(getActivity().getApplicationContext(),
                                information);

                    }

                }else{

                    String massage=jsoObject1.getString("message");
                    ToastOnUiThread(massage);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void ToastOnUiThread(final String message) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity().getApplicationContext(), message,
                            Toast.LENGTH_LONG).show();
                }
            });

        }

    }


    @Override
    public void transactionSucceeded(PWTransaction pwTransaction) {

    }

    @Override
    public void transactionFailed(PWTransaction pwTransaction, PWError pwError) {

    }

    @Override
    public void creationAndRegistrationSucceeded(PWTransaction pwTransaction) {

        setStatusText("Processing...");


        if (currentTokenization) {
            // execute it
            try {
                _binder.obtainToken(pwTransaction);
            } catch (PWException e) {
                setStatusText("Invalid Transaction.");
                e.printStackTrace();
            }
        } else {
            // execute it
            try {
                _binder.debitTransaction(pwTransaction);
            } catch (PWException e) {
                setStatusText("Invalid Transaction.");
                e.printStackTrace();
            }
        }


    }

    @Override
    public void creationAndRegistrationFailed(PWTransaction pwTransaction, PWError pwError) {
        setStatusText("Error contacting the gateway.");
    }

    private ServiceConnection _serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            _binder = (PWProviderBinder) service;
            // we have a connection to the service
            try {
                _binder.initializeProvider(PWConnect.PWProviderMode.TEST,
                        APPLICATIONIDENTIFIER, PROFILETOKEN);
                _binder.addTokenObtainedListener(AddcardFragment.this);
                _binder.addTransactionListener(AddcardFragment.this);
            } catch (PWException ee) {
                setStatusText("Error initializing the provider.");

                // error initializing the provider
                ee.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            _binder = null;
        }
    };


    private void setStatusText(final String string) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {

             //     progressbar(true, string);

                Snackbar snackbar = Snackbar
                        .make(parent, string, Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
    }


    public void progressbar(boolean b, String string) {

        alertDialogProgressBar = new Dialog(getActivity(), R.style.YourCustomStyle);
        alertDialogProgressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogProgressBar.setContentView(R.layout.layout_progressbar);
        alertDialogProgressBar.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView textView = (TextView) alertDialogProgressBar.findViewById(R.id.textView1);

        textView.setVisibility(View.INVISIBLE);
        textView.setText(string);

        if (b == true) {
            alertDialogProgressBar.show();
        } else {

            alertDialogProgressBar.dismiss();
        }


    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!RegexValiations.hasText(editTextCardcvv))
            ret = false;

        if (!RegexValiations.hasText(editTextCardExpireMonth))
            ret = false;


        if (!RegexValiations.hasText(editTextCardNumber))
            ret = false;

        if (!RegexValiations.hasText(editTextHolderName))
            ret = false;
        if (!RegexValiations.hasText(editTextYear))
            ret = false;
        return ret;

    }


}
