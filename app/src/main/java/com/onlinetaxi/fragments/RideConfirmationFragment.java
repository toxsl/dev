package com.onlinetaxi.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onlinetaxi.R;

/**
 * Created by TOXSL\jyotpreet.kaur on 28/9/16.
 */
public class RideConfirmationFragment extends BaseFragment {

    private FloatingActionButton submitFAB;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ride_requested_confirmation, container, false);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        submitFAB = (FloatingActionButton) view.findViewById(R.id.submitFAB);
        submitFAB.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitFAB:
                baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new ProgressFragment()).commit();
                break;
        }
    }
}
